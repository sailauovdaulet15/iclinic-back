<?php

return [
    //REQUESTS_LIMIT - count of max requests per LIMIT_TIME.
    //by default, it's 50 requests every 3605 seconds (hour + extra 5 seconds);
    'api_url' => env('SENDPULSE_API_URL', 'https://api.sendpulse.com/'),
    'smtp_api_url' => env('SENDPULSE_SMTP_API_URL', 'https://api.sendpulse.com/smtp'),
    'client_id' => env('SENDPULSE_CLIENT_ID', ''),
    'client_secret' => env('SENDPULSE_CLIENT_SECRET', ''),
    'max_requests_count' => env('SENDPULSE_REQUESTS_LIMIT', 50),
    'limit_time' => env('SENDPULSE_LIMIT_TIME', 3605),
    'email_sender' => env('SENDPULSE_EMAIL_SENDER', 'baruzhan@rocketfirm.com'),
    'email_sender_name' => env('SENDPULSE_EMAIL_SENDER_NAME', 'iClinic'),
    'email_subject' => env('SENDPULSE_EMAIL_SUBJECT', 'Новая заявка'),
    'template_id' => env('SENDPULSE_MAIL_TEMPLATE', '35533'),

];
