<?php

use App\Models\HistoryVisit;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('order_row/{position}/{table}/{sort}/{direction}/{id}', 'App\Http\Controllers\Voyager\VoyagerBreadController@orderRow')
        ->name('order_row');
});

Route::post('add-appointment/{user_id}', function ($user_id) {
    HistoryVisit::factory()->count(20)->state([
        'user_id' => $user_id
    ])->create();
    return response('success');
})->name('addAppointment.user');

Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});
