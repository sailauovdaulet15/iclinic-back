<?php

use App\Enums\MainEnum;
use App\Http\Controllers\Api\v1\AuthController;
use App\Http\Controllers\Api\v1\MedicineCardController;
use App\Http\Controllers\Api\v1\PaymentController;
use App\Http\Controllers\Api\v1\RegistrationController;
use App\Http\Controllers\Api\v1\ContactController;
use App\Http\Controllers\Api\v1\CountryController;
use App\Http\Controllers\Api\v1\DoctorController;
use App\Http\Controllers\Api\v1\FaqController;
use App\Http\Controllers\Api\v1\ForgotPasswordController;
use App\Http\Controllers\Api\v1\HandbookBloodGroupController;
use App\Http\Controllers\Api\v1\MainController;
use App\Http\Controllers\Api\v1\NewsController;
use App\Http\Controllers\Api\v1\OrderingCallController;
use App\Http\Controllers\Api\v1\PharmacyController;
use App\Http\Controllers\Api\v1\PharmacyTabController;
use App\Http\Controllers\Api\v1\ProfileController;
use App\Http\Controllers\Api\v1\ServiceController;
use App\Http\Controllers\Api\v1\ServiceLandingController;
use App\Http\Controllers\Api\v1\SpecialisationController;
use App\Http\Controllers\Api\v1\StockController;
use App\Http\Controllers\Api\v1\BurgerMenuController;
use App\Http\Controllers\Api\v1\MainMenuController;
use App\Http\Controllers\Api\v1\FooterController;
use App\Http\Controllers\OnlineConsultationsController;
use App\Http\Controllers\SoapController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\AppointmentController;
use App\Http\Controllers\Api\v1\HandbookSurveyQuestionController;
use App\Http\Controllers\Api\v1\AnswerOnQuestionController;
use App\Http\Controllers\Api\v1\SymptomController;
use App\Http\Controllers\Api\v1\MedicineCardFileController;
use App\Http\Controllers\Api\v1\AboutUsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api',
    'prefix'     => 'v1/auth',
], function ($router) {
    Route::post('forgot-password', [ForgotPasswordController::class, 'forgotPassword']);
    Route::post('check-sms-code', [ForgotPasswordController::class, 'checkSmsCode']);
    Route::post('change-password', [ForgotPasswordController::class, 'changePassword']);
    Route::post('register', [RegistrationController::class, 'register']);
    Route::post('register/submit-account', [RegistrationController::class, 'submitAccount']);
    Route::get('register/check-iin/{iin}', [RegistrationController::class, 'checkIIN']);
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::get('profile', [AuthController::class, 'me']);
    Route::patch('profile', [ProfileController::class, 'update']);
    Route::get('profile/history', [ProfileController::class, 'history']);
    Route::patch('profile/update-password', [ProfileController::class, 'updatePassword']);
    Route::post('profile/change-avatar', [ProfileController::class, 'changeAvatar']);
    Route::get('profile/online-consultations', [OnlineConsultationsController::class, 'index'])
        ->middleware('auth:api');
    Route::get('doctor/online-consultations',
        [OnlineConsultationsController::class, 'indexDoctor'])->middleware('auth:api');

    Route::get('profile/medicine-cards', [MedicineCardController::class, 'index'])->middleware('auth:api');

    // Agora
    Route::post('/agora/chat', 'App\Http\Controllers\AgoraVideoController@tokenChat');
    Route::post('/agora/token', 'App\Http\Controllers\AgoraVideoController@token');
    Route::post('/agora/call-user', 'App\Http\Controllers\AgoraVideoController@callUser');
});

Route::name('v1.')->prefix('v1')->group(function () {
    Route::apiResource('faq', FaqController::class)->only('index');
    Route::apiResource('about-us', AboutUsController::class)->only('index', 'show');
    Route::apiResource('main', MainController::class)->only('index');
    Route::get('main/{type}', [MainController::class, 'show'])->where("type", implode('|', MainEnum::getConstants()))
        ->name('main.show');
    Route::apiResource('contacts', ContactController::class)->only('index');
    Route::apiResource('pharmacies', PharmacyController::class)->only('index');
    Route::apiResource('pharmacy-tabs', PharmacyTabController::class)->only('index');
    Route::apiResource('news', NewsController::class)->only('index', 'show');
    Route::apiResource('service-landings', ServiceLandingController::class)->only('show');
    Route::apiResource('services', ServiceController::class)->only('index', 'show');
    Route::get('services-count/{id}/specialisation', [ServiceController::class, 'getCountBySpecialisation'])
        ->name('services.count');
    Route::get('services/recommended/{id}', [ServiceController::class, 'getRecommended'])->name('services.recommended');
    Route::get('services/{slug}/doctor', [ServiceController::class, 'getByDoctor'])->name('services.doctor');
    Route::get('services/{id}/specialisation', [ServiceController::class, 'getBySpecialisation'])
        ->name('services.specialisation');
    Route::apiResource('doctors', DoctorController::class)->only('index', 'show');
    Route::get('doctors/{slug}/visits', [DoctorController::class, 'visits'])->name('doctors.visits');
    Route::get('doctors-service/{id}', [DoctorController::class, 'getDoctorsByService'])->name('doctors.service');
    Route::get('doctors-services', [DoctorController::class, 'getDoctorsByServices'])->name('doctors.services');
    Route::get('doctors-count/{id}/specialisation', [DoctorController::class, 'countBySpecialisation'])
        ->name('doctors.specialisation');
    Route::apiResource('appointment', AppointmentController::class)->only('store');
    Route::delete('appointment/{id}', [AppointmentController::class, 'destroy'])->name('appointment.delete');
    Route::post('appointment/delete-redis', [AppointmentController::class, 'destroyRedis'])
        ->name('appointment.destroy-redis');
    Route::get('appointment/get-by-user', [AppointmentController::class, 'userAppointment'])
        ->name('appointment.get-by-user');
    Route::get('appointment/view', [AppointmentController::class, 'view'])->name('appointment.view');
    Route::get('appointment/history', [AppointmentController::class, 'showClientHistory'])->name('appointment.history');

    Route::apiResource('stocks', StockController::class)->only('index', 'show');
    Route::apiResource('burger-menus', BurgerMenuController::class)->only('index');
    Route::apiResource('main-menus', MainMenuController::class)->only('index');
    Route::apiResource('footers', FooterController::class)->only('index');
    Route::apiResource('specialisations', SpecialisationController::class)->only('index', 'show');
    Route::get('specialisations/{slug}/doctors', [SpecialisationController::class, 'doctors'])
        ->name('specialisations.doctors');
    Route::get('specialisations/{slug}/services', [SpecialisationController::class, 'services'])
        ->name('specialisations.services');
    Route::get('test', [SoapController::class, 'show']);
    Route::apiResource('ordering-calls', OrderingCallController::class)->only('store');
    Route::apiResource('countries', CountryController::class)->only('index', 'show');
    Route::apiResource('handbook-blood-groups', HandbookBloodGroupController::class)->only('index', 'show');
    Route::apiResource('handbook-survey-question', HandbookSurveyQuestionController::class)->only('index');
    Route::apiResource('answer-on-question', AnswerOnQuestionController::class)->only('store');
    Route::post('answer-on-question/many', [AnswerOnQuestionController::class, 'storeMany'])
        ->name('answer-on-question.storeMany');
    Route::apiResource('symptoms', SymptomController::class)->only('show');

    Route::post('payment/token', [PaymentController::class, 'getToken']);
    Route::get('payment/info', [PaymentController::class, 'getPaymentInfo']);
    Route::post('payment/post-link', [PaymentController::class, 'postLink']);
    Route::post('payment/failure-post-link', [PaymentController::class, 'postLink']);

    Route::get('appointment-online/archive',[AppointmentController::class, 'onlineArchiveAppointment']);
    Route::get('appointment-online/scheduled',[AppointmentController::class, 'onlineScheduledAppointment']);

    Route::get('is-read/on/{id}', [\App\Http\Controllers\Api\v1\MedicineCardFileController::class, 'isReadOn']);
    Route::get('is-read/off/{id}', [\App\Http\Controllers\Api\v1\MedicineCardFileController::class, 'isReadOff']);


});
