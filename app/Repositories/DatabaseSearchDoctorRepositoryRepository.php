<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\SearchDoctorRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class DatabaseSearchDoctorRepositoryRepository implements SearchDoctorRepositoryInterface
{

    public function search($searchTerm): Collection
    {
        $attributes = ['first_name', 'second_name', 'third_name'];
        $searchTerm = mb_strtolower($searchTerm);
        return User::query()
            ->doctors()
            ->where(function ($query) use ($attributes, $searchTerm) {
                foreach ($attributes as $attribute) {
                    $query->orWhere(DB::raw("lower({$attribute})"), 'LIKE', "{$searchTerm}%");
                }
            })->get();
    }
}
