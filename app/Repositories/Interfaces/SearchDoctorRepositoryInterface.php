<?php

namespace App\Repositories\Interfaces;

use Illuminate\Support\Collection;

interface SearchDoctorRepositoryInterface
{
    public function search($searchTerm): Collection;
}
