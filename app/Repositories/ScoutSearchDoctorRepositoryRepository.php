<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Interfaces\SearchDoctorRepositoryInterface;
use Illuminate\Support\Collection;

class ScoutSearchDoctorRepositoryRepository implements SearchDoctorRepositoryInterface
{

    public function search($searchTerm): Collection
    {
        $doctors = User::search($searchTerm)->get();
        return $doctors;
    }
}
