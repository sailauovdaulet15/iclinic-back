<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\AnswerOnQuestion
 *
 * @property int $id
 * @property int $history_visit_id
 * @property int $handbook_survey_question_id
 * @property int $patient_id
 * @property int $doctor_user_id
 * @property int $answer
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Doctor $doctor
 * @property-read \App\Models\HandbookSurveyQuestion $handbookSurveyQuestion
 * @property-read \App\Models\HistoryVisit $historyVisit
 * @property-read \App\Models\User $patient
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOnQuestion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOnQuestion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOnQuestion query()
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOnQuestion whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOnQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOnQuestion whereDoctorUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOnQuestion whereHandbookSurveyQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOnQuestion whereHistoryVisitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOnQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOnQuestion wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AnswerOnQuestion whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AnswerOnQuestion extends Model
{
    use HasFactory;

    protected $fillable = [
        'handbook_survey_question_id', 'answer', 'history_visit_id',
        'patient_id', 'doctor_user_id',
    ];


    public function historyVisit(): BelongsTo
    {
        return $this->belongsTo(HistoryVisit::class);
    }

    public function handbookSurveyQuestion(): BelongsTo
    {
        return $this->belongsTo(HandbookSurveyQuestion::class);
    }

    public function patient(): BelongsTo
    {
        return $this->belongsTo(User::class, 'patient_id', 'id');
    }

    public function doctor(): BelongsTo
    {
        return $this->belongsTo(Doctor::class, 'doctor_user_id', 'user_id');
    }
}
