<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\HandbookBloodGroup
 *
 * @property int                                                         $id
 * @property string                                                      $title
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookBloodGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookBloodGroup newQuery()
 * @method static Builder|HandbookBloodGroup onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookBloodGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookBloodGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookBloodGroup whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookBloodGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookBloodGroup whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookBloodGroup whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookBloodGroup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookBloodGroup withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookBloodGroup withTranslations($locales = null, $fallback = true)
 * @method static Builder|HandbookBloodGroup withTrashed()
 * @method static Builder|HandbookBloodGroup withoutTrashed()
 * @mixin Eloquent
 */
class HandbookBloodGroup extends Model
{
    use HasFactory, SoftDeletes, Translatable;

    protected array $translatable = ['title'];
}
