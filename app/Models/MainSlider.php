<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\MainSlider
 *
 * @property int                                                         $id
 * @property string                                                      $title
 * @property string                                                      $description
 * @property string                                                      $image_uri
 * @property int                                                         $position Позиция
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider newQuery()
 * @method static Builder|MainSlider onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider query()
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider whereImageUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|MainSlider withTranslations($locales = null, $fallback = true)
 * @method static Builder|MainSlider withTrashed()
 * @method static Builder|MainSlider withoutTrashed()
 * @mixin Eloquent
 * @property-read mixed $image
 */
class MainSlider extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;
    protected $translatable = ['title', 'description'];
    protected $appends = ['image'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'id',
        'image_uri'

    ];

    public function getImageAttribute(){
        return Storage::disk(config('voyager.storage.disk'))->url($this->image_uri);
    }
}
