<?php

namespace App\Models;

use App\Casts\FloatCaster;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Carbon;

/**
 * App\Models\DoctorService
 *
 * @property int $id
 * @property int $doctor_user_id
 * @property int $service_id
 * @property bool $is_exclusive Эксклюзивный
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static Builder|DoctorService newModelQuery()
 * @method static Builder|DoctorService newQuery()
 * @method static Builder|DoctorService query()
 * @method static Builder|DoctorService whereCreatedAt($value)
 * @method static Builder|DoctorService whereDeletedAt($value)
 * @method static Builder|DoctorService whereDoctorUserId($value)
 * @method static Builder|DoctorService whereId($value)
 * @method static Builder|DoctorService whereIsExclusive($value)
 * @method static Builder|DoctorService whereServiceId($value)
 * @method static Builder|DoctorService whereUpdatedAt($value)
 * @mixin Eloquent
 * @property FloatCaster|null $service_percent % от услуги
 * @property string|null $start_date Дата начала действия (оказания услуги)
 * @property int|null $form_of_participation_id Форма участия (оказывает, направляет)
 * @property int|null $status Статус (действует, прекращено)
 * @property FloatCaster|null $sum_of Сумма
 * @method static Builder|DoctorService whereFormOfParticipationId($value)
 * @method static Builder|DoctorService whereServicePercent($value)
 * @method static Builder|DoctorService whereStartDate($value)
 * @method static Builder|DoctorService whereStatus($value)
 * @method static Builder|DoctorService whereSumOf($value)
 */
class DoctorService extends Pivot
{

    protected $fillable = [
        'doctor_user_id', 'service_id',
        'form_of_participation_id', 'service_percent', 'start_date',
        'sum_of', 'status'
    ];
    /**
     * Делаю каст по дефолту там делает round с длинной на 2
     * @var string[]
     */
    protected $casts = [
        'sum_of' => FloatCaster::class,
        'service_percent' => FloatCaster::class
    ];
}
