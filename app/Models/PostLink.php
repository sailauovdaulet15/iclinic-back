<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PostLink extends Model
{
    use HasFactory;

    protected $fillable = [
        'amount',
        'currency',
        'reference',
        'code',
        'secure',
        'accountId',
        'invoiceId',
        'name',
        'email',
        'phone',
        'issuer',
        'cardMask',
    ];

    public function appointment(): BelongsTo
    {
        return $this->belongsTo(Appointment::class, 'invoiceId', 'invoiceId');
    }
}
