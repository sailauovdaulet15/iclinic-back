<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MedicineCardFile extends Model
{
    use HasFactory;

    protected $fillable = [
        'medicine_card_id',
        'file_name',
        'file_path',
        'is_read'
    ];

    protected $appends = [
        'file'
    ];

    public function medicineCard(): BelongsTo
    {
        return $this->belongsTo(MedicineCard::class);
    }

    public function getFileAttribute(): string
    {
        return \Storage::disk(config('voyager.storage.disk'))->url($this->file_path);
    }
}
