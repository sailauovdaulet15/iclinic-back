<?php

namespace App\Models;

use App\Enums\BookStatusEnum;
use App\Models\Admin\AdminDoctor;
use App\Services\Appointment\ParseSlotService;
use Carbon\CarbonInterval;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Validation\ValidationException;

/**
 * App\Models\Visit
 *
 * @property int         $id
 * @property string      $date
 * @property string      $start_time
 * @property string      $end_time
 * @property int|null    $cabinet_num
 * @property bool        $time_mode
 * @property bool        $is_active
 * @property int         $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Visit newModelQuery()
 * @method static Builder|Visit newQuery()
 * @method static Builder|Visit query()
 * @method static Builder|Visit whereCabinetNum($value)
 * @method static Builder|Visit whereCreatedAt($value)
 * @method static Builder|Visit whereDate($value)
 * @method static Builder|Visit whereEndTime($value)
 * @method static Builder|Visit whereId($value)
 * @method static Builder|Visit whereIsActive($value)
 * @method static Builder|Visit whereStartTime($value)
 * @method static Builder|Visit whereTimeMode($value)
 * @method static Builder|Visit whereUpdatedAt($value)
 * @method static Builder|Visit whereUserId($value)
 * @mixin Eloquent
 * @property string|null     $uid
 * @property int             $duration Продолжительность берется от врача (booted в модели Visit)
 * @property mixed           $data
 * @property-read Doctor     $doctor
 * @property-read array|null $interval_slots
 * @method static Builder|Visit byDate($date)
 * @method static Builder|Visit whereData($value)
 * @method static Builder|Visit whereDuration($value)
 * @method static Builder|Visit whereUid($value)
 */
class Visit extends Model
{
    use HasFactory;

    protected $fillable = [
        'data', 'user_id', 'uid'
    ];

//    protected $appends = [
//        'intervalSlots'
//    ];

    protected static function booted()
    {
        parent::boot();

        static::creating(function ($model) {
            // todo create observer
            try {
                if ($duration = AdminDoctor::where('user_id', $model->user_id)->pluck('duration')) {
                    $model->duration = $duration[0];
                }
            } catch (\Exception $e) {
                throw new \Exception("user doesn't have duration $model->user_id");
                //throw ValidationException::withMessages(['user_id' => "user doesn't have duration $model->user_id"]);
            }
        });
    }

    public function getFreeSlotsCount()
    {
        return array_reduce($this->interval_slots, function ($count, $item) {
            $count += $item['status'];
            return $count;
        });
    }

    public function getIntervalSlotsAttribute(): ?array
    {
        $service = new ParseSlotService($this);

        return $service->takeVisitByDate();
    }

    public function scopeByDate($query, $date)
    {
        // todo сделать форматирование по такому типу ____-__-__ поиск по базе
        $str = "%\"date\": \"$date\"%";
        return $query->where('data', 'like', $str);
    }


    public function doctor()
    {
        return $this->belongsTo(Doctor::class, 'user_id', 'user_id');
    }

}
