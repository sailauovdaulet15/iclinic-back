<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AppointmentStatus
 *
 * @method static Builder|AppointmentStatus newModelQuery()
 * @method static Builder|AppointmentStatus newQuery()
 * @method static Builder|AppointmentStatus query()
 * @mixin Eloquent
 */
class AppointmentStatus extends Model
{
    use HasFactory;
}
