<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\Main
 *
 * @property int                                                         $id
 * @property string                                                      $type
 * @property string|null                                                 $title
 * @property string|null                                                 $next_page_title
 * @property string|null                                                 $description
 * @property string|null                                                 $image_uri
 * @property int                                                         $position Позиция
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|Main newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Main newQuery()
 * @method static Builder|Main onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Main query()
 * @method static \Illuminate\Database\Eloquent\Builder|Main sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|Main sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|Main whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Main whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Main whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Main whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Main whereImageUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Main whereNextPageTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Main wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Main whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Main whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|Main whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Main whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Main withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|Main withTranslations($locales = null, $fallback = true)
 * @method static Builder|Main withTrashed()
 * @method static Builder|Main withoutTrashed()
 * @mixin Eloquent
 * @property-read string $d_b_table_name
 */
class Main extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;
    protected $translatable = ['title', 'description', 'next_page_title'];


    /**
     * Форматируем для того чтобы брать как имя таблицы
     * @return string
     */
    public function getDBTableNameAttribute(): string
    {
        return 'main_' . Str::slug($this->type, '_');
    }
}
