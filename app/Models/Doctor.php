<?php

namespace App\Models;

use App\Scopes\VisibleScope;
use Database\Factories\DoctorFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;
use Voyager;

/**
 * App\Models\Doctor
 *
 * @property-read null                                            $translated
 * @property-read Service                                         $service
 * @property-read \Kalnoy\Nestedset\Collection|Service[]          $services
 * @property-read int|null                                        $services_count
 * @property-write mixed                                          $disable_position_update
 * @property-write mixed                                          $position_column
 * @property-read Collection|Translation[]                        $translations
 * @property-read int|null                                        $translations_count
 * @property-read User                                            $user
 * @property int                                                  $user_id
 * @property string|null                                          $category   Категория
 * @property int|null                                             $experience Стаж
 * @property string|null                                          $heals      Лечит кого
 * @property string|null                                          $about
 * @property array|null                                           $link
 * @property array|null                                           $work_experience
 * @property array|null                                           $education_experience
 * @property int                                                  $position
 * @property int                                                  $service_id
 * @property Carbon|null                                          $deleted_at
 * @property Carbon|null                                          $created_at
 * @property Carbon|null                                          $updated_at
 * @method static Builder|Doctor whereAbout($value)
 * @method static Builder|Doctor whereCategory($value)
 * @method static Builder|Doctor whereCreatedAt($value)
 * @method static Builder|Doctor whereDeletedAt($value)
 * @method static Builder|Doctor whereEducationExperience($value)
 * @method static Builder|Doctor whereExperience($value)
 * @method static Builder|Doctor whereHeals($value)
 * @method static Builder|Doctor whereLink($value)
 * @method static Builder|Doctor wherePosition($value)
 * @method static Builder|Doctor whereServiceId($value)
 * @method static Builder|Doctor whereUpdatedAt($value)
 * @method static Builder|Doctor whereUserId($value)
 * @method static Builder|Doctor whereWorkExperience($value)
 * @method static DoctorFactory factory(...$parameters)
 * @method static Builder|Doctor newModelQuery()
 * @method static Builder|Doctor newQuery()
 * @method static \Illuminate\Database\Query\Builder|Doctor onlyTrashed()
 * @method static Builder|Doctor query()
 * @method static Builder|Doctor sorted($way = 'ASC')
 * @method static Builder|Doctor sortedByDESC()
 * @method static Builder|Doctor withTranslation($locale = null, $fallback = true)
 * @method static Builder|Doctor withTranslations($locales = null, $fallback = true)
 * @method static \Illuminate\Database\Query\Builder|Doctor withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Doctor withoutTrashed()
 * @mixin Eloquent
 * @property bool                                                 $is_visible
 * @property string|null                                          $active_status
 * @property int|null                                             $agent_percent
 * @property int|null                                             $duration
 * @property string|null                                          $image_uri
 * @property int|null                                             $handbook_heal_category_id
 * @property int                                                  $specialisation_id
 * @property-read string                                          $first_second_name
 * @property-read string                                          $user_name
 * @property-read HandbookHealCategory|null                       $handbookHealCategory
 * @property-read Collection|Review[]                             $reviews
 * @property-read int|null                                        $reviews_count
 * @property-read Specialisation                                  $specialisation
 * @method static Builder|Doctor whereActiveStatus($value)
 * @method static Builder|Doctor whereAgentPercent($value)
 * @method static Builder|Doctor whereDuration($value)
 * @method static Builder|Doctor whereHandbookHealCategoryId($value)
 * @method static Builder|Doctor whereImageUri($value)
 * @method static Builder|Doctor whereIsVisible($value)
 * @method static Builder|Doctor whereSpecialisationId($value)
 * @property string|null                                          $about2
 * @property-read string                                          $image
 * @property-read string                                          $user_slug
 * @property-read \Kalnoy\Nestedset\Collection|RecommendService[] $recommendServices
 * @property-read int|null                                        $recommend_services_count
 * @property-read Collection|Specialisation[]                     $specialisations
 * @property-read int|null                                        $specialisations_count
 * @property-read Collection|Visit[]                              $visits
 * @property-read int|null                                        $visits_count
 * @method static Builder|Doctor reviewWithoutDoctors()
 * @method static Builder|Doctor whereAbout2($value)
 * @method static Builder|Doctor orderByName()
 * @property-read Collection|HistoryVisit[]                       $historyVisits
 * @property-read int|null                                        $history_visits_count
 * @method static Builder|Doctor visibleOnSite()
 * @property-read array|null $embedded_links
 * @method static Builder|Doctor whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 */
class Doctor extends Model
{
    use HasFactory, Translatable, PositionTrait;

    protected $primaryKey = 'user_id';
    protected $translatable = ['about', 'about2'];

    public $defaultImagePath = 'assets/images/doctor.png';

    public $additional_attributes = ['userName', 'firstSecondName', 'userSlug'];
    protected $fillable = [
        'experience',
        'is_visible',
        'active_status',
        'agent_percent',
        'duration',
        'category',
        'user_id',
        'handbook_heal_category_id',
        'user_id',
    ];

    protected $guarded = [];


    public function getNameAndSpecialisationAttribute(): string
    {
        $specialisationLabels = $this->specialisations->pluck('title')->implode(', ');
        return $specialisationLabels .' '. $this->firstSecondName;
    }

    public function getImageAttribute(): string
    {
        return $this->image_uri !== null ? Voyager::image($this->image_uri) : asset($this->defaultImagePath);
    }

    public function getUserSlugAttribute(): string
    {
        return $this->user->slug;
    }

    public function getUserNameAttribute(): string
    {
        return $this->user->name;
    }

    public function getFirstSecondNameAttribute(): string
    {
        return "{$this->user->second_name} {$this->user->first_name}";
    }

    public function getEmbeddedLinksAttribute(): ?array
    {
        $result = optional(json_decode($this->link))->rows;
        if ($result !== null) {
            foreach ($result as &$row) {
                $row->value = 'https://www.youtube.com/embed/' . substr($row->value,
                        strpos($row->value, '=') + 1);
            }
        }

        return $result;
    }

    public function appointments(): HasMany
    {
        return $this->hasMany(Appointment::class, 'doctor_user_id', 'user_id');
    }

    public function specialisations(): BelongsToMany
    {
        return $this->belongsToMany(Specialisation::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function services(): BelongsToMany
    {
        return $this->belongsToMany(Service::class);
    }

    public function visits(): HasMany
    {
        return $this->hasMany(Visit::class, 'user_id', 'user_id')
            ->orderBy('date');
    }

    public function historyVisits(): HasMany
    {
        return $this->hasMany(HistoryVisit::class, 'doctor_user_id', 'user_id');
    }

    public function recommendServices(): BelongsToMany
    {
        return $this->belongsToMany(
            RecommendService::class,
            'doctor_recommend_service', 'doctor_user_id', 'recommend_service_id'
        );
    }

    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class);
    }

    public function handbookHealCategory(): BelongsTo
    {
        return $this->belongsTo(HandbookHealCategory::class);
    }

    public function scopeReviewWithoutDoctors(Builder $query): Builder
    {
        return $query->whereHas('reviews', fn($q) => $q->doesntHave('doctor'));
    }

    public function scopeOrderByName(Builder $query): Builder
    {
        return $query
            ->join('users', 'users.id', '=', 'doctors.user_id')
            ->orderBy('users.second_name');
    }

    public function scopeVisibleOnSite(Builder $query): Builder
    {
        return $query->where('is_visible', true);
    }

    protected static function booted()
    {
        parent::boot();
        static::addGlobalScope(new VisibleScope());
    }
}
