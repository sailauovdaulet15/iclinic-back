<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\HandbookHealCategory
 *
 * @property int                                                         $id
 * @property string                                                      $type
 * @property string                                                      $title
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookHealCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookHealCategory newQuery()
 * @method static Builder|HandbookHealCategory onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookHealCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookHealCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookHealCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookHealCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookHealCategory whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookHealCategory whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookHealCategory whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookHealCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookHealCategory withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookHealCategory withTranslations($locales = null, $fallback = true)
 * @method static Builder|HandbookHealCategory withTrashed()
 * @method static Builder|HandbookHealCategory withoutTrashed()
 * @mixin Eloquent
 */
class HandbookHealCategory extends Model
{
    use HasFactory, Translatable, SoftDeletes;
}
