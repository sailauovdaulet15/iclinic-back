<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\HandbookOrderStatus
 *
 * @property int                                                         $id
 * @property string                                                      $name
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property string                                                      $type
 * @property-read null                                                   $translated
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static Builder|HandbookOrderStatus newModelQuery()
 * @method static Builder|HandbookOrderStatus newQuery()
 * @method static Builder|HandbookOrderStatus query()
 * @method static Builder|HandbookOrderStatus whereCreatedAt($value)
 * @method static Builder|HandbookOrderStatus whereId($value)
 * @method static Builder|HandbookOrderStatus whereName($value)
 * @method static Builder|HandbookOrderStatus whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|HandbookOrderStatus whereType($value)
 * @method static Builder|HandbookOrderStatus whereUpdatedAt($value)
 * @method static Builder|HandbookOrderStatus withTranslation($locale = null, $fallback = true)
 * @method static Builder|HandbookOrderStatus withTranslations($locales = null, $fallback = true)
 * @mixin Eloquent
 */
class HandbookOrderStatus extends Model
{
    use HasFactory, Translatable;

    protected $fillable = [
        'name'
    ];

    protected $translatable = [
        'name'
    ];
}
