<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\AboutUs
 *
 * @property int                                                         $id
 * @property string                                                      $key
 * @property string                                                      $title
 * @property string|null                                                 $image_uri
 * @property int                                                         $position Позиция
 * @property int                                                         $is_active Активный
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs newQuery()
 * @method static Builder|AboutUs onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs query()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs whereImageUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUs withTranslations($locales = null, $fallback = true)
 * @method static Builder|AboutUs withTrashed()
 * @method static Builder|AboutUs withoutTrashed()
 * @mixin Eloquent
 * @property-read string $db_table_name
 * @property-read mixed $image
 */
class AboutUs extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;
    protected $translatable = ['title'];

    /**
     * Форматируем, для того чтобы брать как имя таблицы
     * @return string
     */
    public function getDbTableNameAttribute(): string
    {
        return 'about_us_' . Str::slug($this->key, '_');
    }

    public function getImageAttribute(){
        return Storage::disk(config('voyager.storage.disk'))->url($this->attributes['image_uri']);
    }
}
