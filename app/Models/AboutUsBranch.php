<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\AboutUsBranch
 *
 * @property int                                                         $id
 * @property string                                                      $image_uri
 * @property string                                                      $title
 * @property string                                                      $description
 * @property int                                                         $about_us_id
 * @property int                                                         $position Позиция
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch newQuery()
 * @method static Builder|AboutUsBranch onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch query()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch whereAboutUsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch whereImageUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsBranch withTranslations($locales = null, $fallback = true)
 * @method static Builder|AboutUsBranch withTrashed()
 * @method static Builder|AboutUsBranch withoutTrashed()
 * @mixin Eloquent
 * @property-read mixed $image
 */
class AboutUsBranch extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;
    protected $translatable = ['title', 'description'];

    protected $appends = ['image'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'id',
        'image_uri'
    ];


    public function getImageAttribute(){
        return Storage::disk(config('voyager.storage.disk'))->url($this->attributes['image_uri']);
    }
}
