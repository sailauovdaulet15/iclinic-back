<?php

namespace App\Models;

use App\Enums\AppointmentStatusEnum;
use App\Soap\SoapService;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Kalnoy\Nestedset\Collection;

/**
 * App\Models\Appointment
 *
 * @property int                         $id
 * @property string                      $appointment_date Дата приема
 * @property string                      $appointment_time Время приема
 * @property int                         $bonus_spent      Потрачено Бонусов
 * @property bool                        $is_online_payment
 * @property int                         $status_id
 * @property int                         $patient_id
 * @property Carbon|null                 $created_at
 * @property Carbon|null                 $updated_at
 * @method static Builder|Appointment newModelQuery()
 * @method static Builder|Appointment newQuery()
 * @method static Builder|Appointment query()
 * @method static Builder|Appointment whereAppointmentDate($value)
 * @method static Builder|Appointment whereAppointmentTime($value)
 * @method static Builder|Appointment whereBonusSpent($value)
 * @method static Builder|Appointment whereCreatedAt($value)
 * @method static Builder|Appointment whereId($value)
 * @method static Builder|Appointment whereIsOnlinePayment($value)
 * @method static Builder|Appointment wherePatientId($value)
 * @method static Builder|Appointment whereStatusId($value)
 * @method static Builder|Appointment whereUpdatedAt($value)
 * @mixin Eloquent
 * @property int|null                    $cabinet_num
 * @property string                      $date             Дата приема
 * @property string                      $start_time       Время начала приема
 * @property string                      $end_time         Время окончания приема
 * @property int                         $status
 * @property int                         $doctor_user_id
 * @property string|null                 $uid
 * @property-read AppointmentStatus|null $appointmentStatus
 * @property-read Doctor                 $doctor
 * @property-read mixed                  $created_at_time
 * @property-read mixed                  $doctor_full_name
 * @property-read mixed                  $patient_full_name
 * @property-read Collection|Service[]   $services
 * @property-read int|null               $services_count
 * @property-read User                   $user
 * @method static Builder|Appointment canceled()
 * @method static Builder|Appointment booked()
 * @method static Builder|Appointment isOnline()
 * @method static Builder|Appointment whereCabinetNum($value)
 * @method static Builder|Appointment whereDate($value)
 * @method static Builder|Appointment whereDoctorUserId($value)
 * @method static Builder|Appointment whereEndTime($value)
 * @method static Builder|Appointment whereStartTime($value)
 * @method static Builder|Appointment whereStatus($value)
 * @method static Builder|Appointment whereUid($value)
 */
class Appointment extends Model
{
    use HasFactory;

    protected $casts = [
        'created_at' => 'timestamp',
    ];


    protected $fillable = [
        'start_time',
        'end_time',
        'date',
        'uid',
        'created_at',
        'cabinet_num',
        'patient_id',
        'doctor_user_id',
        'invoiceId',
        'status',
        'is_online_payment'
    ];

    public $additional_attributes = [
        'isOnlineConsultation'
    ];

    public function appointmentStatus(): BelongsTo
    {
        return $this->belongsTo(AppointmentStatus::class, 'status_id', 'id');
    }

    public function scopeCanceled($query)
    {
        return $query->where('status', AppointmentStatusEnum::CANCELLED);
    }

    public function scopeBooked($query)
    {
        return $query->where('status', AppointmentStatusEnum::BOOKED);
    }

    public function scopeIsOnline($query)
    {
        return $query->where('is_online_payment', true);
    }


    /*************RELATIONSHIPS*************/

    public function doctor(): BelongsTo
    {
        return $this->belongsTo(Doctor::class, 'doctor_user_id', 'user_id');
    }

    public function services(): BelongsToMany
    {
        return $this->belongsToMany(Service::class, 'appointment_service');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'patient_id');
    }

    public function postLink(): BelongsTo
    {
        return $this->belongsTo(PostLink::class, 'invoiceId', 'invoiceId');
    }

    public function medicineCards(): HasMany
    {
        return $this->hasMany(MedicineCard::class);
    }
    /*************MUTATORS*************/

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function setStartTimeAttribute($value)
    {
        $this->attributes['start_time'] = Carbon::parse($value)->format('H:i');
    }


    public function setEndTimeAttribute($value)
    {
        $this->attributes['end_time'] = Carbon::parse($value)->format('H:i');
    }

    /*************Accessors*************/

    public function getDateAttribute()
    {
        return $this->attributes['date'] ? Carbon::parse($this->attributes['date'])->format('Y-m-d') : '';
    }

    public function getStartTimeAttribute()
    {
        return $this->attributes['start_time'] ? Carbon::parse($this->attributes['start_time'])->format('H:i') : '';
    }


    public function getEndTimeAttribute()
    {
        return $this->attributes['end_time'] ? Carbon::parse($this->attributes['end_time'])->format('H:i') : '';
    }

    public function getCreatedAtTimeAttribute()
    {
        return $this->getRawOriginal('created_at');
    }

    public function getPatientFullNameAttribute()
    {
        return !empty($this->user->full_name) ? $this->user->full_name : '';
    }

    public function getDoctorFullNameAttribute()
    {
        return !empty($this->doctor->user->full_name) ? $this->doctor->user->full_name : '';
    }

    public function getIsOnlineConsultationAttribute(): bool
    {
        return $this->services()->where('is_online', false)->doesntExist();
    }

    public function cancel($withSoap = false): void
    {
        // todo захандлить ошибку
        try {
            DB::transaction(function () use ($withSoap) {
                $this->status = AppointmentStatusEnum::CANCELLED;

                if ($withSoap) {
                    app(SoapService::class)->cancelAppointment($this);
                }
                CanceledAppointment::create([
                    'appointment_id' => $this->id,
                ]);
                $this->save();
            });
        } catch (\Throwable $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }
    }

}
