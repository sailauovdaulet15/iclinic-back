<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\ServicePointsCategory
 *
 * @property int                                                              $id
 * @property string                                                           $type
 * @property string                                                           $title
 * @property Carbon|null                                  $deleted_at
 * @property Carbon|null                                  $created_at
 * @property Carbon|null                                  $updated_at
 * @property-read Collection|MainServicePoint[] $servicePoints
 * @property-read int|null                                                    $service_points_count
 * @method static Builder|ServicePointsCategory newModelQuery()
 * @method static Builder|ServicePointsCategory newQuery()
 * @method static \Illuminate\Database\Query\Builder|ServicePointsCategory onlyTrashed()
 * @method static Builder|ServicePointsCategory query()
 * @method static Builder|ServicePointsCategory whereCreatedAt($value)
 * @method static Builder|ServicePointsCategory whereDeletedAt($value)
 * @method static Builder|ServicePointsCategory whereId($value)
 * @method static Builder|ServicePointsCategory whereTitle($value)
 * @method static Builder|ServicePointsCategory whereType($value)
 * @method static Builder|ServicePointsCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|ServicePointsCategory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ServicePointsCategory withoutTrashed()
 * @method static Builder|ServicePointsCategory withServicePointsLanding()
 * @mixin Eloquent
 */
class ServicePointsCategory extends Model
{
    use HasFactory, SoftDeletes;

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'id'
    ];

    // -------relationships-------
    public function servicePoints()
    {
        return $this->hasMany(MainServicePoint::class, 'category_id', 'id');
    }


}
