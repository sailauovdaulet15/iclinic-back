<?php

namespace App\Models;

use App\Scopes\HasDoctorScope;
use App\Scopes\HasSpecialisationScope;
use App\Scopes\ServiceHasPriceScope;
use App\Scopes\ServiceHasTitleScope;
use App\Scopes\VisibleScope;
use App\Traits\SluggableTrait;
use Database\Factories\ServiceFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Kalnoy\Nestedset\NodeTrait;
use Kalnoy\Nestedset\QueryBuilder;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\Service
 *
 * @property int                                         $id
 * @property string                                      $title
 * @property string|null                                 $description
 * @property string|null                                 $link
 * @property int|null                                    $price_original
 * @property int|null                                    $price_percent
 * @property int|null                                    $price_bonus
 * @property int                                         $_lft
 * @property int                                         $_rgt
 * @property int|null                                    $parent_id
 * @property Carbon|null                                 $created_at
 * @property Carbon|null                                 $updated_at
 * @property Carbon|null                                 $deleted_at
 * @property-read \Kalnoy\Nestedset\Collection|Service[] $children
 * @property-read int|null                               $children_count
 * @property-read mixed                                  $price_with_discount
 * @property-read null                                   $translated
 * @property-read Service|null                           $parent
 * @property-read Collection|RecommendService[]          $recommendServices
 * @property-read int|null                               $recommend_services_count
 * @property-read Collection|Translation[]               $translations
 * @property-read int|null                               $translations_count
 * @method static \Kalnoy\Nestedset\Collection|static[] all($columns = ['*'])
 * @method static QueryBuilder|Service ancestorsAndSelf($id, array $columns = [])
 * @method static QueryBuilder|Service ancestorsOf($id, array $columns = [])
 * @method static QueryBuilder|Service applyNestedSetScope(?string $table = null)
 * @method static QueryBuilder|Service countErrors()
 * @method static QueryBuilder|Service d()
 * @method static QueryBuilder|Service defaultOrder(string $dir = 'asc')
 * @method static QueryBuilder|Service descendantsAndSelf($id, array $columns = [])
 * @method static QueryBuilder|Service descendantsOf($id, array $columns = [], $andSelf = false)
 * @method static QueryBuilder|Service fixSubtree($root)
 * @method static QueryBuilder|Service fixTree($root = null)
 * @method static \Kalnoy\Nestedset\Collection|static[] get($columns = ['*'])
 * @method static QueryBuilder|Service getNodeData($id, $required = false)
 * @method static QueryBuilder|Service getPlainNodeData($id, $required = false)
 * @method static QueryBuilder|Service getTotalErrors()
 * @method static QueryBuilder|Service hasChildren()
 * @method static QueryBuilder|Service hasParent()
 * @method static QueryBuilder|Service isBroken()
 * @method static QueryBuilder|Service leaves(array $columns = [])
 * @method static QueryBuilder|Service makeGap(int $cut, int $height)
 * @method static QueryBuilder|Service moveNode($key, $position)
 * @method static QueryBuilder|Service newModelQuery()
 * @method static QueryBuilder|Service newQuery()
 * @method static \Illuminate\Database\Query\Builder|Service onlyTrashed()
 * @method static QueryBuilder|Service orWhereAncestorOf(bool $id, bool $andSelf = false)
 * @method static QueryBuilder|Service orWhereDescendantOf($id)
 * @method static QueryBuilder|Service orWhereNodeBetween($values)
 * @method static QueryBuilder|Service orWhereNotDescendantOf($id)
 * @method static QueryBuilder|Service query()
 * @method static QueryBuilder|Service rebuildSubtree($root, array $data, $delete = false)
 * @method static QueryBuilder|Service rebuildTree(array $data, $delete = false, $root = null)
 * @method static QueryBuilder|Service reversed()
 * @method static QueryBuilder|Service root(array $columns = [])
 * @method static QueryBuilder|Service whereAncestorOf($id, $andSelf = false, $boolean = 'and')
 * @method static QueryBuilder|Service whereAncestorOrSelf($id)
 * @method static QueryBuilder|Service whereCreatedAt($value)
 * @method static QueryBuilder|Service whereDeletedAt($value)
 * @method static QueryBuilder|Service whereDescendantOf($id, $boolean = 'and', $not = false, $andSelf = false)
 * @method static QueryBuilder|Service whereDescendantOrSelf(string $id, string $boolean = 'and', string $not = false)
 * @method static QueryBuilder|Service whereDescription($value)
 * @method static QueryBuilder|Service whereId($value)
 * @method static QueryBuilder|Service whereIsAfter($id, $boolean = 'and')
 * @method static QueryBuilder|Service whereIsBefore($id, $boolean = 'and')
 * @method static QueryBuilder|Service whereIsLeaf()
 * @method static QueryBuilder|Service whereIsRoot()
 * @method static QueryBuilder|Service whereLft($value)
 * @method static QueryBuilder|Service whereLink($value)
 * @method static QueryBuilder|Service whereNodeBetween($values, $boolean = 'and', $not = false)
 * @method static QueryBuilder|Service whereNotDescendantOf($id)
 * @method static QueryBuilder|Service whereParentId($value)
 * @method static QueryBuilder|Service wherePriceBonus($value)
 * @method static QueryBuilder|Service wherePriceOriginal($value)
 * @method static QueryBuilder|Service wherePricePercent($value)
 * @method static QueryBuilder|Service whereRgt($value)
 * @method static QueryBuilder|Service whereTitle($value)
 * @method static QueryBuilder|Service whereUpdatedAt($value)
 * @method static QueryBuilder|Service withDepth(string $as = 'depth')
 * @method static QueryBuilder|Service withTranslation($locale = null, $fallback = true)
 * @method static QueryBuilder|Service withTranslations($locales = null, $fallback = true)
 * @method static \Illuminate\Database\Query\Builder|Service withTrashed()
 * @method static QueryBuilder|Service withoutRoot()
 * @method static \Illuminate\Database\Query\Builder|Service withoutTrashed()
 * @mixin Eloquent
 * @property-read ServiceLanding|null                    $serviceLanding
 * @method static QueryBuilder|Service roots()
 * @method static ServiceFactory factory(...$parameters)
 * @method static QueryBuilder|Service hasLanding()
 * @property-read Collection|Doctor[]                    $doctors
 * @property-read int|null                               $doctors_count
 * @property string|null                                 $annotation
 * @property int                                         $position Позиция
 * @property int                                         $order    Сортировка
 * @property bool                                        $is_visible
 * @property int                                         $specialisation_id
 * @property-read Service|null                           $parentId
 * @method static QueryBuilder|Service whereAnnotation($value)
 * @method static QueryBuilder|Service whereIsVisible($value)
 * @method static QueryBuilder|Service whereOrder($value)
 * @method static QueryBuilder|Service wherePosition($value)
 * @method static QueryBuilder|Service whereSpecialisationId($value)
 * @property-read Collection|ServiceLanding[]            $serviceLandings
 * @property-read int|null                               $service_landings_count
 * @property string|null                                 $uid
 * @property int|null                                    $reception_type_id
 * @property string                                      $slug
 * @property-read Collection|DoctorService[]             $doctorServices
 * @property-read int|null                               $doctor_services_count
 * @property-read Specialisation|null                    $specialisation
 * @property-read Collection|Stock[]                     $stocks
 * @property-read int|null                               $stocks_count
 * @method static QueryBuilder|Service exclusive(bool $is_exclusive = true)
 * @method static QueryBuilder|Service whereReceptionTypeId($value)
 * @method static QueryBuilder|Service whereSlug($value)
 * @method static QueryBuilder|Service whereUid($value)
 * @property-read Collection|Appointment[]               $appointments
 * @property-read int|null                               $appointments_count
 * @property-read Collection|HistoryVisit[]              $historyVisits
 * @property-read int|null                               $history_visits_count
 */
class Service extends Model
{
    use HasFactory, NodeTrait, Translatable, SluggableTrait;

    protected $translatable = ['title', 'slug', 'description', 'annotation'];
    protected $appends = [
        'price_with_discount',
    ];
    protected $additional_attributes = ['price_with_discount'];
    protected $fillable = [
        'uid',
        'title',
        'parent_id',
        'specialisation_id',
        'is_visible',
        'duration',
        'price_original',
        'is_online',
        'is_appointment'
    ];

    // fields to use as slug
    protected $sluggables = ['title'];

    // ----------Relationships----------

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function historyVisits(): BelongsToMany
    {
        return $this->belongsToMany(HistoryVisit::class, 'history_visit_service',
            'history_visit_id', 'service_id');
    }

    // For Voyager-Extension package
    public function parentId()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function recommendServices()
    {
        return $this->belongsToMany(RecommendService::class, 'recommend_services', 'service_id',
            'recommend_service_id');
    }

    public function specialisation()
    {
        return $this->belongsTo(Specialisation::class);
    }

    public function doctors()
    {
        return $this->belongsToMany(Doctor::class);
    }

    public function stocks(): HasMany
    {
        return $this->hasMany(Stock::class);
    }

    public function doctorServices(): HasMany
    {
        return $this->hasMany(DoctorService::class);
    }

    // ----------Accessors----------

    public function getPriceWithDiscountAttribute(): int
    {
        return round($this->price_original * ((100 - $this->price_percent) / 100));
    }

    // ----------Scopes----------

    /**
     * Scope a query where only roots.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeRoots(Builder $query)
    {
        return $query->whereNull('parent_id');
    }

    /**
     * Scope a query with exclusives
     *
     * @param Builder $query
     * @param bool    $is_exclusive
     *
     * @return mixed
     */
    public function scopeExclusive(Builder $query, bool $is_exclusive = true)
    {
        return $query
            ->join('doctor_service', 'services.id', '=', 'doctor_service.service_id')
            ->whereIsExclusive($is_exclusive);
    }

    protected static function booted()
    {
        parent::boot();

        static::addGlobalScope(new HasSpecialisationScope());
        //        static::addGlobalScope(new HasDoctorScope());
        static::addGlobalScope(new ServiceHasTitleScope());
        static::addGlobalScope(new ServiceHasPriceScope());
        static::addGlobalScope(new VisibleScope());
    }

    public function appointments(): BelongsToMany
    {
        return $this->belongsToMany(Appointment::class, 'appointment_service');
    }

}
