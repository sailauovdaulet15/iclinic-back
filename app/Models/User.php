<?php

namespace App\Models;

use App\Casts\PhoneCaster;
use App\Enums\RoleEnum;
use App\Scopes\HasSpecialisationScope;
use App\Scopes\OnlyActiveScope;
use App\Traits\SluggableTrait;
use Database\Factories\UserFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Laravel\Scout\Searchable;
use TCG\Voyager\Models\Role;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Voyager;

/**
 * App\Models\User
 *
 * @property int                                                        $id
 * @property string                                                     $name
 * @property string                                                     $email
 * @property Carbon|null                                                $email_verified_at
 * @property string                                                     $password
 * @property string|null                                                $remember_token
 * @property Carbon|null                                                $created_at
 * @property Carbon|null                                                $updated_at
 * @property string|null                                                $avatar
 * @property int|null                                                   $role_id
 * @property string|null                                                $settings
 * @property mixed                                                      $locale
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null                                              $notifications_count
 * @property-read Role|null                                             $role
 * @property-read Collection|Role[]                                     $roles
 * @property-read int|null                                              $roles_count
 * @method static UserFactory factory(...$parameters)
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereAvatar($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereRoleId($value)
 * @method static Builder|User whereSettings($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string|null                                                $phone
 * @property-read Doctor|null                                           $doctor
 * @method static Builder|User admins()
 * @method static Builder|User doctors()
 * @method static Builder|User users()
 * @method static Builder|User wherePhone($value)
 * @property string|null                                                $first_name
 * @property string|null                                                $second_name
 * @property string|null                                                $third_name
 * @property int|null                                                   $nationality_id
 * @property string|null                                                $sex
 * @property string|null                                                $language
 * @property bool                                                       $is_limited_by_mobility
 * @property string|null                                                $postcode
 * @property string|null                                                $phone2
 * @property string|null                                                $address
 * @property string|null                                                $date_birth
 * @property string|null                                                $country
 * @property string|null                                                $region
 * @property string|null                                                $city
 * @property int|null                                                   $legal_agent_id
 * @property int|null                                                   $general_discount Скидка на все услуги
 * @property int|null                                                   $bonus_count
 * @property bool                                                       $is_notification
 * @property bool                                                       $is_push_notification
 * @property int|null                                                   $handbook_blood_group_id
 * @property-read string                                                $full_name
 * @property-read User|null                                             $legalAgent
 * @property-read Nationality|null                                      $nationality
 * @method static Builder|User whereAddress($value)
 * @method static Builder|User whereBonusCount($value)
 * @method static Builder|User whereCity($value)
 * @method static Builder|User whereCountry($value)
 * @method static Builder|User whereDateBirth($value)
 * @method static Builder|User whereFirstName($value)
 * @method static Builder|User whereGeneralDiscount($value)
 * @method static Builder|User whereHandbookBloodGroupId($value)
 * @method static Builder|User whereIsLimitedByMobility($value)
 * @method static Builder|User whereIsNotification($value)
 * @method static Builder|User whereIsPushNotification($value)
 * @method static Builder|User whereLanguage($value)
 * @method static Builder|User whereLegalAgentId($value)
 * @method static Builder|User whereNationalityId($value)
 * @method static Builder|User wherePhone2($value)
 * @method static Builder|User wherePostcode($value)
 * @method static Builder|User whereRegion($value)
 * @method static Builder|User whereSecondName($value)
 * @method static Builder|User whereSex($value)
 * @method static Builder|User whereThirdName($value)
 * @method static Builder|User name($searchTerm)
 * @property-read Collection|ServiceLanding[]                           $serviceLandings
 * @property-read int|null                                              $service_landings_count
 * @method static Builder|User doctorToCreate()
 * @property int|null                                                   $country_id
 * @property string|null                                                $slug
 * @property-read string                                                $avatar_image
 * @property-read Collection|Visit[]                                    $visits
 * @property-read int|null                                              $visits_count
 * @method static Builder|User hasDoctorProfile()
 * @method static Builder|User whereCountryId($value)
 * @method static Builder|User whereSlug($value)
 * @property-read string                                                $formatted_phone2
 * @property-read string                                                $formatted_phone
 * @property-read HandbookBloodGroup|null                               $handbookBloodGroup
 * @property-read Collection|HistoryVisit[]                             $historyVisits
 * @property-read int|null                                              $history_visits_count
 * @property-read Mailing|null                                          $mailing
 * @property bool $is_active
 * @method static Builder|User whereIsActive($value)
 */
class User extends \TCG\Voyager\Models\User implements JWTSubject
{
    use HasFactory, Notifiable, SluggableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'role_id',
        'first_name',
        'second_name',
        'third_name',
        'nationality_id',
        'sex',
        'language',
        'is_limited_by_mobility',
        'postcode',
        'phone2',
        'address',
        'date_birth',
        'country_id',
        'region',
        'city',
        'legal_agent_id',
        'handbook_blood_group_id',
        'is_notification',
        'is_push_notification',
        'is_active',
    ];

    public $defaultImagePath = 'assets/images/user.png';

    protected array $sluggables = [
        'first_name',
        'second_name',
        'third_name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone'             => PhoneCaster::class,
    ];

    public $additional_attributes = ['full_name'];

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'id'        => $this->getKey(),
            'full_name' => $this->full_name,
        ];
    }

    // ----------RELATIONSHIPS----------

    public function nationality(): BelongsTo
    {
        return $this->belongsTo(Nationality::class);
    }

    public function legalAgent(): BelongsTo
    {
        return $this->belongsTo(self::class, 'legal_agent_id', 'id');
    }

    public function doctor(): HasOne
    {
        return $this->hasOne(Doctor::class);
    }

    public function serviceLandings(): MorphToMany
    {
        return $this->morphToMany(ServiceLanding::class, 'service_landingable');
    }

    public function visits(): HasMany
    {
        return $this->hasMany(Visit::class);
    }

    public function mailing(): HasOne
    {
        return $this->hasOne(Mailing::class);
    }

    public function handbookBloodGroup(): BelongsTo
    {
        return $this->belongsTo(HandbookBloodGroup::class);
    }

    public function appointments(): HasMany
    {
        return $this->hasMany(Appointment::class, 'patient_id', 'id');
    }

    public function historyVisits(): HasMany
    {
        return $this->hasMany(HistoryVisit::class);
    }


    // ----------SCOPES----------

    public function scopeDoctors(Builder $query): Builder
    {
        return $query->whereHas('role', function ($q) {
            $q->where('name', RoleEnum::DOCTOR);
        });
    }

    public function scopeHasDoctorProfile(Builder $query): Builder
    {
        return $query->has('doctor');
    }

    public function scopeDoctorToCreate(Builder $query): Builder
    {
        return $query->doesntHave('doctor');
    }

    public function scopeUsers(Builder $query): Builder
    {
        return $query->whereHas('role', function ($q) {
            $q->where('name', RoleEnum::USER);
        });
    }

    public function scopeAdmins(Builder $query): Builder
    {
        return $query->whereHas('role', function ($q) {
            $q->where('name', RoleEnum::ADMIN);
        });
    }

    public function scopeName(Builder $query, string $searchTerm): Builder
    {
        $attributes = ['first_name', 'second_name', 'third_name'];

        return $query->where(function ($query) use ($attributes, $searchTerm) {
            foreach ($attributes as $attribute) {
                $query->orWhere($attribute, 'LIKE', $searchTerm);
            }
        });
    }

    // ----------Accessors----------

    public function getFullNameAttribute(): string
    {
        return mb_convert_case("{$this->second_name} {$this->first_name} {$this->third_name}", MB_CASE_TITLE, "UTF-8");
    }

    public function getAvatarImageAttribute(): string
    {
        return $this->avatar !== null ? Voyager::image($this->avatar) : asset($this->defaultImagePath);
    }

    public function getFormattedPhoneAttribute(): string
    {
        return "+7" . $this->phone;
    }

    public function getFormattedPhone2Attribute(): string
    {
        return "+7" . $this->phone2;
    }

    // ----------Mutators----------

    public function setEmailAttribute($value): void
    {
        if (empty($value)) { // will check for empty string
            $this->attributes['email'] = null;
        } else {
            $this->attributes['email'] = $value;
        }
    }

    public function setIsLimitedByMobilityAttribute($value): void
    {
        $this->attributes['is_limited_by_mobility'] = (bool) $value;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    /**
     * Get the name of the index associated with the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'users';
    }


    protected static function booted()
    {
        parent::boot();

        static::addGlobalScope(new OnlyActiveScope());
    }
}
