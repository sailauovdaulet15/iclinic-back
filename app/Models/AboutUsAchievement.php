<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\AboutUsAchievement
 *
 * @property int                                                         $id
 * @property string                                                      $title
 * @property string                                                      $description
 * @property int                                                         $about_us_id
 * @property int                                                         $position Позиция
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement newQuery()
 * @method static Builder|AboutUsAchievement onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement query()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement whereAboutUsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsAchievement withTranslations($locales = null, $fallback = true)
 * @method static Builder|AboutUsAchievement withTrashed()
 * @method static Builder|AboutUsAchievement withoutTrashed()
 * @mixin Eloquent
 */
class AboutUsAchievement extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;
    protected $translatable = ['title', 'description'];
}
