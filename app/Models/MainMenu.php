<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\MainMenu
 *
 * @property int                                                         $id
 * @property string                                                      $title Название
 * @property string                                                      $link_external_internal Ссылка (внешняя или внутреняя)
 * @property int                                                         $position
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static Builder|MainMenu newModelQuery()
 * @method static Builder|MainMenu newQuery()
 * @method static \Illuminate\Database\Query\Builder|MainMenu onlyTrashed()
 * @method static Builder|MainMenu query()
 * @method static Builder|MainMenu sorted($way = 'ASC')
 * @method static Builder|MainMenu sortedByDESC()
 * @method static Builder|MainMenu whereCreatedAt($value)
 * @method static Builder|MainMenu whereDeletedAt($value)
 * @method static Builder|MainMenu whereId($value)
 * @method static Builder|MainMenu whereLinkExternalInternal($value)
 * @method static Builder|MainMenu wherePosition($value)
 * @method static Builder|MainMenu whereTitle($value)
 * @method static Builder|MainMenu whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|MainMenu whereUpdatedAt($value)
 * @method static Builder|MainMenu withTranslation($locale = null, $fallback = true)
 * @method static Builder|MainMenu withTranslations($locales = null, $fallback = true)
 * @method static \Illuminate\Database\Query\Builder|MainMenu withTrashed()
 * @method static \Illuminate\Database\Query\Builder|MainMenu withoutTrashed()
 * @mixin Eloquent
 * @property string $href Ссылка (внешняя или внутреняя)
 * @method static Builder|MainMenu whereHref($value)
 */
class MainMenu extends Model
{
    use HasFactory, SoftDeletes, PositionTrait, Translatable;

    protected array $translatable = ['title'];


}
