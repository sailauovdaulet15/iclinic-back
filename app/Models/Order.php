<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Order
 *
 * @property int                             $id
 * @property int                             $user_id
 * @property string                          $order_number
 * @property int                             $order_price
 * @property string                          $date_of_payment
 * @property int                             $handbook_order_type_id
 * @property int                             $handbook_order_status_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property mixed $0
 * @property-read HandbookOrderStatus|null   $orderStatus
 * @property-read HandbookOrderType|null     $orderType
 * @method static Builder|Order newModelQuery()
 * @method static Builder|Order newQuery()
 * @method static Builder|Order query()
 * @method static Builder|Order whereCreatedAt($value)
 * @method static Builder|Order whereDateOfPayment($value)
 * @method static Builder|Order whereHandbookOrderStatusId($value)
 * @method static Builder|Order whereHandbookOrderTypeId($value)
 * @method static Builder|Order whereId($value)
 * @method static Builder|Order whereOrderNumber($value)
 * @method static Builder|Order whereOrderPrice($value)
 * @method static Builder|Order whereUpdatedAt($value)
 * @method static Builder|Order whereUserId($value)
 * @mixin Eloquent
 */
class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'order_number',
        'order_price',
        'handbook_order_type_id',
        'handbook_order_status_id',
    ];
    protected $casts = [
        'date_of_payment',
    ];

    public function orderType():BelongsTo
    {
        return $this->belongsTo(HandbookOrderType::class);
    }

    public function orderStatus():BelongsTo
    {
        return $this->belongsTo(HandbookOrderStatus::class);
    }
}
