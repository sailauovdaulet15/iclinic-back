<?php

namespace App\Models;

use Database\Factories\MainServicePointFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\MainServicePoint
 *
 * @property int                                                         $id
 * @property string                                                      $key
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static Builder|MainServicePoint newModelQuery()
 * @method static Builder|MainServicePoint newQuery()
 * @method static \Illuminate\Database\Query\Builder|MainServicePoint onlyTrashed()
 * @method static Builder|MainServicePoint query()
 * @method static Builder|MainServicePoint whereCreatedAt($value)
 * @method static Builder|MainServicePoint whereDeletedAt($value)
 * @method static Builder|MainServicePoint whereDescription($value)
 * @method static Builder|MainServicePoint whereId($value)
 * @method static Builder|MainServicePoint whereKey($value)
 * @method static Builder|MainServicePoint whereLink($value)
 * @method static Builder|MainServicePoint whereTitle($value)
 * @method static Builder|MainServicePoint whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|MainServicePoint whereUpdatedAt($value)
 * @method static Builder|MainServicePoint withTranslation($locale = null, $fallback = true)
 * @method static Builder|MainServicePoint withTranslations($locales = null, $fallback = true)
 * @method static \Illuminate\Database\Query\Builder|MainServicePoint withTrashed()
 * @method static \Illuminate\Database\Query\Builder|MainServicePoint withoutTrashed()
 * @mixin Eloquent
 * @property int                        $service_id
 * @property int                        $category_id
 * @property-read ServicePointsCategory $category
 * @property-read Service               $service
 * @method static Builder|MainServicePoint whereCategoryId($value)
 * @method static Builder|MainServicePoint whereServiceId($value)
 * @method static MainServicePointFactory factory(...$parameters)
 * @property string                     $title
 * @property string                     $description
 * @property string|null                $link
 * @property int                        $specialisation_id
 * @method static Builder|MainServicePoint whereSpecialisationId($value)
 * @property-read Specialisation        $specialisation
 * @property int                                    $position
 * @property-write mixed                            $disable_position_update
 * @property-write mixed                            $position_column
 * @method static Builder|MainServicePoint sorted($way = 'ASC')
 * @method static Builder|MainServicePoint sortedByDESC()
 * @method static Builder|MainServicePoint wherePosition($value)
 */
class MainServicePoint extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;
    protected $translatable = ['title', 'description'];


    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'id'
    ];

    // --------Relationships--------

    public function specialisation()
    {
        return $this->belongsTo(Specialisation::class);
    }

    public function category()
    {
        return $this->belongsTo(ServicePointsCategory::class, 'category_id', 'id');
    }

}
