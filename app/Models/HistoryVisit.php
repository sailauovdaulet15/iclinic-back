<?php

namespace App\Models;

use App\Repositories\Interfaces\SearchDoctorRepositoryInterface;
use Database\Factories\HistoryVisitFactory;
use Eloquent;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Kalnoy\Nestedset\Collection;

/**
 * App\Models\HistoryVisit
 *
 * @property int                                                                           $id
 * @property string                                                                        $uid
 * @property int                                                                           $doctor_user_id
 * @property int                                                                           $user_id
 * @property string                                                                        $date_of_receipt
 * @property Carbon|null                                                                   $created_at
 * @property Carbon|null                                                                   $updated_at
 * @property-read Doctor                                                                   $doctor
 * @property-read Collection|Service[]                                                     $services
 * @property-read int|null                                                                 $services_count
 * @property-read User                                                                     $user
 * @method static HistoryVisitFactory factory(...$parameters)
 * @method static Builder|HistoryVisit newModelQuery()
 * @method static Builder|HistoryVisit newQuery()
 * @method static Builder|HistoryVisit query()
 * @method static Builder|HistoryVisit whereCreatedAt($value)
 * @method static Builder|HistoryVisit whereDateOfReceipt($value)
 * @method static Builder|HistoryVisit whereDoctorUserId($value)
 * @method static Builder|HistoryVisit whereId($value)
 * @method static Builder|HistoryVisit whereUid($value)
 * @method static Builder|HistoryVisit whereUpdatedAt($value)
 * @method static Builder|HistoryVisit whereUserId($value)
 * @mixin Eloquent
 * @property int                                                                           $total_sum
 * @property int|null                                                                      $sum_bonus
 * @method static Builder|HistoryVisit whereSumBonus($value)
 * @method static Builder|HistoryVisit whereTotalSum($value)
 * @property string                                                                        $date
 * @property string|null                                                                   $room
 * @property string|null                                                                   $date_start
 * @property-read string                                                                   $href
 * @property-read string                                                                   $href_param
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\HandbookOrderType[] $payments
 * @property-read int|null                                                                 $payments_count
 * @method static Builder|HistoryVisit dateEnd($date)
 * @method static Builder|HistoryVisit dateStart($date)
 * @method static Builder|HistoryVisit doctorId($values)
 * @method static Builder|HistoryVisit serviceId($values)
 * @method static Builder|HistoryVisit sortByDoctorName($order)
 * @method static Builder|HistoryVisit sortByServiceTitle($order)
 * @method static Builder|HistoryVisit whereDate($value)
 * @method static Builder|HistoryVisit whereDateStart($value)
 * @method static Builder|HistoryVisit whereHasDoctorName($name)
 * @method static Builder|HistoryVisit whereRoom($value)
 */
class HistoryVisit extends Model
{
    use HasFactory;

    protected $fillable = [
        'doctor_user_id',
        'uid',
        'date',
        'user_id',
        'date_start',
        'room',
        'total_sum',
        'is_active'
    ];

    /*********RELATIONSHIPS*********/

    public function services(): BelongsToMany
    {
        return $this->belongsToMany(Service::class,
            'history_visit_services',
            'history_visit_id',
            'service_id')
            ->withPivot(['amount', 'sum']);
    }

    public function payments(): BelongsToMany
    {
        return $this->belongsToMany(HandbookOrderType::class,
            'history_visit_payments',
            'history_visit_id',
            'handbook_order_type_id')
            ->withPivot(['sum']);
    }

    public function medicineCard(): HasOne
    {
        return $this->hasOne(MedicineCard::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function doctor(): BelongsTo
    {
        return $this->belongsTo(Doctor::class, 'doctor_user_id', 'user_id');
    }

    /*********SCOPES*********/

    public function scopeActive(Builder $query, $active = true): Builder
    {
        return $query->where('is_active', $active);
    }

    public function scopeDateStart(Builder $query, $date): Builder
    {
        return $query->where('date', '>=', Carbon::parse($date));
    }


    public function scopeDateEnd(Builder $query, $date): Builder
    {
        return $query->where('date', '<=', Carbon::parse($date));
    }

    public function scopeConsultations(Builder $query): Builder
    {
        return $query->whereHas('services', function (Builder $query) {
            return $query->where('is_online', true);
        });
    }

    public function scopeActiveVisits(Builder $query): Builder
    {
        return $query
            ->where('date', '>=', now()->format('Y-m-d'));
        //->where('date_start', '>=', now()->format('Y-m-d H:i'));
    }

    public function scopeArchived(Builder $query): Builder
    {
        return $query
            ->where('date', '<', now()->format('Y-m-d'));
        //->where('date_start', '<', now()->format('Y-m-d H:i'));
    }

    /**
     * @throws BindingResolutionException
     */
    public function scopeWhereHasDoctorName(Builder $query, $name): Builder
    {
        $doctor_ids = app()->make(SearchDoctorRepositoryInterface::class)
            ->search($name)
            ->pluck('id');

        return $query->whereIn('doctor_user_id', $doctor_ids);
    }

    public function scopeServiceId(Builder $query, $values): Builder
    {
        if (!is_array($values)) {
            $values = [$values];
        }

        return $query->whereHas('services', function ($q) use ($values) {
            $q->whereIn('services.id', $values);
        });
    }

    public function scopeDoctorId(Builder $query, $values): Builder
    {
        if (!is_array($values)) {
            $values = [$values];
        }

        return $query->whereIn('doctor_user_id', $values);
    }

    public function scopeSortByServiceTitle(Builder $query, $order): Builder
    {
        return $query
            ->join('history_visit_service',
                "history_visits.id", "=", "history_visit_service.history_visit_id")
            ->join('services',
                "services.id", "=", "history_visit_service.service_id")
            ->groupBy('history_visit_service.id')
            ->orderBy('services.title', $order);
    }

    public function scopeSortByDoctorName(Builder $query, $order): Builder
    {
        return $query
            ->join('users',
                "history_visits.doctor_user_id", "=", "users.id")
            ->orderBy('users.first_name', $order)
            ->orderBy('users.second_name', $order)
            ->orderBy('users.third_name', $order);
    }

    public function scopeWhereHasSpecialisation(Builder $query, $specialisation_id): Builder
    {
        return $query->whereHas('services', function ($q) use ($specialisation_id) {
            $q->where('specialisation_id', $specialisation_id);
        });
    }

    public function getHrefParamAttribute(): string
    {
        return "key=history_visit_id&filter=equals&s=$this->uid";
    }

    public function getHrefAttribute(): string
    {
        return 'Перейти';
    }

    public function getCalculatedTotalAttribute(): string
    {
        return $this->services()->sum('sum');
    }

    public function getHasMedicineCardAttribute(): bool
    {
        return $this->medicineCard()->exists();
    }

    public function cancel()
    {
        try {
            DB::transaction(function () {
                $this->is_active = false;
                $this->save();
            });
        } catch (\Throwable $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage());
        }
    }
}
