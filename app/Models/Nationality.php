<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\Nationality
 *
 * @property-read null                                                   $translated
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static Builder|Nationality newModelQuery()
 * @method static Builder|Nationality newQuery()
 * @method static \Illuminate\Database\Query\Builder|Nationality onlyTrashed()
 * @method static Builder|Nationality query()
 * @method static Builder|Nationality whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|Nationality withTranslation($locale = null, $fallback = true)
 * @method static Builder|Nationality withTranslations($locales = null, $fallback = true)
 * @method static \Illuminate\Database\Query\Builder|Nationality withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Nationality withoutTrashed()
 * @property int $id
 * @property string $title
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @method static Builder|Nationality whereCreatedAt($value)
 * @method static Builder|Nationality whereDeletedAt($value)
 * @method static Builder|Nationality whereId($value)
 * @method static Builder|Nationality whereTitle($value)
 * @method static Builder|Nationality whereUpdatedAt($value)
 * @property int $code
 * @property string $code_alfa_2
 * @property string $code_alfa_3
 * @property string $full_title
 * @method static Builder|Nationality whereCode($value)
 * @method static Builder|Nationality whereCodeAlfa2($value)
 * @method static Builder|Nationality whereCodeAlfa3($value)
 * @method static Builder|Nationality whereFullTitle($value)
 * @mixin \Eloquent
 */
class Nationality extends Country
{
    protected $table = 'countries';
}
