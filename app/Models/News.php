<?php

namespace App\Models;

use Database\Factories\NewsFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;
use Voyager;

/**
 * App\Models\News
 *
 * @property int                             $id
 * @property string                          $title
 * @property string                          $annotated
 * @property string                          $description
 * @property string                          $image_uri
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property string                          $slug
 * @property-read null                       $translated
 * @property-read Collection|Translation[]   $translations
 * @property-read int|null                   $translations_count
 * @method static Builder|News newModelQuery()
 * @method static Builder|News newQuery()
 * @method static \Illuminate\Database\Query\Builder|News onlyTrashed()
 * @method static Builder|News query()
 * @method static Builder|News whereAnnotated($value)
 * @method static Builder|News whereCreatedAt($value)
 * @method static Builder|News whereDeletedAt($value)
 * @method static Builder|News whereDescription($value)
 * @method static Builder|News whereId($value)
 * @method static Builder|News whereImageUri($value)
 * @method static Builder|News whereSlug($value)
 * @method static Builder|News whereTitle($value)
 * @method static Builder|News whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|News whereUpdatedAt($value)
 * @method static Builder|News withTranslation($locale = null, $fallback = true)
 * @method static Builder|News withTranslations($locales = null, $fallback = true)
 * @method static \Illuminate\Database\Query\Builder|News withTrashed()
 * @method static \Illuminate\Database\Query\Builder|News withoutTrashed()
 * @mixin Eloquent
 * @property-read mixed             $image
 * @property int|null               $news_category_id
 * @property-read NewsCategory|null $newsCategory
 * @method static NewsFactory factory(...$parameters)
 * @method static Builder|News whereNewsCategoryId($value)
 * @property string                 $published_at
 * @property-read Collection|News[] $recommendNews
 * @property-read int|null          $recommend_news_count
 * @method static Builder|News wherePublishedAt($value)
 * @method static Builder|News exceptCurrentNews($slug)
 */
class News extends Model
{
    use HasFactory, Translatable;
    protected $translatable = ['title', 'annotated', 'description', 'slug'];

    protected string $defaultImagePath = "assets/images/news.png";

    public function newsCategory(): BelongsTo
    {
        return $this->belongsTo(NewsCategory::class);
    }

    public function getImageAttribute(): string
    {
        return $this->image_uri !== null ? Voyager::image($this->image_uri) : asset($this->defaultImagePath);
    }

    public function recommendNews(): HasMany
    {
        return $this->hasMany(self::class, 'news_category_id', 'news_category_id');
    }

    public function scopeExceptCurrentNews($query, $slug) {
        $query->where('slug', '!=', $slug);
    }
}
