<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\Footer
 *
 * @property int                                                         $id
 * @property string                                                      $title Название
 * @property string                                                      $link_external_internal Ссылка (внешняя или внутреняя)
 * @property int                                                         $position
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static Builder|Footer newModelQuery()
 * @method static Builder|Footer newQuery()
 * @method static \Illuminate\Database\Query\Builder|Footer onlyTrashed()
 * @method static Builder|Footer query()
 * @method static Builder|Footer sorted($way = 'ASC')
 * @method static Builder|Footer sortedByDESC()
 * @method static Builder|Footer whereCreatedAt($value)
 * @method static Builder|Footer whereDeletedAt($value)
 * @method static Builder|Footer whereId($value)
 * @method static Builder|Footer whereLinkExternalInternal($value)
 * @method static Builder|Footer wherePosition($value)
 * @method static Builder|Footer whereTitle($value)
 * @method static Builder|Footer whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|Footer whereUpdatedAt($value)
 * @method static Builder|Footer withTranslation($locale = null, $fallback = true)
 * @method static Builder|Footer withTranslations($locales = null, $fallback = true)
 * @method static \Illuminate\Database\Query\Builder|Footer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Footer withoutTrashed()
 * @mixin Eloquent
 * @property string $href Ссылка (внешняя или внутреняя)
 * @method static Builder|Footer whereHref($value)
 */
class Footer extends Model
{
    use HasFactory, Translatable, PositionTrait, SoftDeletes;

    protected array $translatable = [
        'title'
    ];
}
