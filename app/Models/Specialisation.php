<?php

namespace App\Models;

use Database\Factories\SpecialisationFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\Specialisation
 *
 * @property int                                         $id
 * @property string                                      $title
 * @property int                                         $position
 * @property Carbon|null             $created_at
 * @property Carbon|null             $updated_at
 * @property Carbon|null             $deleted_at
 * @property-read null                                   $translated
 * @property-read ServiceLanding|null                    $serviceLanding
 * @property-read \Kalnoy\Nestedset\Collection|Service[] $services
 * @property-read int|null                               $services_count
 * @property-write mixed                                 $disable_position_update
 * @property-write mixed                                 $position_column
 * @property-read Collection|Translation[]               $translations
 * @property-read int|null                               $translations_count
 * @method static SpecialisationFactory factory(...$parameters)
 * @method static Builder|Specialisation hasLanding()
 * @method static Builder|Specialisation newModelQuery()
 * @method static Builder|Specialisation newQuery()
 * @method static \Illuminate\Database\Query\Builder|Specialisation onlyTrashed()
 * @method static Builder|Specialisation query()
 * @method static Builder|Specialisation sorted($way = 'ASC')
 * @method static Builder|Specialisation sortedByDESC()
 * @method static Builder|Specialisation whereCreatedAt($value)
 * @method static Builder|Specialisation whereDeletedAt($value)
 * @method static Builder|Specialisation whereId($value)
 * @method static Builder|Specialisation wherePosition($value)
 * @method static Builder|Specialisation whereTitle($value)
 * @method static Builder|Specialisation whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|Specialisation whereUpdatedAt($value)
 * @method static Builder|Specialisation withTranslation($locale = null, $fallback = true)
 * @method static Builder|Specialisation withTranslations($locales = null, $fallback = true)
 * @method static \Illuminate\Database\Query\Builder|Specialisation withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Specialisation withoutTrashed()
 * @mixin Eloquent
 * @property string                                      $slug
 * @method static Builder|Specialisation whereSlug($value)
 * @property string|null                                 $uid
 * @property-read Collection|Doctor[]                    $doctors
 * @property-read int|null                               $doctors_count
 * @property-read \Kalnoy\Nestedset\Collection|Service[] $exclusiveServices
 * @property-read int|null                               $exclusive_services_count
 * @method static Builder|Specialisation searchByDoctorNameWithWhereHas($search)
 * @method static Builder|Specialisation searchByServiceTitle($search)
 * @method static Builder|Specialisation whereUid($value)
 * @property-read Collection|User[]                      $doctorUsers
 * @property-read int|null                               $doctor_users_count
 * @property-read Collection|\App\Models\Symptoms[] $symptoms
 * @property-read int|null $symptoms_count
 */
class Specialisation extends Model
{
    use HasFactory, Translatable, PositionTrait;
    protected $translatable = ['title', 'slug'];
    protected $fillable = ['title', 'slug', 'uid'];

    public function services(): HasMany
    {
        return $this->hasMany(Service::class);
    }

    public function exclusiveServices()
    {
        return $this->hasMany(Service::class)->exclusive();
    }

    public function doctors(): BelongsToMany
    {
        return $this->belongsToMany(Doctor::class);
    }

    public function doctorUsers(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'doctor_specialisation', 'doctor_user_id', 'specialisation_id');
    }

    public function serviceLanding(): HasOne
    {
        return $this->hasOne(ServiceLanding::class);
    }

    public function symptoms(): HasMany
    {
        return $this->hasMany(Symptoms::class);
    }

    /**
     * Scope a query to only include has landing.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeHasLanding(Builder $query): Builder
    {
        return $query->has('serviceLanding');
    }


    public function scopeSearchByServiceTitle(Builder $query, $search): Builder
    {
        return $query->with(['services' => function ($serviceQuery) use ($search) {
            $serviceQuery->where(DB::raw('lower(title)'), 'like', "%{$search}%")
                ->orderBy('title');
        }])->whereHas('services', function ($serviceQuery) use ($search) {
            $serviceQuery->where(DB::raw('lower(title)'), 'like', "%{$search}%");
        });
    }

}
