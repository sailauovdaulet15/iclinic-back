<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\BurgerMenu
 *
 * @property int                                                         $id
 * @property string                                                      $title Название
 * @property string                                                      $link_external_internal Ссылка (внешняя или внутреняя)
 * @property int                                                         $position
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static Builder|BurgerMenu newModelQuery()
 * @method static Builder|BurgerMenu newQuery()
 * @method static \Illuminate\Database\Query\Builder|BurgerMenu onlyTrashed()
 * @method static Builder|BurgerMenu query()
 * @method static Builder|BurgerMenu sorted($way = 'ASC')
 * @method static Builder|BurgerMenu sortedByDESC()
 * @method static Builder|BurgerMenu whereCreatedAt($value)
 * @method static Builder|BurgerMenu whereDeletedAt($value)
 * @method static Builder|BurgerMenu whereId($value)
 * @method static Builder|BurgerMenu whereLinkExternalInternal($value)
 * @method static Builder|BurgerMenu wherePosition($value)
 * @method static Builder|BurgerMenu whereTitle($value)
 * @method static Builder|BurgerMenu whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|BurgerMenu whereUpdatedAt($value)
 * @method static Builder|BurgerMenu withTranslation($locale = null, $fallback = true)
 * @method static Builder|BurgerMenu withTranslations($locales = null, $fallback = true)
 * @method static \Illuminate\Database\Query\Builder|BurgerMenu withTrashed()
 * @method static \Illuminate\Database\Query\Builder|BurgerMenu withoutTrashed()
 * @mixin Eloquent
 * @property string $href Ссылка (внешняя или внутреняя)
 * @method static Builder|BurgerMenu whereHref($value)
 */
class BurgerMenu extends Model
{
    use HasFactory, SoftDeletes, PositionTrait, Translatable;

    protected array $translatable = ['title'];


}
