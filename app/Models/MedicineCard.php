<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class MedicineCard extends Model
{
    use HasFactory;

    protected $fillable = [
        'history_visit_id',
        'uid',
        'appointment_id'
    ];


    /*********RELATIONSHIPS*********/

    public function files(): HasMany
    {
        return $this->hasMany(MedicineCardFile::class);
    }
}
