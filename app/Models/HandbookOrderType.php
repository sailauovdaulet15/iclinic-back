<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\HandbookOrderType
 *
 * @property int                                                         $id
 * @property string                                                      $name
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property string                                                      $type
 * @property-read null                                                   $translated
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static Builder|HandbookOrderType newModelQuery()
 * @method static Builder|HandbookOrderType newQuery()
 * @method static Builder|HandbookOrderType query()
 * @method static Builder|HandbookOrderType whereCreatedAt($value)
 * @method static Builder|HandbookOrderType whereId($value)
 * @method static Builder|HandbookOrderType whereName($value)
 * @method static Builder|HandbookOrderType whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|HandbookOrderType whereType($value)
 * @method static Builder|HandbookOrderType whereUpdatedAt($value)
 * @method static Builder|HandbookOrderType withTranslation($locale = null, $fallback = true)
 * @method static Builder|HandbookOrderType withTranslations($locales = null, $fallback = true)
 * @mixin Eloquent
 */
class HandbookOrderType extends Model
{
    use HasFactory, Translatable;

    protected $fillable = [
        'name'
    ];

    protected $translatable = [
        'name'
    ];
}
