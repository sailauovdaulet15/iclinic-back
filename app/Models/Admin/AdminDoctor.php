<?php

namespace App\Models\Admin;

use App\Traits\HasParentModel;
use App\Models\Doctor as BaseDoctor;


class AdminDoctor extends BaseDoctor
{
    use HasParentModel;

    protected static function booted()
    {
        parent::boot();
    }

}
