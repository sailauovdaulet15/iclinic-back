<?php

namespace App\Models\Admin;

use App\Traits\HasParentModel;
use App\Models\Service as BaseService;


class AdminService extends BaseService
{
    use HasParentModel;

    protected static function booted()
    {
        parent::boot();
    }

}
