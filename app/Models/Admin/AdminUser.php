<?php

namespace App\Models\Admin;

use App\Enums\RoleEnum;
use App\Traits\HasParentModel;
use App\Models\User as BaseUser;
use Illuminate\Support\Str;
use TCG\Voyager\Models\Role;


class AdminUser extends BaseUser
{
    use HasParentModel;

    protected static function booted()
    {
        parent::boot();
    }

    public static function firstOrCreateNotActive($name, $role = RoleEnum::USER)
    {
        return AdminUser::query()->firstOrCreate(['name' => $name], [
            'password' => bcrypt(Str::random()),
            'is_active' => false,
            'role_id' => Role::query()->where('name', $role)->first()->id,
        ]);
    }

}
