<?php

namespace App\Models;

use App\Enums\IconEnum;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\Stock
 *
 * @property int                                                         $id
 * @property string                                                      $title
 * @property string                                                      $description
 * @property string|null                     $image_uri
 * @property string|null                     $card_color
 * @property int                             $first_icon_id
 * @property int                             $second_icon_id
 * @property string                          $start_time
 * @property string                          $end_time
 * @property int                             $position
 * @property Carbon|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Icon                       $firstIcon
 * @property-read string                     $card_rgb_color
 * @property-read null                       $translated
 * @property-read Icon                       $secondIcon
 * @property-write mixed                     $disable_position_update
 * @property-write mixed                     $position_column
 * @property-read Collection|Translation[]   $translations
 * @property-read int|null                   $translations_count
 * @method static Builder|Stock count($count)
 * @method static Builder|Stock newModelQuery()
 * @method static Builder|Stock newQuery()
 * @method static \Illuminate\Database\Query\Builder|Stock onlyTrashed()
 * @method static Builder|Stock query()
 * @method static Builder|Stock sorted($way = 'ASC')
 * @method static Builder|Stock sortedByDESC()
 * @method static Builder|Stock whereCardColor($value)
 * @method static Builder|Stock whereCreatedAt($value)
 * @method static Builder|Stock whereDeletedAt($value)
 * @method static Builder|Stock whereDescription($value)
 * @method static Builder|Stock whereEndTime($value)
 * @method static Builder|Stock whereFirstIconId($value)
 * @method static Builder|Stock whereId($value)
 * @method static Builder|Stock whereImageUri($value)
 * @method static Builder|Stock wherePosition($value)
 * @method static Builder|Stock whereSecondIconId($value)
 * @method static Builder|Stock whereStartTime($value)
 * @method static Builder|Stock whereTitle($value)
 * @method static Builder|Stock whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|Stock whereUpdatedAt($value)
 * @method static Builder|Stock withTranslation($locale = null, $fallback = true)
 * @method static Builder|Stock withTranslations($locales = null, $fallback = true)
 * @method static \Illuminate\Database\Query\Builder|Stock withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Stock withoutTrashed()
 * @mixin Eloquent
 * @property int|null $service_id
 * @method static Builder|Stock whereServiceId($value)
 */
class Stock extends Model
{
    use HasFactory, PositionTrait, SoftDeletes, Translatable;

    protected array $translatable = [
        'title',
        'description'
    ];

    public function firstIcon(): BelongsTo
    {
        return $this->belongsTo(Icon::class, 'first_icon_id', 'id')->where('key', IconEnum::FIRST_ICON);
    }

    public function secondIcon(): BelongsTo
    {
        return $this->belongsTo(Icon::class, 'second_icon_id', 'id')->where('key', IconEnum::SECOND_ICON);
    }

    public function scopeCount($query, $count)
    {
        return $query->take($count);
    }

    /**
     * Convert hex color to RGB
     * @return string
     * @example "255, 255, 255"
     */
    public function getCardRgbColorAttribute(): string
    {
        return implode(', ', sscanf($this->card_color, "#%02x%02x%02x"));
    }
}
