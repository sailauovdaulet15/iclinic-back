<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\HandbookSurveyQuestion
 *
 * @property int                                                         $id
 * @property string                                                      $title
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @mixin Eloquent
 * @property int                                                         $position Позиция
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion newQuery()
 * @method static Builder|HandbookSurveyQuestion onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion query()
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|HandbookSurveyQuestion withTranslations($locales = null, $fallback = true)
 * @method static Builder|HandbookSurveyQuestion withTrashed()
 * @method static Builder|HandbookSurveyQuestion withoutTrashed()
 */
class HandbookSurveyQuestion extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;

    protected $fillable = ['title', 'position'];

    protected $translatable = ['title'];

    public static function boot(): void
    {
        parent::boot();

        self::creating(function(self $model): void {
            $model->position = self::max('position') + 1;
        });

        self::deleting(function(self $model): void {
            $items = self::where('position', '>', $model->position)->get();

            foreach ($items as $item) {
                $item->position--;
                $item->save();
            }
        });
    }
}
