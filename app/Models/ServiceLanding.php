<?php

namespace App\Models;

use Database\Factories\ServiceLandingFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Kalnoy\Nestedset\Collection;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\ServiceLanding
 *
 * @property int                                                         $id
 * @property string                                                      $title
 * @property string                                                      $description
 * @property array                                                       $specialisation_count_details
 * @property string                                                      $image_uri
 * @property string                                                      $slug
 * @property int                                                         $service_id
 * @property Carbon|null                             $deleted_at
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property-read null                                                   $translated
 * @property-read Service                                                $service
 * @property-read \Illuminate\Database\Eloquent\Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static ServiceLandingFactory factory(...$parameters)
 * @method static Builder|ServiceLanding newModelQuery()
 * @method static Builder|ServiceLanding newQuery()
 * @method static \Illuminate\Database\Query\Builder|ServiceLanding onlyTrashed()
 * @method static Builder|ServiceLanding query()
 * @method static Builder|ServiceLanding whereCreatedAt($value)
 * @method static Builder|ServiceLanding whereDeletedAt($value)
 * @method static Builder|ServiceLanding whereDescription($value)
 * @method static Builder|ServiceLanding whereDetails($value)
 * @method static Builder|ServiceLanding whereId($value)
 * @method static Builder|ServiceLanding whereImageUri($value)
 * @method static Builder|ServiceLanding whereServiceId($value)
 * @method static Builder|ServiceLanding whereSlug($value)
 * @method static Builder|ServiceLanding whereTitle($value)
 * @method static Builder|ServiceLanding whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|ServiceLanding whereUpdatedAt($value)
 * @method static Builder|ServiceLanding withTranslation($locale = null, $fallback = true)
 * @method static Builder|ServiceLanding withTranslations($locales = null, $fallback = true)
 * @method static \Illuminate\Database\Query\Builder|ServiceLanding withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ServiceLanding withoutTrashed()
 * @mixin Eloquent
 * @property int                                                      $specialisation_id
 * @property-read Specialisation                                      $specialisation
 * @method static Builder|ServiceLanding whereSpecialisationId($value)
 * @property string                                                   $annotation
 * @property-read \Illuminate\Database\Eloquent\Collection|User[]     $doctors
 * @property-read int|null                                            $doctors_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Review[]   $reviews
 * @property-read int|null                                            $reviews_count
 * @property-read Collection|Service[]                                $services
 * @property-read int|null                                            $services_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Symptoms[] $symptoms
 * @property-read int|null                                            $symptoms_count
 * @method static Builder|ServiceLanding whereAnnotation($value)
 * @property-read Collection|Service[]                                $recommendServices
 * @property-read int|null                                            $recommend_services_count
 * @method static Builder|ServiceLanding whereSpecialisationCountDetails($value)
 */
class ServiceLanding extends Model
{
    use HasFactory, SoftDeletes, Translatable;
    public $translatable = ['title', 'annotation', 'slug'];

    protected $fillable = ['specialisation_count_details'];


    public function specialisation()
    {
        return $this->belongsTo(Specialisation::class);
    }

    public function reviews()
    {
        return $this->morphedByMany(Review::class, 'service_landingable');
    }

    public function recommendServices()
    {
        return $this->morphedByMany(Service::class, 'service_landingable');
    }

    public function services()
    {
        return $this->specialisation->services();
    }


}
