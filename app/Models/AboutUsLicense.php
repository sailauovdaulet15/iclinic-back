<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\AboutUsLicense
 *
 * @property int $id
 * @property string $image_uri
 * @property string $description
 * @property int $about_us_id
 * @property int $position Позиция
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read null $translated
 * @property-write mixed $disable_position_update
 * @property-write mixed $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense newQuery()
 * @method static Builder|AboutUsLicense onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense query()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense whereAboutUsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense whereImageUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsLicense withTranslations($locales = null, $fallback = true)
 * @method static Builder|AboutUsLicense withTrashed()
 * @method static Builder|AboutUsLicense withoutTrashed()
 * @mixin Eloquent
 * @property-read mixed $image
 */
class AboutUsLicense extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;

    protected $translatable = ['description'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'id',
        'image_uri',
        'file'
    ];



    public function getFile()
    {
        $image = json_decode($this->file);

        return isset($image)  ? url('/') . '/storage/' . $image[0]->download_link : null;
    }


}
