<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\AboutUsSlider
 *
 * @property int                                                         $id
 * @property string                                                      $image_uri
 * @property string                                                      $description
 * @property int                                                         $about_us_id
 * @property int                                                         $position Позиция
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider newQuery()
 * @method static Builder|AboutUsSlider onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider query()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider whereAboutUsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider whereImageUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsSlider withTranslations($locales = null, $fallback = true)
 * @method static Builder|AboutUsSlider withTrashed()
 * @method static Builder|AboutUsSlider withoutTrashed()
 * @mixin Eloquent
 * @property-read mixed $image
 */
class AboutUsSlider extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;
    protected $translatable = ['description'];

    protected $appends = ['image'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'id',
        'image_uri'
    ];


    public function getImageAttribute(){
        return Storage::disk(config('voyager.storage.disk'))->url($this->attributes['image_uri']);
    }
}
