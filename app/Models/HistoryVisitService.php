<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * App\Models\HistoryVisitService
 *
 * @property int $id
 * @property int $history_visit_id
 * @property int $service_id
 * @property int $amount
 * @property int $sum
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitService newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitService newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitService query()
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitService whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitService whereHistoryVisitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitService whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitService whereSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitService whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class HistoryVisitService extends Model
{
    use HasFactory;

}
