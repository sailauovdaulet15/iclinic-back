<?php

namespace App\Models;

use App\Enums\IconEnum;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;

/**
 * App\Models\Icon
 *
 * @property int                                                   $id
 * @property string                                                $title
 * @property string                                                $image_uri
 * @property string                                                $key Ключ иконки
 * @property Carbon|null                       $deleted_at
 * @property Carbon|null                       $created_at
 * @property Carbon|null                       $updated_at
 * @property-read Collection|Stock[] $stocks
 * @property-read int|null                                         $stocks_count
 * @method static \Illuminate\Database\Eloquent\Builder|Icon firstIcon()
 * @method static \Illuminate\Database\Eloquent\Builder|Icon newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Icon newQuery()
 * @method static Builder|Icon onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Icon query()
 * @method static \Illuminate\Database\Eloquent\Builder|Icon secondIcon()
 * @method static \Illuminate\Database\Eloquent\Builder|Icon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Icon whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Icon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Icon whereImageUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Icon whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Icon whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Icon whereUpdatedAt($value)
 * @method static Builder|Icon withTrashed()
 * @method static Builder|Icon withoutTrashed()
 * @mixin Eloquent
 */
class Icon extends Model
{
    use HasFactory, SoftDeletes;

    public function stocks()
    {
        return $this->hasMany(Stock::class, 'first_icon_id', 'id');
    }

    public function scopeFirstIcon($query)
    {
        return $query->where('key', IconEnum::FIRST_ICON);
    }

    public function scopeSecondIcon($query)
    {
        return $query->where('key', IconEnum::SECOND_ICON);
    }
}
