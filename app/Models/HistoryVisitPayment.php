<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * App\Models\HistoryVisitPayment
 *
 * @property int $id
 * @property int $history_visit_id
 * @property int|null $handbook_order_type_id
 * @property int $sum
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitPayment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitPayment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitPayment query()
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitPayment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitPayment whereHandbookOrderTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitPayment whereHistoryVisitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitPayment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitPayment whereSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|HistoryVisitPayment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class HistoryVisitPayment extends Model
{
    use HasFactory;

}
