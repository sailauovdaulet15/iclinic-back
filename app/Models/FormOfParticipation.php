<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\FormOfParticipation
 *
 * @property int $id
 * @property string $title
 * @property string $type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|FormOfParticipation newModelQuery()
 * @method static Builder|FormOfParticipation newQuery()
 * @method static Builder|FormOfParticipation query()
 * @method static Builder|FormOfParticipation whereCreatedAt($value)
 * @method static Builder|FormOfParticipation whereId($value)
 * @method static Builder|FormOfParticipation whereTitle($value)
 * @method static Builder|FormOfParticipation whereType($value)
 * @method static Builder|FormOfParticipation whereUpdatedAt($value)
 * @mixin Eloquent
 */
class FormOfParticipation extends Model
{
    use HasFactory;

    protected $fillable = [
        'type', 'title'
    ];
}
