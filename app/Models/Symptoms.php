<?php

namespace App\Models;

use Database\Factories\SymptomsFactory;
use DB;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\Symptoms
 *
 * @property int                             $id
 * @property string                          $title Заголовок
 * @property string                          $description Описание
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read null                       $translated
 * @property-read Collection|Translation[]   $translations
 * @property-read int|null                   $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms newQuery()
 * @method static Builder|Symptoms onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms query()
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms withTranslations($locales = null, $fallback = true)
 * @method static Builder|Symptoms withTrashed()
 * @method static Builder|Symptoms withoutTrashed()
 * @mixin Eloquent
 * @property-read Collection|ServiceLanding[] $serviceLandings
 * @property-read int|null                                                  $service_landings_count
 * @method static SymptomsFactory factory(...$parameters)
 * @property int $specialisation_id
 * @property int $position
 * @property-write mixed $disable_position_update
 * @property-write mixed $position_column
 * @property-read \App\Models\Specialisation $specialisation
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Symptoms whereSpecialisationId($value)
 */
class Symptoms extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;

    protected array $translatable = ['title', 'description'];


    public function serviceLandings()
    {
        return $this->morphToMany(ServiceLanding::class, 'service_landingable');
    }

    public function specialisation(): BelongsTo
    {
        return $this->belongsTo(Specialisation::class);
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function(self $symptom): void {
            $symptom->position = self::where('specialisation_id', $symptom->specialisation_id)->count() + 1;
        });


        self::updating(function(self $symptom):void {
            if(intval($symptom->getOriginal('specialisation_id'))!==intval($symptom->specialisation_id)){

                self::where('position', '>', intval($symptom->getOriginal('position')))
                    ->where(['specialisation_id' => intval($symptom->getOriginal('specialisation_id'))])
                    ->update(['position' => DB::raw('position-1')]);

                $symptom->position = self::where(['specialisation_id' => intval($symptom->specialisation_id)])->count()+1;
            }
        });
    }
}
