<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\AboutUsWhyUs
 *
 * @property int                                                         $id
 * @property string                                                      $image_uri
 * @property string                                                      $title
 * @property string                                                      $description
 * @property int                                                         $about_us_id
 * @property int                                                         $position Позиция
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs newQuery()
 * @method static Builder|AboutUsWhyUs onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs query()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs whereAboutUsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs whereImageUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|AboutUsWhyUs withTranslations($locales = null, $fallback = true)
 * @method static Builder|AboutUsWhyUs withTrashed()
 * @method static Builder|AboutUsWhyUs withoutTrashed()
 * @mixin Eloquent
 * @property-read mixed $image
 */
class AboutUsWhyUs extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;
    protected $translatable = ['title', 'description'];
    protected $appends = ['image'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'id',
        'image_uri'
    ];


    public function getImageAttribute(){
        return Storage::disk(config('voyager.storage.disk'))->url($this->attributes['image_uri']);
    }
}
