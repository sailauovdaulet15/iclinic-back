<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;


/**
 * App\Models\OrderingCall
 *
 * @property int                                                         $id
 * @property string                          $user_name Имя
 * @property string                          $user_phone Номер телефона
 * @property int|null                        $user_id
 * @property int|null                        $specialisation_id
 * @property string|null                     $date Удобная дата приема
 * @property string|null                     $time Удобное время приема
 * @property string|null                     $comment Комментарий
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read Doctor|null                $doctors
 * @property-read null                       $translated
 * @property-read Specialisation|null        $specialisation
 * @property-read Collection|Translation[]   $translations
 * @property-read int|null                   $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall newQuery()
 * @method static Builder|OrderingCall onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall whereSpecialisationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall whereUserName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall whereUserPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderingCall withTranslations($locales = null, $fallback = true)
 * @method static Builder|OrderingCall withTrashed()
 * @method static Builder|OrderingCall withoutTrashed()
 * @mixin Eloquent
 */
class OrderingCall extends Model
{
    use HasFactory, Translatable, SoftDeletes;

    protected array $translatable = [
        'user_name',
        'comment',
    ];

    protected $fillable = [
        'user_name', 'comment', 'user_phone',
        'date', 'time', 'user_id',
        'specialisation_id'
    ];

    public function doctors(): BelongsTo
    {
        return $this->belongsTo(Doctor::class, 'user_id');
    }

    public function specialisation(): BelongsTo
    {
        return $this->belongsTo(Specialisation::class);
    }
}
