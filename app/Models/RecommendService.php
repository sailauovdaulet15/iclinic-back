<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;
use Kalnoy\Nestedset\QueryBuilder;
use TCG\Voyager\Models\Translation;

/**
 * App\Models\RecommendService
 *
 * @property int                                                  $id
 * @property string                                               $title
 * @property string|null                                          $description
 * @property string|null                                          $link
 * @property int|null                                             $price_original
 * @property int|null                                             $price_percent
 * @property int|null                                             $price_bonus
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static Builder|RecommendService newModelQuery()
 * @method static Builder|RecommendService newQuery()
 * @method static Builder|RecommendService query()
 * @method static Builder|RecommendService whereCreatedAt($value)
 * @method static Builder|RecommendService whereDeletedAt($value)
 * @method static Builder|RecommendService whereDescription($value)
 * @method static Builder|RecommendService whereId($value)
 * @method static Builder|RecommendService whereLft($value)
 * @method static Builder|RecommendService whereLink($value)
 * @method static Builder|RecommendService whereParentId($value)
 * @method static Builder|RecommendService wherePriceBonus($value)
 * @method static Builder|RecommendService wherePriceOriginal($value)
 * @method static Builder|RecommendService wherePricePercent($value)
 * @method static Builder|RecommendService whereRgt($value)
 * @method static Builder|RecommendService whereTitle($value)
 * @method static Builder|RecommendService whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read \Kalnoy\Nestedset\Collection|Service[]          $children
 * @property-read int|null                                        $children_count
 * @property-read mixed                                           $price_with_discount
 * @property-read null                                            $translated
 * @property-read Service|null                                    $parent
 * @property-read \Kalnoy\Nestedset\Collection|RecommendService[] $recommendServices
 * @property-read int|null                                        $recommend_services_count
 * @property-read ServiceLanding|null                             $serviceLanding
 * @property-read Collection|Translation[]                        $translations
 * @property-read int|null                                        $translations_count
 * @method static \Kalnoy\Nestedset\Collection|static[] all($columns = ['*'])
 * @method static QueryBuilder|RecommendService ancestorsAndSelf($id, array $columns = [])
 * @method static QueryBuilder|RecommendService ancestorsOf($id, array $columns = [])
 * @method static QueryBuilder|RecommendService applyNestedSetScope(?string $table = null)
 * @method static QueryBuilder|RecommendService countErrors()
 * @method static QueryBuilder|Service d()
 * @method static QueryBuilder|RecommendService defaultOrder(string $dir = 'asc')
 * @method static QueryBuilder|RecommendService descendantsAndSelf($id, array $columns = [])
 * @method static QueryBuilder|RecommendService descendantsOf($id, array $columns = [], $andSelf = false)
 * @method static QueryBuilder|RecommendService fixSubtree($root)
 * @method static QueryBuilder|RecommendService fixTree($root = null)
 * @method static \Kalnoy\Nestedset\Collection|static[] get($columns = ['*'])
 * @method static QueryBuilder|RecommendService getNodeData($id, $required = false)
 * @method static QueryBuilder|RecommendService getPlainNodeData($id, $required = false)
 * @method static QueryBuilder|RecommendService getTotalErrors()
 * @method static QueryBuilder|RecommendService hasChildren()
 * @method static QueryBuilder|Service hasLanding()
 * @method static QueryBuilder|RecommendService hasParent()
 * @method static QueryBuilder|RecommendService isBroken()
 * @method static QueryBuilder|RecommendService leaves(array $columns = [])
 * @method static QueryBuilder|RecommendService makeGap(int $cut, int $height)
 * @method static QueryBuilder|RecommendService moveNode($key, $position)
 * @method static QueryBuilder|RecommendService orWhereAncestorOf(bool $id, bool $andSelf = false)
 * @method static QueryBuilder|RecommendService orWhereDescendantOf($id)
 * @method static QueryBuilder|RecommendService orWhereNodeBetween($values)
 * @method static QueryBuilder|RecommendService orWhereNotDescendantOf($id)
 * @method static QueryBuilder|RecommendService rebuildSubtree($root, array $data, $delete = false)
 * @method static QueryBuilder|RecommendService rebuildTree(array $data, $delete = false, $root = null)
 * @method static QueryBuilder|RecommendService reversed()
 * @method static QueryBuilder|RecommendService root(array $columns = [])
 * @method static QueryBuilder|Service roots()
 * @method static QueryBuilder|RecommendService whereAncestorOf($id, $andSelf = false, $boolean = 'and')
 * @method static QueryBuilder|RecommendService whereAncestorOrSelf($id)
 * @method static QueryBuilder|RecommendService whereDescendantOf($id, $boolean = 'and', $not = false, $andSelf = false)
 * @method static QueryBuilder|RecommendService whereDescendantOrSelf(string $id, string $boolean = 'and', string $not = false)
 * @method static QueryBuilder|RecommendService whereIsAfter($id, $boolean = 'and')
 * @method static QueryBuilder|RecommendService whereIsBefore($id, $boolean = 'and')
 * @method static QueryBuilder|RecommendService whereIsLeaf()
 * @method static QueryBuilder|RecommendService whereIsRoot()
 * @method static QueryBuilder|RecommendService whereNodeBetween($values, $boolean = 'and', $not = false)
 * @method static QueryBuilder|RecommendService whereNotDescendantOf($id)
 * @method static QueryBuilder|Service whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static QueryBuilder|RecommendService withDepth(string $as = 'depth')
 * @method static QueryBuilder|Service withTranslation($locale = null, $fallback = true)
 * @method static QueryBuilder|Service withTranslations($locales = null, $fallback = true)
 * @method static QueryBuilder|RecommendService withoutRoot()
 * @property-read Collection|Doctor[]                             $doctors
 * @property-read int|null                                        $doctors_count
 * @property string|null                                          $annotation
 * @property int                                                  $position Позиция
 * @property int                                                  $order Сортировка
 * @property bool                                                 $is_visible
 * @property int                                                  $specialisation_id
 * @property-read Service|null                                    $parentId
 * @method static QueryBuilder|RecommendService whereAnnotation($value)
 * @method static QueryBuilder|RecommendService whereIsVisible($value)
 * @method static QueryBuilder|RecommendService whereOrder($value)
 * @method static QueryBuilder|RecommendService wherePosition($value)
 * @method static QueryBuilder|RecommendService whereSpecialisationId($value)
 * @property-read Collection|ServiceLanding[]                     $serviceLandings
 * @property-read int|null                                        $service_landings_count
 * @property string|null                                          $uid
 * @property int|null                                             $reception_type_id
 * @property string                                               $slug
 * @property-read Collection|Doctor[]                                $doctorRecommends
 * @property-read int|null                                           $doctor_recommends_count
 * @property-read Collection|DoctorService[]                         $doctorServices
 * @property-read int|null                                           $doctor_services_count
 * @property-read Specialisation|null                                $specialisation
 * @property-read Collection|Stock[]                                 $stocks
 * @property-read int|null                                $stocks_count
 * @method static QueryBuilder|Service exclusive(bool $is_exclusive = true)
 * @method static QueryBuilder|RecommendService whereReceptionTypeId($value)
 * @method static QueryBuilder|RecommendService whereSlug($value)
 * @method static QueryBuilder|RecommendService whereUid($value)
 * @property-read Collection|Appointment[]                $appointments
 * @property-read int|null                                $appointments_count
 * @property-read Collection|HistoryVisit[]               $historyVisits
 * @property-read int|null                                $history_visits_count
 */
class RecommendService extends Service
{
    use HasFactory;

    protected $table = 'services';

    public function doctorRecommends(): BelongsToMany
    {
        return $this->belongsToMany(Doctor::class,
            'doctor_recommend_service', 'recommend_service_id', 'doctor_user_id');
    }


}
