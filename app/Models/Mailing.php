<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\Mailing
 *
 * @property int                             $id
 * @property int                             $user_id
 * @property string                          $email
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User                       $user
 * @method static Builder|Mailing newModelQuery()
 * @method static Builder|Mailing newQuery()
 * @method static Builder|Mailing query()
 * @method static Builder|Mailing whereCreatedAt($value)
 * @method static Builder|Mailing whereEmail($value)
 * @method static Builder|Mailing whereId($value)
 * @method static Builder|Mailing whereUpdatedAt($value)
 * @method static Builder|Mailing whereUserId($value)
 * @mixin Eloquent
 */
class Mailing extends Model
{

    use HasFactory;

    protected $primaryKey = 'user_id';
    protected $fillable = [
        'user_id',
        'email',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }


}
