<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\Pharmacy
 *
 * @property int                                                         $id
 * @property string                                                      $image_uri
 * @property int                                                         $total_count
 * @property int                             $price
 * @property int                             $stock
 * @property string                          $title
 * @property string                          $description
 * @property int                             $pharmacy_type_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property int                             $pharmacy_tab_id
 * @property-read null                       $translated
 * @property-read PharmacyTab                $pharmacyTab
 * @property-read PharmacyType               $pharmacyType
 * @property-read Collection|Translation[]   $translations
 * @property-read int|null                   $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy newQuery()
 * @method static Builder|Pharmacy onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy query()
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy whereImageUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy wherePharmacyTabId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy wherePharmacyTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy whereStock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy whereTotalCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|Pharmacy withTranslations($locales = null, $fallback = true)
 * @method static Builder|Pharmacy withTrashed()
 * @method static Builder|Pharmacy withoutTrashed()
 * @mixin Eloquent
 * @property-read mixed $image
 */
class Pharmacy extends Model
{
    use HasFactory, Translatable, SoftDeletes;
    protected $translatable = ['title', 'description'];

    public function pharmacyType(): BelongsTo
    {
        return $this->belongsTo(PharmacyType::class);
    }

    public function pharmacyTab(): BelongsTo
    {
        return $this->belongsTo(PharmacyTab::class);
    }

    public function getImageAttribute(){
        return Storage::disk(config('voyager.storage.disk'))->url($this->attributes['image_uri']);
    }
}
