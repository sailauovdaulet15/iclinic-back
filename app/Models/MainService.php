<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;


/**
 * App\Models\MainService
 *
 * @property int                                                         $id
 * @property string                                                      $title
 * @property string                                                      $description
 * @property string                                                      $image_uri
 * @property string|null                                                 $link
 * @property int                                                         $position Позиция
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|MainService newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainService newQuery()
 * @method static Builder|MainService onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|MainService query()
 * @method static \Illuminate\Database\Eloquent\Builder|MainService sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|MainService sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|MainService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainService whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainService whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainService whereImageUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainService whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainService wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainService whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainService whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|MainService whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainService withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|MainService withTranslations($locales = null, $fallback = true)
 * @method static Builder|MainService withTrashed()
 * @method static Builder|MainService withoutTrashed()
 * @mixin Eloquent
 * @property-read mixed $image
 */
class MainService extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;
    protected array $translatable = ['title', 'description'];
    protected $appends = ['image'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'id',
        'image_uri'
    ];

    public function getImageAttribute(){
        return Storage::disk(config('voyager.storage.disk'))->url($this->image_uri);
    }
}
