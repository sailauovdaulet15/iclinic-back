<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\Review
 *
 * @property int                             $id
 * @property string                          $name Имя
 * @property string                          $feedback Отзыв
 * @property int                             $position
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read null                       $translated
 * @property-write mixed                     $disable_position_update
 * @property-write mixed                     $position_column
 * @property-read Collection|Translation[]   $translations
 * @property-read int|null                   $translations_count
 * @method static Builder|Review newModelQuery()
 * @method static Builder|Review newQuery()
 * @method static \Illuminate\Database\Query\Builder|Review onlyTrashed()
 * @method static Builder|Review query()
 * @method static Builder|Review sorted($way = 'ASC')
 * @method static Builder|Review sortedByDESC()
 * @method static Builder|Review whereCreatedAt($value)
 * @method static Builder|Review whereDeletedAt($value)
 * @method static Builder|Review whereFeedback($value)
 * @method static Builder|Review whereId($value)
 * @method static Builder|Review whereName($value)
 * @method static Builder|Review wherePosition($value)
 * @method static Builder|Review whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|Review whereUpdatedAt($value)
 * @method static Builder|Review withTranslation($locale = null, $fallback = true)
 * @method static Builder|Review withTranslations($locales = null, $fallback = true)
 * @method static \Illuminate\Database\Query\Builder|Review withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Review withoutTrashed()
 * @mixin Eloquent
 * @property-read Collection|ServiceLanding[] $serviceLandings
 * @property-read int|null                                                  $service_landings_count
 * @property int|null                                                       $doctor_user_id
 * @property string|null                                                    $source_link
 * @method static Builder|Review whereDoctorUserId($value)
 * @method static Builder|Review whereSourceLink($value)
 */
class Review extends Model
{
    use HasFactory, Translatable, PositionTrait, SoftDeletes;

    protected array $translatable = [
        'name',
        'feedback',
    ];

    public function serviceLandings()
    {
        return $this->morphToMany(ServiceLanding::class, 'service_landingable');
    }
}
