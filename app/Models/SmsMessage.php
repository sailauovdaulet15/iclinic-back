<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\SmsMessage
 *
 * @property int $id
 * @property string $phone
 * @property $sms_code
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|SmsMessage newModelQuery()
 * @method static Builder|SmsMessage newQuery()
 * @method static Builder|SmsMessage query()
 * @method static Builder|SmsMessage whereCreatedAt($value)
 * @method static Builder|SmsMessage whereId($value)
 * @method static Builder|SmsMessage whereSmsCode($value)
 * @method static Builder|SmsMessage whereUpdatedAt($value)
 * @method static Builder|SmsMessage whereUserId($value)
 * @mixin Eloquent
 * @method static Builder|SmsMessage wherePhone($value)
 */
class SmsMessage extends Model
{
    use HasFactory;

    protected $fillable = [
        'phone',
        'sms_code'
    ];
}
