<?php

namespace App\Models;

use App\Traits\GeometryCast;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Spatial;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\Contact
 *
 * @property int                             $id
 * @property string                          $address
 * @property array                           $phones Телефоны
 * @property array                           $schedule Расписание
 * @property mixed                           $map Карта
 * @property int                             $city_id
 * @property int                             $position Позиция
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read City                       $city
 * @property-read null                       $translated
 * @property-write mixed                     $disable_position_update
 * @property-write mixed                     $position_column
 * @property-read Collection|Translation[]   $translations
 * @property-read int|null                   $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|Contact newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Contact newQuery()
 * @method static Builder|Contact onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Contact query()
 * @method static \Illuminate\Database\Eloquent\Builder|Contact sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|Contact sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereMap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact wherePhones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereSchedule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|Contact withTranslations($locales = null, $fallback = true)
 * @method static Builder|Contact withTrashed()
 * @method static Builder|Contact withoutTrashed()
 * @mixin Eloquent
 * @property-read array $formatted_phones
 */
class Contact extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait, GeometryCast;

    protected $translatable = ['address'];

    protected $spatial = [
        'map',
    ];

    protected $casts = [
        'map' => 'array',
    ];


    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function getFormattedPhonesAttribute(): array
    {

        $phones = null;
        $fieldsGroup = optional(json_decode($this->phones))->fields;
        if (preg_match('/^\d(\d{3})(\d{3})(\d{2})(\d{2})$/', $fieldsGroup->mobile_phone->value, $matches)) {
            // +7 (777) 777 - 77 - 77
            $phones[] = ['phone' => "+7 ({$matches[1]}) {$matches[2]} - {$matches[3]} - {$matches[4]}"];
        }
        // +7 (7777) 777 - 777
        if (preg_match('/^\d(\d{4})(\d{3})(\d{3})$/', $fieldsGroup->phone->value, $matches)) {
            $phones[] = ['phone' => "+7 ({$matches[1]}) {$matches[2]} - {$matches[3]}"];
        }

        return $phones;
    }
}
