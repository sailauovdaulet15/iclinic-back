<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\ReceptionType
 *
 * @property int $id
 * @property string $title
 * @property string $type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|ReceptionType newModelQuery()
 * @method static Builder|ReceptionType newQuery()
 * @method static Builder|ReceptionType query()
 * @method static Builder|ReceptionType whereCreatedAt($value)
 * @method static Builder|ReceptionType whereId($value)
 * @method static Builder|ReceptionType whereTitle($value)
 * @method static Builder|ReceptionType whereType($value)
 * @method static Builder|ReceptionType whereUpdatedAt($value)
 * @mixin Eloquent
 */
class ReceptionType extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'type'
    ];
}
