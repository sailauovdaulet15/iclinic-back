<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\NewsSlider
 *
 * @property int $id
 * @property string $title
 * @property string $image_uri
 * @property int $news
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|NewsSlider newModelQuery()
 * @method static Builder|NewsSlider newQuery()
 * @method static Builder|NewsSlider query()
 * @method static Builder|NewsSlider whereCreatedAt($value)
 * @method static Builder|NewsSlider whereId($value)
 * @method static Builder|NewsSlider whereImageUri($value)
 * @method static Builder|NewsSlider whereNews($value)
 * @method static Builder|NewsSlider whereTitle($value)
 * @method static Builder|NewsSlider whereUpdatedAt($value)
 * @mixin Eloquent
 */
class NewsSlider extends Model
{
    use HasFactory;
}
