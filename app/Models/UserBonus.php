<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileUnacceptableForCollection;

/**
 * App\Models\UserBonus
 *
 * @property int                             $id
 * @property int                             $user_id
 * @property int                             $cancellation_amount
 * @property int                             $replenishment_amount
 * @property string                          $cancellation_at
 * @property string                          $replenishment_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property mixed $0
 * @property mixed $1
 * @property-read User                       $user
 * @method static Builder|UserBonus newModelQuery()
 * @method static Builder|UserBonus newQuery()
 * @method static Builder|UserBonus query()
 * @method static Builder|UserBonus whereCancellationAmount($value)
 * @method static Builder|UserBonus whereCancellationAt($value)
 * @method static Builder|UserBonus whereCreatedAt($value)
 * @method static Builder|UserBonus whereId($value)
 * @method static Builder|UserBonus whereReplenishmentAmount($value)
 * @method static Builder|UserBonus whereReplenishmentAt($value)
 * @method static Builder|UserBonus whereUpdatedAt($value)
 * @method static Builder|UserBonus whereUserId($value)
 * @mixin Eloquent
 */
class UserBonus extends Model
{
    use HasFactory;

    protected $fillable = [
      'user_id',
      'cancellation_amount',
      'replenishment_amount',
    ];

    protected $casts = [
        'cancellation_at',
        'replenishment_at',
    ];

    public function user():BelongsTo{
        return $this->belongsTo(User::class);
    }
}
