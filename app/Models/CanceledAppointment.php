<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\CanceledAppointment
 *
 * @property int         $id
 * @property string|null $message Причину отмены
 * @property int         $appointment_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|CanceledAppointment newModelQuery()
 * @method static Builder|CanceledAppointment newQuery()
 * @method static Builder|CanceledAppointment query()
 * @method static Builder|CanceledAppointment whereAppointmentId($value)
 * @method static Builder|CanceledAppointment whereCreatedAt($value)
 * @method static Builder|CanceledAppointment whereId($value)
 * @method static Builder|CanceledAppointment whereMessage($value)
 * @method static Builder|CanceledAppointment whereUpdatedAt($value)
 * @mixin Eloquent
 */
class CanceledAppointment extends Model
{
    use HasFactory;

    protected $fillable = [
        'message',
        'appointment_id',
    ];
}
