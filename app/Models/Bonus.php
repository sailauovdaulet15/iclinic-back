<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\Bonus
 *
 * @property int                                                         $id
 * @property string                                                      $description Описание
 * @property string                                                      $icon Иконка
 * @property string                                                      $numbers Цифры
 * @property string                                                      $unit_measurement Единица измерения
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static Builder|Bonus newModelQuery()
 * @method static Builder|Bonus newQuery()
 * @method static \Illuminate\Database\Query\Builder|Bonus onlyTrashed()
 * @method static Builder|Bonus query()
 * @method static Builder|Bonus whereCreatedAt($value)
 * @method static Builder|Bonus whereDeletedAt($value)
 * @method static Builder|Bonus whereDescription($value)
 * @method static Builder|Bonus whereIcon($value)
 * @method static Builder|Bonus whereId($value)
 * @method static Builder|Bonus whereNumbers($value)
 * @method static Builder|Bonus whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|Bonus whereUnitMeasurement($value)
 * @method static Builder|Bonus whereUpdatedAt($value)
 * @method static Builder|Bonus withTranslation($locale = null, $fallback = true)
 * @method static Builder|Bonus withTranslations($locales = null, $fallback = true)
 * @method static \Illuminate\Database\Query\Builder|Bonus withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Bonus withoutTrashed()
 * @mixin Eloquent
 * @property int $position
 * @property-write mixed $disable_position_update
 * @property-write mixed $position_column
 * @method static Builder|Bonus sorted($way = 'ASC')
 * @method static Builder|Bonus sortedByDESC()
 * @method static Builder|Bonus wherePosition($value)
 */
class Bonus extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;

    protected array $translatable = ['description'];
}
