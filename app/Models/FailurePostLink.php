<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FailurePostLink extends Model
{
    use HasFactory;

    protected $fillable = [
        'amount',
        'currency',
        'message',
        'code',
        'accountId',
        'invoiceId'
    ];
}
