<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\PharmacyTab
 *
 * @property int                                                         $id
 * @property string                                                      $title
 * @property int                                                         $position Позиция
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab newQuery()
 * @method static Builder|PharmacyTab onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab query()
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyTab withTranslations($locales = null, $fallback = true)
 * @method static Builder|PharmacyTab withTrashed()
 * @method static Builder|PharmacyTab withoutTrashed()
 * @mixin Eloquent
 */
class PharmacyTab extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;
    protected $translatable = ['title'];
}
