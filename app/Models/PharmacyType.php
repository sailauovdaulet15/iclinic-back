<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\PharmacyType
 *
 * @property int                             $id
 * @property string                          $title
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @property-read null                       $translated
 * @property-read Collection|Pharmacy[]      $pharmacies
 * @property-read int|null                   $pharmacies_count
 * @property-read Collection|Translation[]   $translations
 * @property-read int|null                   $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyType newQuery()
 * @method static Builder|PharmacyType onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyType query()
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyType whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyType whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyType withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|PharmacyType withTranslations($locales = null, $fallback = true)
 * @method static Builder|PharmacyType withTrashed()
 * @method static Builder|PharmacyType withoutTrashed()
 * @mixin Eloquent
 */
class PharmacyType extends Model
{
    use HasFactory, Translatable, SoftDeletes;
    protected $translatable = ['title'];

    public function pharmacies(): HasMany
    {
        return $this->hasMany(Pharmacy::class);
    }
}
