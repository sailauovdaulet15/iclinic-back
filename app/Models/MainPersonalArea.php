<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Pion\Support\Eloquent\Position\Traits\PositionTrait;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\MainPersonalArea
 *
 * @property int                                                         $id
 * @property string                                                      $title
 * @property string                                                      $description
 * @property string                                                      $image_uri
 * @property int                                                         $position Позиция
 * @property Carbon|null                             $created_at
 * @property Carbon|null                             $updated_at
 * @property Carbon|null                             $deleted_at
 * @property-read null                                                   $translated
 * @property-write mixed                                                 $disable_position_update
 * @property-write mixed                                                 $position_column
 * @property-read Collection|Translation[] $translations
 * @property-read int|null                                               $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea newQuery()
 * @method static Builder|MainPersonalArea onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea query()
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea sorted($way = 'ASC')
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea sortedByDESC()
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea whereImageUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea withTranslation($locale = null, $fallback = true)
 * @method static \Illuminate\Database\Eloquent\Builder|MainPersonalArea withTranslations($locales = null, $fallback = true)
 * @method static Builder|MainPersonalArea withTrashed()
 * @method static Builder|MainPersonalArea withoutTrashed()
 * @mixin Eloquent
 * @property-read mixed $image
 */
class MainPersonalArea extends Model
{
    use HasFactory, Translatable, SoftDeletes, PositionTrait;
    protected $translatable = ['title', 'description'];
    protected $appends = ['image'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'id',
        'image_uri'
    ];

    public function getImageAttribute(){
        return Storage::disk(config('voyager.storage.disk'))->url($this->image_uri);
    }
}
