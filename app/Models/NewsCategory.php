<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Traits\Translatable;

/**
 * App\Models\NewsCategory
 *
 * @property int                             $id
 * @property string                          $title
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read null                       $translated
 * @property-read Collection|News[]          $news
 * @property-read int|null                   $news_count
 * @property-read Collection|Translation[]   $translations
 * @property-read int|null                   $translations_count
 * @method static Builder|NewsCategory newModelQuery()
 * @method static Builder|NewsCategory newQuery()
 * @method static Builder|NewsCategory query()
 * @method static Builder|NewsCategory whereCreatedAt($value)
 * @method static Builder|NewsCategory whereId($value)
 * @method static Builder|NewsCategory whereTitle($value)
 * @method static Builder|NewsCategory whereTranslation(string $field, string $operator, string $value = null, string $locales = null, string $default = true)
 * @method static Builder|NewsCategory whereUpdatedAt($value)
 * @method static Builder|NewsCategory withTranslation($locale = null, $fallback = true)
 * @method static Builder|NewsCategory withTranslations($locales = null, $fallback = true)
 * @mixin Eloquent
 */
class NewsCategory extends Model
{
    use HasFactory, Translatable;
    protected $translatable = ['title'];

    public function news(): HasMany
    {
        return $this->hasMany(News::class);
    }

}
