<?php

namespace App\Soap;

use App\Constants\EnvironmentConstant;
use App\Constants\LanguageConstant;
use App\Enums\SexEnum;
use App\Models\Appointment;
use App\Models\Country;
use App\Models\PostLink;
use App\Models\User;
use App\Soap\Actions\CreateHistoryClient;
use App\Soap\Actions\GetClientHistory;
use App\Soap\Request\CancelAppointment;
use App\Soap\Request\Client;
use App\Soap\Request\CreateAppointment;
use App\Soap\Request\HistoryVisit;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Illuminate\Support\Facades\Log;
use stdClass;

class SoapService
{
    protected SoapWrapper $soap;

    public function __construct()
    {
        $this->soap = new SoapWrapper();
        $this->soap->add('Iclinic', function ($service) {
            $service
                ->wsdl(config('soap.wsdl'))
                ->trace(true)
                ->options([
                    'login'    => config('soap.options.login'),
                    'password' => config('soap.options.password'),
                ]);
        });
    }

    /**
     * Сервис по проверке ИИН
     * Возвращает либо пусто если нет в 1с
     * либо объект со всеми данными в 1с
     *
     * @param $iin
     *
     * @return array|null
     */
    public function proverkaIIN($iin)
    {
        $model       = $this->soap->call('Iclinic.proverkaIIN', [
            "proverkaIIN" =>
                [
                    'IIN' => $iin,
                ],
        ]);
        $returnModel = $model->return;

        return self::registerTransformer($returnModel);
    }

    // todo make class return instead of array
    private static function registerTransformer($model)
    {
        if ((array) $model) {
            return [
                'first_name'             => $model->name,
                'second_name'            => $model->surname,
                'third_name'             => $model->middlename,
                'name'                   => $model->IIN,
                'date_birth'             => $model->dateBirth,
                'is_limited_by_mobility' => $model->patientDisabled,
                'nationality_id'         => Country::where('code_alfa_2', $model->nationality)->first()->id,
                'phone'                  => $model->phone != null ? self::checkPhone($model->phone) : '',
                'phone2'                 => $model->phone2 != null ? self::checkPhone($model->phone2) : '',
                'language'               => LanguageConstant::getKey($model->language),
                'gender'                 => SexEnum::getKey($model->gender),
                'password'               => $model->password,
            ];
        }

        return null;
    }

    private static function checkPhone($phone)
    {
        if(preg_match('!(\b\+?[0-9()\[\]./ -]{7,17}\b|\b\+?[0-9()\[\]./ -]{7,17}\s+(extension|x|#|-|code|ext)\s+[0-9]{1,6})!i',$phone)){
            preg_match_all('/\d+/', $phone, $matches);
            $phone = implode('', $matches[0]);
            if (strlen($phone) == 11) {
                return substr($phone, 1);
            } elseif (strlen($phone) == 10) {
                return $phone;
            }
        }
        throw new \Exception("Phone number is incorrect: $phone");

    }

    /**
     * @param User $user
     *
     * @return string returns IIN of the user
     */
    public function newClient(User $user): string
    {
        $userClient = Client::fromUserModel($user);
        $iin        = ($this->soap->call('Iclinic.NewClient', [
            "NewClient" =>
                [
                    "Client" => $userClient,
                ],
        ]))->return;

        return $iin;
    }

    /**
     * Отправляет смс сообщение на номер телефона
     *
     * @param $phone
     * @param $smsCode
     */
    public function sendSMSMessage($phone, $smsCode)
    {
        if (config('app.env') !== EnvironmentConstant::TESTING) {
            $this->soap->call('Iclinic.sendSMSToRecoverPassword', [
                "sendSMSToRecoverPassword" =>
                    [
                        'phoneNumber' => $this->mutatePhone($phone),
                        'code'        => $smsCode,
                    ],
            ]);
        }
    }

    private function mutatePhone($phone)
    {
        return strlen($phone) < 11 ? "+7{$phone}" : $phone;
    }

    public function createAppointment(Appointment $appointment)
    {
        $item = CreateAppointment::fromAppointmentModel($appointment);
        $item->log();
        $result = $this->soap->call('Iclinic.createAppointment', [
            "createAppointment" =>
                $item,
        ])->return;
        return $result;
    }

    public function getClientVisitHistory($clientIIN): array
    {
        $documents = json_decode($this->soap->call('Iclinic.getClientVisitHistory', [
            "getClientVisitHistory" =>
                [
                    'IIN' => $clientIIN,
                ],
        ])->return)->documents;

        return GetClientHistory::execute($documents);
    }

    public function createHistoryVisit(PostLink $postLink)
    {
        $historyVisit = CreateHistoryClient::execute($postLink);
        $historyVisit->log();

        return $this->soap->call('Iclinic.createHistoryVisit', [
            "createAppointment" =>
                $historyVisit->toArray(),
        ])->return;
    }

    public function cancelAppointment(Appointment $appointment)
    {
        $item = CancelAppointment::fromAppointmentModel($appointment);
        $item->log();

        return $this->soap->call('Iclinic.cancelAppointment', [
            "cancelAppointment" =>
                $item->toArray(),
        ]);
    }
}
