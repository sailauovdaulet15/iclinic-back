<?php

namespace App\Soap\Actions;

use App\Models\Appointment;
use App\Models\PostLink;
use App\Soap\Request\HistoryVisit;
use Illuminate\Support\Carbon;

class CreateHistoryClient
{
    public static function execute(PostLink $postLink): HistoryVisit
    {
        $appointment = $postLink->appointment;

        $services = $appointment->services->map(function ($service) {
            return [
                'service' => $service->uid,
                'serviceName' => $service->title,
                'amount' => 1,
                'sum' => $service->priceWithDiscount,
            ];
        });

        $data = [
            'ID' => $appointment->invoiceId,
            'clientIIN' => $appointment->user->name,
            'doctorIIN' => $appointment->doctor->user->name,
            'date' => $appointment->date,
            'room' => $appointment->cabinet_num,
            'dateStart' => Carbon::parse($appointment->start_time)->format('Y-m-d H:i:s'),
            'dateEnd' => Carbon::parse($appointment->end_time)->format('Y-m-d H:i:s'),
            'services' => json_encode($services),
            'payment_sum' => $postLink->amount,
            'appointment_id' => $appointment->uid,
            'checking' => false
        ];

        return HistoryVisit::fromArray($data);
    }
}
