<?php

namespace App\Soap\Actions;

use App\Models\User;
use App\Soap\Types\AppointmentHistory;

class GetClientHistory
{

    public static function execute(array $documents): array
    {
        $result = [];
        foreach ($documents as $appointment) {
            $result[] = AppointmentHistory::fromAppointment($appointment);
        }
        return $result;
    }
}
