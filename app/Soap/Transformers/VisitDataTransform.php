<?php

namespace App\Soap\Transformers;

use Illuminate\Support\Carbon;

class VisitDataTransform
{
    /**
     * @var string
     */
    public string $date;
    /**
     * @var string
     */
    public string $start_time;
    /**
     * @var string
     */
    public string $end_time;
    /**
     * @var integer
     */
    public int $cabinet_num;
    /**
     * @var boolean
     */
    public bool $time_mode;

    public function __construct($date, $object)
    {
        $this->date = Carbon::parse($date)->format('Y-m-d');
        $this->start_time = $object['TimeBegin'];
        $this->end_time = $object['TimeEnd'];
        $this->cabinet_num = (int) $object['Cabinet'];
        $this->time_mode = $object['TimeMode'];
    }
}
