<?php

namespace App\Soap\Transformers;

use App\Models\Admin\AdminUser;
use App\Models\User;
use Illuminate\Support\Str;

class VisitTransform
{
    public int $user_id;

    public array $visits;

    public static function fromAssociativeJson($iin, $data): self
    {
        $self = new self();

        $value         = $data;
        $self->user_id = AdminUser::firstOrCreateNotActive($iin)->id;

        foreach ($value as $date => $items) {
            foreach ($items as $item) {
                $self->visits[] = new VisitDataTransform($date, $item);
            }
        }

        return $self;
    }
}
