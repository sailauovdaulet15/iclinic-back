<?php

namespace App\Soap;

use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use SoapFault;
use Viewflex\Zoap\Demo\DemoProvider as Provider;

class IclinicService
{
    /*
   |--------------------------------------------------------------------------
   | Public Methods
   |--------------------------------------------------------------------------
   */

    /**
     * Authenticates user/password, returning status of true with token, or throws SoapFault.
     *
     * @param string $user
     * @param string $password
     *
     * @return array
     * @throws SoapFault
     */
    public function auth($user, $password)
    {

        if (Provider::validateUser($user, $password)) {
            return ['status' => 'true', 'token' => Provider::getToken($user)];
        } else {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }

    }


    /**
     * Returns boolean authentication result using given token or user/password.
     *
     * @param string $token
     * @param string $user
     * @param string $password
     *
     * @return bool
     */
    public function ping($token = '', $user = '', $password = '')
    {
        return Provider::authenticate($token, $user, $password);
    }


    /**
     * Add Specialisation
     *
     * @param string $uid
     * @param string $name_ru
     * @param string $name_kz
     * @param string $name_en
     * @param string $token
     *
     * @return string
     * @throws SoapFault
     */
    public function addSpecialisation($uid, $name_ru, $name_kz = '', $name_en = '', $token = '')
    {
        if (!$uid) {
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Please specify uid of specialisation.');
        }

        if (!Provider::authenticate($token)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }
        try {
            return ServiceProvider::createSpecialisation($uid, $name_ru, $name_en, $name_kz);
        } catch (Exception $e) {
            Log::channel('soap')->error($e->getMessage());
        }
    }


    /**
     * @param string  $uid
     * @param string  $specialisation
     * @param string  $name_ru
     * @param string  $name_kz
     * @param string  $name_en
     * @param int     $duration
     * @param boolean $visible
     * @param string  $reception_type
     * @param boolean $is_online
     * @param boolean $is_appointment
     * @param string  $token
     *
     * @return string
     * @throws SoapFault
     */
    public function addService(
        string $uid,
        string $specialisation,
        string $name_ru,
        string $name_kz = '',
        string $name_en = '',
        int $duration = 0,
        bool $visible,
        string $reception_type,
        bool $is_online,
        bool $is_appointment,
        string $token = ''
    ): string {
        if (!$uid) {
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Please specify uid of service.');
        }

        if (!Provider::authenticate($token)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }
        try {
            return ServiceProvider::createService(
                $uid, $specialisation, $name_ru,
                $name_en, $name_kz,
                $duration, $visible, $reception_type,
                $is_online, $is_appointment
            );
        } catch (Exception $exception) {
            Log::channel('soap')->error($exception->getMessage());
        }
    }

    /**
     * Add Service Price
     *
     * @param string $uid
     * @param int    $price
     * @param string $token
     *
     * @return string
     * @throws SoapFault
     */
    public function addServicePrice(
        $uid,
        $price,
        $token = ''
    ) {
        if (!$uid) {
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Please specify uid of service.');
        }

        if (!Provider::authenticate($token)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }
        try {
            return ServiceProvider::addServicePrice($uid, $price);
        } catch (Exception $e) {
            Log::channel('soap')->error($e->getMessage());
        }
    }

    /**
     * Add Service Price
     *
     * @param string $IIN
     * @param string $services
     * @param string $service_percent
     * @param string $start_date
     * @param string $form_of_participation
     * @param string $status
     * @param string $sum_of
     * @param string $token
     *
     * @return array
     * @throws SoapFault
     */
    public function addUserService(
        string $IIN,
        string $services,
        string $service_percent,
        string $start_date,
        string $form_of_participation,
        string $status,
        string $sum_of,
        string $token = ''
    ): array {
        if (!$IIN) {
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Please specify product id.');
        }

        if (!Provider::authenticate($token)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }
        try {
            return ServiceProvider::addUserToService(
                $IIN, $services, $service_percent,
                $start_date, $form_of_participation, $status,
                $sum_of
            );
        } catch (Exception $e) {
            Log::channel('soap')->error($e->getMessage());
        }
    }

    /**
     * Add Service Price
     *
     * @param string $IIN
     * @param int    $services
     * @param string $token
     *
     * @return array
     * @throws SoapFault
     */
    public function removeUserService(
        $IIN,
        $services,
        $token = ''
    ) {
        if (!$IIN) {
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Please specify product id.');
        }

        if (!Provider::authenticate($token)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }
        try {
            return ServiceProvider::removeUserFromService(
                $IIN, $services);
        } catch (Exception $e) {
            Log::channel('soap')->error($e->getMessage());
        }
    }


    /**
     * Add Service Price
     *
     * @param string $uid
     * @param string $json
     * @param string $token
     *
     * @return array
     * @throws SoapFault
     */
    public function addVisitTable(
        $uid,
        $json,
        $token = ''
    ) {
        if (!$uid) {
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Please specify product id.');
        }

        if (!Provider::authenticate($token)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }
        try {
            return ServiceProvider::addVisit(
                $uid, $json);
        } catch (Exception $e) {
            Log::channel('soap')->error($e->getMessage());
        }
    }

    /**
     * Add Person
     *
     * @param string  $surname
     * @param string  $name
     * @param string  $middlename
     * @param string  $IIN
     * @param string  $email
     * @param string  $dateBirth
     * @param boolean $patientDisabled
     * @param string  $nationality
     * @param string  $gender
     * @param string  $phone
     * @param string  $phone2
     * @param string  $language
     * @param string  $password
     * @param string  $legalAgent
     * @param string  $specialisation
     * @param string  $services
     * @param string  $group_id
     * @param string  $country
     * @param string  $region
     * @param string  $city
     * @param string  $postcode
     * @param string  $address
     * @param string  $category
     * @param string  $stazh
     * @param string  $activeStatus
     * @param integer $agentPercent
     * @param boolean $visible
     * @param integer $duration
     * @param string  $ageHealsFrom
     * @param string  $ageHealsTo
     * @param string  $token
     *
     * @return array
     * @throws SoapFault
     */
    public function addPerson(
        string $surname,
        string $name,
        string $middlename,
        string $IIN,
        string $email,
        string $dateBirth,
        bool $patientDisabled,
        string $nationality,
        string $gender,
        string $phone,
        string $phone2,
        string $language,
        string $password,
        string $legalAgent,
        string $specialisation,
        string $services,
        string $group_id,
        string $country,
        string $region,
        string $city,
        string $postcode,
        string $address,
        string $category,
        string $stazh,
        string $activeStatus,
        int $agentPercent,
        bool $visible,
        int $duration,
        string $ageHealsFrom,
        string $ageHealsTo,
        string $token = ''
    ): array {
        if (!$IIN) {
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Please specify product id.');
        }

        if (!Provider::authenticate($token)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }
        try {
            return ServiceProvider::
            addPerson($surname, $name,
                $middlename, $IIN, $email,
                $dateBirth, $patientDisabled, $nationality,
                $gender, $phone, $phone2,
                $language, $password, $legalAgent,
                $specialisation, $services, $group_id,
                $country, $region, $city,
                $postcode, $address, $category,
                $stazh, $activeStatus, $agentPercent,
                $visible, $duration, $ageHealsFrom, $ageHealsTo);
        } catch (Exception $e) {
            Log::channel('soap')->error($e->getMessage());
        }
    }

    /**
     * Add appointment
     *
     * @param string $uid
     * @param string $doctor
     * @param string $created_at
     * @param string $starts_at
     * @param string $ends_at
     * @param string $cabinet_num
     * @param string $services
     * @param string $patient
     * @param string $token
     *
     * @return array
     * @throws SoapFault
     */
    public function addAppointment(
        string $uid,
        string $doctor,
        string $created_at,
        string $starts_at,
        string $ends_at,
        string $cabinet_num,
        string $services,
        string $patient,
        string $token = ''
    ): array {
        Log::channel('soap')->info($services);
        if (!$uid) {
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Please specify product id.');
        }

        if (!Provider::authenticate($token)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }
        try {
            return ServiceProvider::addAppointment($uid, $doctor, $created_at,
                $starts_at, $ends_at, $cabinet_num,
                $services, $patient);
        } catch (Exception $e) {
            Log::channel('soap')->error($e->getMessage());
        }
    }

    /**
     * @param string $uid
     * @param string $client_iin
     * @param string $doctor_iin
     * @param string $date
     * @param string $room
     * @param string $date_start
     * @param string $services
     * @param string $payments
     * @param string $token
     *
     * @return array
     * @throws SoapFault
     */
    public function addHistoryVisit(
        string $uid,
        string $client_iin,
        string $doctor_iin,
        string $date,
        string $room,
        string $date_start,
        string $services,
        string $payments,
        string $token = ''
    ): array {
        if (!$uid) {
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Please specify product id.');
        }

        if (!Provider::authenticate($token)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }
        try {
            return ServiceProvider::addHistoryVisit($uid, $client_iin, $doctor_iin,
                $date, $room, $date_start,
                $services, $payments);
        } catch (Exception $e) {
            Log::channel('soap')->error($e->getMessage());
        }
    }

    /**
     * @param string $uid
     * @param string $token
     *
     * @return array
     * @throws SoapFault
     */
    public function cancelAppointment(
        string $uid,
        string $token = ''
    ): array {
        if (!$uid) {
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Please specify product id.');
        }

        if (!Provider::authenticate($token)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }
        try {
            return ServiceProvider::cancelAppointment($uid);
        } catch (Exception $e) {
            Log::channel('soap')->error($e->getMessage());
        }
    }

    /**
     * @param string $uid
     * @param string $appointmentId
     * @param string $historyId
     * @param string $client_iin
     * @param string $doctor_iin
     * @param string $date
     * @param string $files
     * @param string $token
     *
     * @return array
     * @throws SoapFault
     */
    public function addMedicineCard(
        string $uid,
        string $appointmentId,
        string $historyId,
        string $client_iin,
        string $doctor_iin,
        string $date,
        string $files,
        string $token = ''
    ): array {
        if (!$uid) {
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Please specify product id.');
        }

        if (!Provider::authenticate($token)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }
        try {
            return ServiceProvider::addMedicineCard($uid, $appointmentId, $historyId,
                $client_iin, $doctor_iin,
                $date, $files);
        } catch (Exception $e) {
            Log::channel('soap')->error($e->getMessage());
        }
    }

    /**
     * @param string $uid
     * @param string $token
     *
     * @return array
     * @throws SoapFault
     */
    public function cancelHistoryVisit(string $uid, string $token = ''): array
    {
        if (!$uid) {
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Please specify product id.');
        }

        if (!Provider::authenticate($token)) {
            header("Status: 401");
            throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
        }
        try {
            return ServiceProvider::cancelHistoryVisit($uid);
        } catch (Exception $e) {
            Log::channel('soap')->error($e->getMessage());
        }

    }

}
