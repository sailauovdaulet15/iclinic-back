<?php

namespace App\Soap\Request;

use Illuminate\Support\Facades\Log;

class HistoryVisit
{

    //private string $ID;
    private string $clientIIN;
    private string $doctorIIN;
    private string $date;
    private string $room;
    private string $dateStart;
    private string $dateEnd;
    private string $services;
    private string $payment_sum;
    private string $idAppointment;
    private bool $checking;

    public static function fromArray(array $data)
    {
        $self = new self();

        //$self->ID = $data['ID'];
        $self->clientIIN     = $data['clientIIN'];
        $self->doctorIIN     = $data['doctorIIN'];
        $self->date          = $data['date'];
        $self->room          = $data['room'] ?? '';
        $self->dateStart     = $data['dateStart'];
        $self->dateEnd       = $data['dateEnd'];
        $self->services      = $data['services'];
        $self->payment_sum   = $data['payment_sum'];
        $self->idAppointment = $data['appointment_id'];
        $self->checking      = $data['checking'];

        return $self;
    }

    public function toArray()
    {
        return [
            //'ID' => $this->ID,
            'clientIIN'     => $this->clientIIN,
            'doctorIIN'     => $this->doctorIIN,
            'date'          => $this->date,
            'room'          => $this->room,
            'dateStart'     => $this->dateStart,
            'dateEnd'       => $this->dateEnd,
            'services'      => $this->services,
            'payment_sum'   => $this->payment_sum,
            'idAppointment' => $this->idAppointment,
            'checking'      => $this->checking,
        ];
    }

    public function log($channel = 'soap', $message = 'Create: HistoryVisit')
    {
        Log::channel($channel)->info($message, $this->toArray());
    }

    public function getClientIIN(): string
    {
        return $this->clientIIN;
    }

    public function getDoctorIIN(): string
    {
        return $this->doctorIIN;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function getRoom(): string
    {
        return $this->room;
    }

    public function getDateStart(): string
    {
        return $this->dateStart;
    }

    public function getDateEnd(): string
    {
        return $this->dateEnd;
    }

    public function getServices(): string
    {
        return $this->services;
    }

    public function getPaymentSum(): string
    {
        return $this->payment_sum;
    }

    public function getID(): string
    {
        return $this->ID;
    }
}
