<?php

namespace App\Soap\Request;

use App\Constants\LanguageConstant;
use App\Enums\HandbookHealCategoryEnum;
use App\Enums\RoleEnum;
use App\Enums\SexEnum;
use App\Models\User;

class Client
{
    protected ?string $surname;
    protected ?string $name;
    protected ?string $middlename;
    protected ?string $IIN;
    protected ?string $email;
    protected ?string $dateBirth;
    protected ?bool $patientDisabled;
    protected ?string $nationality;
    protected ?string $gender;
    protected ?string $phone;
    protected ?string $phone2;
    protected ?string $language;
    protected ?string $password;
    protected ?string $legalAgent;
    protected ?string $specialisation;
    protected ?string $services;
    protected ?string $group_id;
    protected ?string $country;
    protected ?string $region;
    protected ?string $city;
    protected ?string $postcode;
    protected ?string $address;
    // doctor fields
    protected ?string $category;
    protected ?string $stazh;
    protected ?string $activeStatus;
    protected ?int $agentPercent;
    protected ?bool $visible;
    protected ?int $duration;
    protected ?string $ageHealsFrom;
    protected ?string $ageHealsTo;

    public function __construct(?string $surname = null, ?string $name = null, ?string $middlename = null,
                                ?string $IIN = null, ?string $email = null, ?string $dateBirth = null,
                                ?bool   $patientDisabled = null, ?string $nationality = null,
                                ?string $gender = null, ?string $phone = null, ?string $phone2 = null,
                                ?string $language = null, ?string $password = null, ?string $legalAgent = null,
                                ?string $specialisation = null, ?string $services = null, ?string $group_id = null,
                                ?string $country = null, ?string $region = null, ?string $city = null,
                                ?string $postcode = null, ?string $address = null, ?string $category = null,
                                ?string $stazh = null, ?string $activeStatus = null, ?int $agentPercent = null,
                                ?bool   $visible = null, ?int $duration = null, ?string $ageHealsFrom = null,
                                ?string $ageHealsTo = null)
    {
        $this->surname = $surname;
        $this->name = $name;
        $this->middlename = $middlename;
        $this->IIN = $IIN;
        $this->email = $email;
        $this->dateBirth = $dateBirth;
        $this->patientDisabled = $patientDisabled;
        $this->nationality = $nationality;
        $this->gender = $gender;
        $this->phone = $phone;
        $this->phone2 = $phone2;
        $this->language = $language;
        $this->password = $password;
        $this->legalAgent = $legalAgent;
        $this->specialisation = $specialisation;
        $this->services = $services;
        $this->group_id = $group_id;
        $this->country = $country;
        $this->region = $region;
        $this->city = $city;
        $this->postcode = $postcode;
        $this->address = $address;
        $this->category = $category;
        $this->stazh = $stazh;
        $this->activeStatus = $activeStatus;
        $this->agentPercent = $agentPercent;
        $this->visible = $visible;
        $this->duration = $duration;
        $this->ageHealsFrom = $ageHealsFrom;
        $this->ageHealsTo = $ageHealsTo;
    }


    public static function fromUserModel(User $user)
    {
        $class = new static();
        $class->surname = $user->second_name;
        $class->name = $user->first_name;
        $class->middlename = $user->third_name ?? null;
        $class->IIN = $user->name;
        $class->email = $user->email ?? null;
        $class->dateBirth = $user->date_birth ?? null;
        $class->patientDisabled = $user->is_limited_by_mobility;
        $class->nationality = $user->nationality->code_alfa_2;
        $class->gender = SexEnum::getTitle($user->sex);
        $class->phone = $user->phone;
        $class->phone2 = $user->phone2 ?? null;
        $class->language = LanguageConstant::getTitle($user->language);
        $class->password = $user->password;
        $class->legalAgent = optional($user->legalAgent)->name;
        $class->specialisation = $class->specialisationTransformed($user);
        $class->services = false;
        $class->group_id = RoleEnum::get1cRoleId(optional($user->role)->name ?? RoleEnum::USER);
        $class->country = optional($user->country)->code_alfa_2 ?? null;
        $class->region = $user->region ?? null;
        $class->city = $user->city ?? null;
        $class->postcode = $user->postcode ?? null;
        $class->address = $user->address ?? null;

        $class->category = optional($user->doctor)->category;
        $class->stazh = optional($user->doctor)->experience;
        $class->activeStatus = optional($user->doctor)->active_status;
        $class->agentPercent = optional($user->doctor)->agent_percent;
        $class->visible = (bool)optional($user->doctor)->visible;
        $class->duration = optional($user->doctor)->duration;
        $class->ageHealsFrom = HandbookHealCategoryEnum::getAgeHealsFrom(optional(optional($user->doctor)->handbookHealCategory)->type);
        $class->ageHealsTo = HandbookHealCategoryEnum::getAgeHealsTo(optional(optional($user->doctor)->handbookHealCategory)->type);

        return $class;
    }

    public function specialisationTransformed($user)
    {
        if ($specs = optional($user->doctor)->specialisations) {
            return implode(';', $specs->pluck('uid'));
        }
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getMiddlename(): ?string
    {
        return $this->middlename;
    }

    /**
     * @return string
     */
    public function getIIN(): string
    {
        return $this->IIN;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getDateBirth(): ?string
    {
        return $this->dateBirth;
    }

    /**
     * @return bool
     */
    public function isPatientDisabled(): bool
    {
        return $this->patientDisabled;
    }

    /**
     * @return string
     */
    public function getNationality(): string
    {
        return $this->nationality;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return string|null
     */
    public function getPhone2(): ?string
    {
        return $this->phone2;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return mixed|string|null
     */
    public function getLegalAgent()
    {
        return $this->legalAgent;
    }

    /**
     * @return string|null
     */
    public function getSpecialisation(): ?string
    {
        return $this->specialisation;
    }

    /**
     * @return string|null
     */
    public function getServices(): ?string
    {
        return $this->services;
    }

    /**
     * @return string
     */
    public function getGroupId(): string
    {
        return $this->group_id;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @return mixed|string|null
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @return mixed|string|null
     */
    public function getStazh()
    {
        return $this->stazh;
    }

    /**
     * @return mixed|string|null
     */
    public function getActiveStatus()
    {
        return $this->activeStatus;
    }

    /**
     * @return int|mixed|null
     */
    public function getAgentPercent()
    {
        return $this->agentPercent;
    }

    /**
     * @return bool|mixed|null
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * @return int|mixed|null
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @return int|string|null
     */
    public function getAgeHealsFrom()
    {
        return $this->ageHealsFrom;
    }

    /**
     * @return int|string|null
     */
    public function getAgeHealsTo()
    {
        return $this->ageHealsTo;
    }


}
