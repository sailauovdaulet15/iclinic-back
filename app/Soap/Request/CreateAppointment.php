<?php

namespace App\Soap\Request;

use App\Models\Appointment;
use Illuminate\Support\Facades\Log;

class CreateAppointment
{
    protected ?string $clientIIN;
    protected ?string $doctorIIN;
    protected ?string $startDate;
    protected ?string $endDate;
    protected ?string $roomNumber;
    protected ?string $services;
    protected ?bool $checking;

    public static function fromAppointmentModel(Appointment $appointment, $checking = false): CreateAppointment
    {
        $class            = new static;
        $class->clientIIN = $appointment->user->name;
        $class->doctorIIN = $appointment->doctor->user->name;

        $class->startDate  = "$appointment->date $appointment->start_time";
        $class->endDate    = "$appointment->date $appointment->end_time";
        $class->roomNumber = $appointment->cabinet_num;
        // todo сделать валидацию чтобы делал проверку на наличие UID
        $class->services = optional($appointment->services)->pluck('uid')->implode(';');
        $class->checking = $checking;

        return $class;
    }

    /**
     * @return string|null
     */
    public function getClientIIN(): ?string
    {
        return $this->clientIIN;
    }

    /**
     * @return string|null
     */
    public function getDoctorIIN(): ?string
    {
        return $this->doctorIIN;
    }

    /**
     * @return string|null
     */
    public function getStartDate(): ?string
    {
        return $this->startDate;
    }

    /**
     * @return string
     */
    public function getEndDate(): string
    {
        return $this->endDate;
    }

    /**
     * @return string|null
     */
    public function getRoomNumber(): ?string
    {
        return $this->roomNumber;
    }

    /**
     * @return string|null
     */
    public function getServices(): ?string
    {
        return $this->services;
    }

    public function getChecking(): ?bool
    {
        return $this->checking;
    }

    public function toArray(): array
    {
        return [
            'clientIIN'  => $this->clientIIN,
            'doctorIIN'  => $this->doctorIIN,
            'startDate'  => $this->startDate,
            'endDate'    => $this->endDate,
            'roomNumber' => $this->roomNumber,
            'services'   => $this->services,
            'checking'   => $this->checking,
        ];
    }

    public function log($channel = 'soap', $message = 'Create: appointment')
    {
        Log::channel($channel)->info($message, $this->toArray());
    }
}
