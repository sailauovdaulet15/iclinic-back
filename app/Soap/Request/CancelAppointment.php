<?php
namespace App\Soap\Request;

use App\Models\Appointment;
use Illuminate\Support\Facades\Log;

class CancelAppointment
{
    protected string $uid;

    public static function fromAppointmentModel(Appointment $appointment): self
    {
        $class = new static;
        $class->uid = $appointment->uid;
        return $class;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->uid,
        ];
    }

    public function log($channel = 'soap', $message = 'Create: appointment')
    {
        Log::channel($channel)->info($message, $this->toArray());
    }
}
