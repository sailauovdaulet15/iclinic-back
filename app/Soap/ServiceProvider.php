<?php

namespace App\Soap;

use App\Enums\StatusEnum;
use App\Jobs\Soap\AddAppointmentJob;
use App\Jobs\Soap\AddHistoryVisitJob;
use App\Jobs\Soap\AddMedicineCardJob;
use App\Jobs\Soap\AddPersonJob;
use App\Jobs\Soap\AddServicePriceJob;
use App\Jobs\Soap\AddUserToServiceJob;
use App\Jobs\Soap\AddVisitJob;
use App\Jobs\Soap\CancelAppointmentJob;
use App\Jobs\Soap\CancelHistoryVisitJob;
use App\Jobs\Soap\CreateServiceJob;
use App\Jobs\Soap\CreateSpecialisationJob;
use App\Models\DoctorService;
use App\Models\FormOfParticipation;
use App\Models\HistoryVisit;
use App\Models\HistoryVisitPayment;
use App\Models\Service;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Log;

class ServiceProvider
{

    /**
     * Creates a specialization.
     *
     * @param string $uid
     * @param string $name_ru
     * @param string $name_en
     * @param string $name_kz
     *
     * @return string
     */
    public static function createSpecialisation(string $uid, string $name_ru, string $name_en, string $name_kz): string
    {
        $insertData = compact('uid', 'name_ru', 'name_en', 'name_kz');
        Log::channel('soap')->info('createSpecialisation: insertData', $insertData);

        CreateSpecialisationJob::dispatch($insertData);

        return $uid;
    }

    /**
     * Creates a service.
     *
     * @param string  $uid
     * @param string  $specialisation
     * @param string  $name_ru
     * @param string  $name_en
     * @param string  $name_kz
     * @param int     $duration
     * @param boolean $visible
     * @param string  $reception_type
     * @param bool    $is_online
     *
     * @return string
     */
    public static function createService(
        string $uid,
        string $specialisation,
        string $name_ru,
        string $name_en,
        string $name_kz,
        int $duration,
        bool $visible,
        string $reception_type,
        bool $is_online,
        bool $is_appointment
    ): string {
        $insertData = compact('uid', 'specialisation', 'name_ru',
            'name_en', 'name_kz', 'duration',
            'visible', 'reception_type', 'is_online', 'is_appointment');
        Log::channel('soap')->info('createService: insertData', $insertData);

        CreateServiceJob::dispatch($insertData);

        return $uid;
    }

    /**
     * Add Service Price.
     *
     * @param string $uid
     * @param int    $price
     *
     * @return string
     */
    public static function addServicePrice(string $uid, int $price)
    {
        $insertData = compact('uid', 'price');
        Log::channel('soap')->info('addServicePrice: insertData', $insertData);

        AddServicePriceJob::dispatch($insertData);

        return $uid;
    }

    /**
     * @param string $IIN
     * @param string $services
     * @param double $service_percent
     * @param string $start_date
     * @param string $form_of_participation
     * @param string $status
     * @param double $sum_of
     *
     * @return array
     */
    public static function addUserToService(
        string $IIN,
        string $services,
        float $service_percent,
        string $start_date,
        string $form_of_participation,
        string $status,
        float $sum_of
    ): array {

        $insertData = compact('IIN',
            'services', 'service_percent',
            'start_date', 'form_of_participation', 'sum_of', 'status');
        Log::channel('soap')->info('addUserToService: insertData', $insertData);

        AddUserToServiceJob::dispatch($insertData);

        return ["ok"];
    }

    /**
     * @param string $IIN
     * @param string $services
     *
     * @return array
     */
    public static function removeUserFromService(string $IIN, string $services)
    {

        $insertData = compact('IIN', 'services');
        Log::channel('soap')->info('removeUserFromService: insertData', $insertData);
        $user = User::where('name', $IIN)->has('doctor')->firstOrFail();

        Service::find($services)->doctors()->detach($user->doctor);

        return ['service removed from doctor'];
    }

    /**
     * @param string $uid
     * @param string $json
     *
     * @return array
     */
    public static function addVisit(string $uid, string $json)
    {
        $insertData = compact('uid', 'json');
        Log::channel('soap')->info('addVisit: insertData', $insertData);

        AddVisitJob::dispatch($insertData);


        return ['ok'];
    }

    /**
     * Add Person
     *
     * @param string  $surname
     * @param string  $name
     * @param string  $middlename
     * @param string  $IIN
     * @param string  $email
     * @param string  $dateBirth
     * @param boolean $patientDisabled
     * @param string  $nationality
     * @param string  $gender
     * @param string  $phone
     * @param string  $phone2
     * @param string  $language
     * @param string  $password
     * @param string  $legalAgent
     * @param string  $specialisation
     * @param string  $services
     * @param string  $group_id
     * @param string  $country
     * @param string  $region
     * @param string  $city
     * @param string  $postcode
     * @param string  $address
     * @param string  $category
     * @param string  $stazh
     * @param string  $activeStatus
     * @param integer $agentPercent
     * @param boolean $visible
     * @param integer $duration
     * @param string  $ageHealsFrom
     * @param string  $ageHealsTo
     *
     * @return array
     * @throws Exception
     */
    public static function addPerson(
        string $surname,
        string $name,
        string $middlename,
        string $IIN,
        string $email,
        string $dateBirth,
        bool $patientDisabled,
        string $nationality,
        string $gender,
        string $phone,
        string $phone2,
        string $language,
        string $password,
        string $legalAgent,
        string $specialisation,
        string $services,
        string $group_id,
        string $country,
        string $region,
        string $city,
        string $postcode,
        string $address,
        string $category,
        string $stazh,
        string $activeStatus,
        int $agentPercent,
        bool $visible,
        int $duration,
        string $ageHealsFrom,
        string $ageHealsTo
    ) {
        $insertData = compact('surname', 'name', 'middlename',
            'IIN', 'email', 'dateBirth',
            'patientDisabled', 'nationality', 'gender',
            'phone', 'phone2', 'language',
            'password', 'legalAgent', 'specialisation',
            'services', 'group_id', 'country',
            'region', 'city', 'postcode',
            'address', 'category', 'stazh',
            'activeStatus', 'agentPercent', 'visible',
            'duration', 'ageHealsFrom', 'ageHealsTo');
        Log::channel('soap')->info('addPerson: insertData', $insertData);
        // проверка зашифрован ли уже пароль

        AddPersonJob::dispatch($insertData);

        return ['ok'];
    }

    /**
     * Add appointment
     *
     * @param string $uid
     * @param string $doctor
     * @param string $created_at
     * @param string $starts_at
     * @param string $ends_at
     * @param string $cabinet_num
     * @param string $services
     * @param string $patient
     *
     * @return array
     */
    public static function addAppointment(
        string $uid,
        string $doctor,
        string $created_at,
        string $starts_at,
        string $ends_at,
        string $cabinet_num,
        string $services,
        string $patient
    ): array {
        $insertData = compact('uid', 'doctor', 'created_at', 'starts_at', 'ends_at',
            'starts_at', 'ends_at', 'cabinet_num', 'services', 'patient');
        Log::channel('soap')->info('addAppointment: insertData', $insertData);

        AddAppointmentJob::dispatch($insertData);

        return ['ok'];
    }

    public static function addHistoryVisit(
        string $uid,
        string $client_iin,
        string $doctor_iin,
        string $date,
        string $room,
        string $date_start,
        string $services,
        string $payments
    ): array {
        $insertData = compact('uid', 'client_iin', 'doctor_iin', 'date', 'room',
            'date_start', 'services', 'payments');
        Log::channel('soap')->info('addHistoryVisit: insertData', $insertData);
        AddHistoryVisitJob::dispatch($insertData);

        return ['ok'];
    }

    public static function cancelAppointment(
        string $uid
    ): array {
        $insertData = compact('uid');
        Log::channel('soap')->info('cancelAppointment: insertData', $insertData);
        CancelAppointmentJob::dispatch($insertData);

        return ['ok'];
    }

    public static function addMedicineCard(
        string $uid,
        string $appointmentId,
        string $historyId,
        string $client_iin,
        string $doctor_iin,
        string $date,
        string $files
    ): array {
        $insertData = compact('uid', 'appointmentId', 'historyId', 'client_iin', 'doctor_iin', 'date', 'files');
        Log::channel('soap')->info('addMedicineCard: insertData', $insertData);
        AddMedicineCardJob::dispatch($insertData);

        return ['ok'];
    }

    public static function cancelHistoryVisit($uid): array
    {
        $insertData = compact('uid');
        Log::channel('soap')->info('cancelHistoryVisit: insertData', $insertData);
        CancelHistoryVisitJob::dispatch($insertData);

        return ['ok'];
    }
}
