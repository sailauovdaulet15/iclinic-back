<?php

namespace App\Soap\Types;

use App\Enums\HandbookOrderTypeEnum;
use App\Models\User;
use Illuminate\Support\Carbon;

class AppointmentHistory
{
    private ?string $ID;

    private ?string $doctorIIN;

    private ?string $date;

    private ?string $paymentType;

    /**
     *
     * @var ServiceAppointment[]
     */
    private ?array $services;

    /**
     * @param string|null $ID
     * @param string|null $doctorIIN
     * @param string|null $date
     * @param string|null $paymentType
     * @param ServiceAppointment[] $services
     */
    public function __construct(?string $ID = null, ?string $doctorIIN = null,
                                ?string $date = null, ?string $paymentType = null,
                                ?array $services = null)
    {
        $this->ID = $ID;
        $this->doctorIIN = $doctorIIN;
        $this->date = $date;
        $this->paymentType = $paymentType;
        $this->services = $services;
    }

    public static function fromAppointment($appointment): AppointmentHistory
    {
        $class = new static();
        $class->ID = $appointment->ID;
        $class->doctorIIN = $appointment->doctorIIN;
        $class->date = $appointment->date;
        $class->paymentType = $appointment->paymentType;
        $services = [];
        foreach ($appointment->services as $service) {
            $services[] = ServiceAppointment::fromServiceObject($service);
        }
        $class->services = $services;
        return $class;
    }

    public function getDoctorUser()
    {
        return User::where('name', '=', $this->doctorIIN)->first();
    }

    public function getDate(): Carbon
    {
        return Carbon::parse($this->date);
    }

    public function getId(): ?string
    {
        return $this->ID;
    }

    public function getPaymentType(): ?string
    {
        return HandbookOrderTypeEnum::getKey($this->paymentType);
    }

    /**
     * @return ServiceAppointment[]|null
     */
    public function getServices(): ?array
    {
        return $this->services;
    }

    public function getTotalSum(): int
    {
        return array_reduce($this->services, function($carry, $item) {
            $carry += $item->getSum();
            return $carry;
        });
    }

}
