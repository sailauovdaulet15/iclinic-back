<?php

namespace App\Soap\Types;

class ServicePrice
{
    /**
     * @var int
     */
    public $uid = 1;

    /**
     * @var int
     */
    public $price;

    public function __construct($uid, $price)
    {
        $this->uid = $uid;
        $this->price = $price;
    }

}
