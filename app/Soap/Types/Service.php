<?php

namespace App\Soap\Types;

class Service
{
    /**
     * @var string
     */
    public $uid = 1;

    /**
     * @var string
     */
    public $name_ru;

    /**
     * @var string
     */
    public $name_kz;

    /**
     * @var string
     */
    public $name_en;

    /**
     * @var string
     */
    public $specialisation;

    /**
     * @var int
     */
    public $duration;

    /**
     * @var boolean
     */
    public $visible;


    /**
     * Service constructor.
     *
     * @param string $uid
     * @param string $name_ru
     * @param string $name_en
     * @param string $name_kz
     * @param string $specialisation
     * @param int $duration
     * @param boolean $visible
     */
    public function __construct($uid, $name_ru, $name_en, $name_kz, $specialisation, $duration, $visible)
    {
        $this->uid = $uid;
        $this->name_ru = $name_ru;
        $this->name_en = $name_en;
        $this->name_kz = $name_kz;
        $this->specialisation = $specialisation;
        $this->duration = $duration;
        $this->visible = $visible;
    }
}
