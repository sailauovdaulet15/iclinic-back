<?php

namespace App\Soap\Types;

class Specialisation
{
    /**
     * @var string
     */
    public $uid = 1;

    /**
     * @var string
     */
    public $name_ru;

    /**
     * @var string
     */
    public $name_kz;

    /**
     * @var string
     */
    public $name_en;

    /**
     * Specialisation constructor.
     *
     * @param $uid
     * @param string $name_ru
     * @param string $name_en
     * @param string $name_kz
     */
    public function __construct($uid, $name_ru, $name_en = '', $name_kz = '')
    {
        $this->uid = $uid;
        $this->name_ru = $name_ru;
        $this->name_en = $name_en;
        $this->name_kz = $name_kz;
    }
}
