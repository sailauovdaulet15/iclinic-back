<?php

namespace App\Soap\Types;

class ServiceAppointment
{
    /**
     * Service UID
     * @var string|null
     */
    private ?string $service;

    private ?string $amount;

    private ?string $sum;

    /**
     * @param string|null $service
     * @param string|null $amount
     * @param string|null $sum
     */
    public function __construct(?string $service = null, ?string $amount = null, ?string $sum = null)
    {
        $this->service = $service;
        $this->amount = $amount;
        $this->sum = $sum;
    }

    public static function fromServiceObject($service): ServiceAppointment
    {
        $class = new static();
        $class->service = $service->service;
        $class->amount = $service->amount;
        $class->sum = $service->sum;
        return $class;
    }

    public function getService(): ?\App\Models\Service
    {
        return \App\Models\Service::where('uid', $this->service)->first();
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function getSum(): ?string
    {
        return $this->sum;
    }

}
