<?php

namespace App\Exceptions;


use Exception;

class AppointmentCantBeSavedException extends Exception
{
    public function render($request) {
        $message = $this->getMessage() ?? "Заявка не может быть сохранена";
        return response()->json(["error" => true, "message" => $message], 400);
    }
}
