<?php

namespace App\Exceptions;


use Exception;

class WaitToSendSMSException extends Exception
{
    public function render($request) {
        return response()->json(["error" => true, "message" => "Wrong role " . $this->getMessage()]);
    }
}
