<?php

namespace App\Exceptions;

use Exception;

class IncorrectSmsCodeException extends Exception
{
    public function render($request)
    {
        return response()->json(["error" => true, "message" => $this->getMessage()]);
    }
}
