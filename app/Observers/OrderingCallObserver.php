<?php

namespace App\Observers;

use App\Jobs\SendOrderingCallEmail;
use App\Models\OrderingCall;
use App\Services\SendpulseService;

class OrderingCallObserver
{

    /**
     * Handle the OrderingCall "created" event.
     *
     * @param OrderingCall $orderingCall
     *
     * @return void
     */
    public function created(OrderingCall $orderingCall)
    {
        $maxCount = config('sendpulse.max_requests_count');
        $delay = config('sendpulse.limit_time');
        if (($count = OrderingCall::query()->where('created_at', '>', now()->subSeconds($delay))->count()) > $maxCount) {
            $delay = $delay * ceil($count / $maxCount);
            $secondsToWait = $delay - (now()->diffInSeconds(OrderingCall::query()->orderBy('created_at')->where('created_at', '>', now()->subSeconds($delay))->first()->created_at));
            SendOrderingCallEmail::dispatch($orderingCall)->delay($secondsToWait);
        } else {
            SendOrderingCallEmail::dispatch($orderingCall);
        }
    }

    /**
     * Handle the OrderingCall "updated" event.
     *
     * @param OrderingCall $orderingCall
     *
     * @return void
     */
    public function updated(OrderingCall $orderingCall)
    {
        //
    }

    /**
     * Handle the OrderingCall "deleted" event.
     *
     * @param OrderingCall $orderingCall
     *
     * @return void
     */
    public function deleted(OrderingCall $orderingCall)
    {
        //
    }

    /**
     * Handle the OrderingCall "restored" event.
     *
     * @param OrderingCall $orderingCall
     *
     * @return void
     */
    public function restored(OrderingCall $orderingCall)
    {
        //
    }

    /**
     * Handle the OrderingCall "force deleted" event.
     *
     * @param OrderingCall $orderingCall
     *
     * @return void
     */
    public function forceDeleted(OrderingCall $orderingCall)
    {
        //
    }
}
