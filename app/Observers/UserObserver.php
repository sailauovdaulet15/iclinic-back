<?php

namespace App\Observers;

use App\Models\User;
use App\Soap\SoapService;

class UserObserver
{
    /**
     * Handle the User "created" event.
     *
     * @param User $user
     * @return false|void
     */
    public function creating(User $user)
    {
        // генерация уникального ИИН и сохранения в базу данных 1С
        if (empty($user->name)) {
            $user->name = app(SoapService::class)->newClient($user);
            if (User::whereName($user->name)->exists()) {
                return false;
            }
        }
    }


//    public function saved(User $user)
//    {
//        // send request to 1C
//        app(SoapService::class)->newClient($user);
//    }
}
