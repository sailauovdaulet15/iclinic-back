<?php


namespace App\Enums;


class BookStatusEnum
{
    const FREE = 1;
    const BOOKED = 0;

    public static function getTitle(int $key): string
    {
        switch ($key) {
            case self::FREE:
                return 'Свободно';
            case self::BOOKED:
                return 'Забронировано';
        }
    }

}
