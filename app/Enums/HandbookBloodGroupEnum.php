<?php


namespace App\Enums;


class HandbookBloodGroupEnum
{
    const A = '0 (I) +';
    const B = '0 (I) -';
    const C = 'A(II) +';
    const D = 'A(II) -';
    const E = 'B(III) +';
    const F = 'B(III) -';
    const G = 'AB(IV) +';
    const H = 'AB(IV) -';
    const I = 'Неизвестно';


    public static function getConstants(): array
    {
        return [
            self::A,
            self::B,
            self::C,
            self::D,
            self::E,
            self::F,
            self::G,
            self::H,
            self::I,
        ];
    }
}
