<?php

namespace App\Enums;

class FormOfParticipationEnum
{
    const PROVIDES = "provides";
    const DIRECTS  = "directs";

    public static function getConstants(): array
    {
        return [
            self::PROVIDES => 'оказывает',
            self::DIRECTS  => 'направляет',
        ];
    }

    public static function getKey($status): string
    {
        return self::getConstants()[$status];
    }
}
