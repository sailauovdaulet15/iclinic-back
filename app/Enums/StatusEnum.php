<?php

namespace App\Enums;

class StatusEnum
{
    const WORKS = 1;
    const STOPS = 2;

    public static function getConstants(): array
    {
        return [
            self::WORKS => 'действует',
            self::STOPS => 'прекращено'
        ];
    }

    /**
     * Возвращаем статус и проверяем по русским записям
     * которые приходят в 1с
     * @param $status
     * @return int
     */
    public static function getKey($status): int
    {
        foreach (self::getConstants() as $key => $value) {
            if ($value == $status) {
                return $key;
            }
        }
    }
}
