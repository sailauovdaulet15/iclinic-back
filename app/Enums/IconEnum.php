<?php


namespace App\Enums;


class IconEnum
{
    const FIRST_ICON = 'first_icon';
    const SECOND_ICON = 'second_icon';


    public static function getDefault(): string
    {
        return self::getConstants()[array_key_first(self::getConstants())];
    }


    public static function getConstants(): array
    {
        return [
            self::FIRST_ICON => 'Первая иконка',
            self::SECOND_ICON => 'Вторая иконка'
        ];
    }
}
