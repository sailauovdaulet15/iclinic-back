<?php


namespace App\Enums;


class HandbookOrderTypeEnum
{
    const ON_SITE  = 'on_site';
    const IN_CASH  = 'in_cash';
    const BY_FIRM  = 'by_firm';
    const CASHLESS = 'cashless';
    const KASPI_QR = 'kaspi_qr';

    public static function getTitle(string $key): string
    {
        switch ($key) {
            case self::ON_SITE:
                return 'На сайте';
            case self::BY_FIRM:
                return 'За счет фирмы';
            case self::IN_CASH:
                return 'Наличными';
            case self::CASHLESS:
                return 'Безналичные';
            case self::KASPI_QR:
                return 'KaspiQR';
            default:
                return 'Не найдено';
        }
    }


    public static function getTitleWithKey(): array
    {
        return [
            self::ON_SITE  => self::getTitle(self::ON_SITE),
            self::IN_CASH  => self::getTitle(self::IN_CASH),
            self::BY_FIRM  => self::getTitle(self::BY_FIRM),
            self::CASHLESS => self::getTitle(self::CASHLESS),
            self::KASPI_QR => self::getTitle(self::KASPI_QR),
        ];
    }

    public static function getKey(string $title): ?string
    {
        foreach (self::getTitleWithKey() as $key => $value) {
            if ($value == $title) {
                return $key;
            }
        }

        return null;
    }
}
