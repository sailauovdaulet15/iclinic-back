<?php


namespace App\Enums;


class AppointmentEnum
{
    protected const REDIS_APPOINTMENT = 'user:appointment:';
    protected const REDIS_APPOINTMENT_SERVICE = 'user:appointment_service:';

    public function getKeyAppointment(): string
    {
        return self::REDIS_APPOINTMENT . $this->getAuthUser();
    }

    public  function getKeyServices(): string
    {
        return self::REDIS_APPOINTMENT_SERVICE . $this->getAuthUser();
    }

    public function getAuthUser()
    {
        return auth('api')->user()->id;
    }
}
