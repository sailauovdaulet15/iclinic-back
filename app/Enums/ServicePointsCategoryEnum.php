<?php


namespace App\Enums;


class ServicePointsCategoryEnum
{
    const HUMAN = 'human';
//    const WOMAN = 'woman';
    const OTHER = 'other';

    public static function getTitle(string $key): string
    {
        switch ($key) {
            case self::HUMAN:
                return 'Человек';
            case self::OTHER:
                return 'Другие';
            default:
                return 'Не найдено';
        }
    }

    public static function getConstants(): array
    {
        return [
            self::HUMAN => 'Человек',
            self::OTHER => 'Другие',
        ];
    }
}
