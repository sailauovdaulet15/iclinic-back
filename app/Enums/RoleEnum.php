<?php

namespace App\Enums;

use App\Exceptions\WrongRoleIdException;

class RoleEnum
{
    const USER = "user";
    const ADMIN = "admin";
    const DOCTOR = "doctor";
    const DOCTOR_AGENTS = "doctor_agents";

    public static function getConstants(): array
    {
        return [
            self::USER,
            self::ADMIN,
            self::DOCTOR,
            self::DOCTOR_AGENTS
        ];
    }

    public static function getKeyFrom1C($role_id): string
    {
        switch ($role_id){
            case 6:
                return self::DOCTOR;
            case 8:
                return self::USER;
            case 10:
                return self::DOCTOR_AGENTS;
            default:
                throw new WrongRoleIdException($role_id);
        }
    }

    public static function get1CConstants(): array
    {
        return [
            self::DOCTOR => 6,
            self::USER => 8,
            self::DOCTOR_AGENTS => 10
        ];
    }

    public static function get1cRoleId($type): int
    {
        return self::get1CConstants()[$type];
    }

}
