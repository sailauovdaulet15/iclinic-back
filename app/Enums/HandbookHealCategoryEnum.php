<?php


namespace App\Enums;


class HandbookHealCategoryEnum
{
    const ADULTS_AND_CHILDREN = 'adults_and_children';
    const ADULTS              = 'adults';
    const CHILDREN            = 'children';

    public static function getConstants(): array
    {
        return [
            self::ADULTS_AND_CHILDREN,
            self::ADULTS,
            self::CHILDREN,
        ];
    }

    public static function getTitle($type): string
    {
        switch ($type) {
            case self::ADULTS_AND_CHILDREN:
                return 'Взрослых и детей';
            case self::ADULTS:
                return 'Взрослых';
            case self::CHILDREN:
                return 'Детей';
            default:
                return 'Не найдено';
        }
    }

    public static function getFormattedType($from, $to): string
    {
        if ($from == 0 && $to == 0) {
            return self::ADULTS_AND_CHILDREN;
        } elseif ($from == 0 && $to <= 18) {
            return self::CHILDREN;
        } elseif ($from >= 18 && $to == 0) {
            return self::ADULTS;
        }
    }

    public static function get1CConstants(): array
    {
        return [
            self::ADULTS_AND_CHILDREN => [0, 0],
            self::ADULTS              => [18, 0],
            self::CHILDREN            => [0, 18],
        ];
    }

    public static function getAgeHealsFrom($type): ?int
    {
        return self::get1CConstants()[$type][0] ?? null;
    }

    public static function getAgeHealsTo($type): ?int
    {
        return self::get1CConstants()[$type][1] ?? null;
    }
}
