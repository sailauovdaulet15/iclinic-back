<?php


namespace App\Enums;


class AppointmentStatusEnum
{
    const BOOKED = 1;
    const CANCELLED = 2;

    public static function getTitle(string $key): string
    {
        switch ($key) {
            case self::BOOKED:
                return 'Записан';
            case self::CANCELLED:
                return 'Отменен';
            default:
                return 'Не найдено';
        }
    }
}
