<?php

namespace App\Enums;

class NationalityEnum
{
    const KZ = "kz";
    const EN = "en";
    const RU = "ru";

    public static function getConstants(): array
    {
        return [
            self::KZ,
            self::RU,
            self::EN
        ];
    }
}
