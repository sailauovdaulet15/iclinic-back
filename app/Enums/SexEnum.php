<?php

namespace App\Enums;

class SexEnum
{
    const MAN = "man";
    const WOMAN = "woman";

    public static function getConstants(): array
    {
        return [
            self::MAN,
            self::WOMAN
        ];
    }

    public static function getTitle($key): string
    {
        switch ($key){
            case self::MAN:
                return 'Мужской';
            case self::WOMAN:
                return 'Женский';
            default:
                return 'НеУказан';
        }
    }

    public static function getTitleWithKey(): array
    {
        return [
            self::MAN => self::getTitle(self::MAN),
            self::WOMAN => self::getTitle(self::WOMAN)
        ];
    }

    public static function getKey(string $title): ?string
    {
        foreach (self::getTitleWithKey() as $key => $value) {
            if ($value == $title) {
                return $key;
            }
        }
        return null;
    }
}
