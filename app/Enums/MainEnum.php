<?php


namespace App\Enums;


class MainEnum
{
    const MAIN = 'sliders';
    const SERVICES = 'services';
    const SERVICE_POINTS = 'service_points';
    const PERSONAL_AREA = 'personal_areas';

    public static function getTitle(string $key): string
    {
        switch ($key) {
            case self::MAIN:
                return 'Слайдеры';
            case self::SERVICE_POINTS:
                return 'Точки Услуг';
            case self::SERVICES:
                return 'Почему мы';
            case self::PERSONAL_AREA:
                return 'ЛК (о клинике)';
            default:
                return 'Не найдено';
        }
//        return match ($key) {
//            self::MAIN => 'Главная',
//            self::SERVICES => 'Услуги',
//            self::ABOUT_US => 'О нас',
//            self::PERSONAL_AREA => 'Личный кабинет',
//            self::PROMOTIONS => 'Акции',
//            default => 'Не найдено',
//        };
    }

    public static function getConstants(): array
    {
        return [
            self::MAIN => 'sliders',
            self::SERVICES => 'services',
            self::SERVICE_POINTS => 'service_points',
            self::PERSONAL_AREA => 'personal_areas',
        ];
    }
}
