<?php


namespace App\Enums;


class AboutUsEnum
{
    const ACHIEVEMENT = 'achievements';
    const BRANCH = 'branches';
    const LICENSE = 'licenses';
    const SLIDER = 'sliders';
    const WHY_US = 'why-us';

    public static function getTitle(string $key): string
    {
        switch ($key) {
            case self::ACHIEVEMENT:
                return 'Достижения';
            case self::BRANCH:
                return 'Филиалы';
            case self::LICENSE:
                return 'Лицензии';
            case self::SLIDER:
                return 'Слайдер';
            case self::WHY_US:
                return 'Почему мы?';
            default:
                return 'Не найдено';
        }
//        match ($key) {
//            self::ACHIEVEMENT => 'Достижения',
//            self::BRANCH => 'Филиалы',
//            self::LICENSE => 'Лицензии',
//            self::SLIDER => 'Слайдер',
//            self::WHY_US => 'Почему мы?',
//            default => 'Не найдено',
//        };
    }
    public static function getConstants(): array
    {
        return [
            self::ACHIEVEMENT => 'achievements',
            self::BRANCH => 'branches',
            self::LICENSE => 'licenses',
            self::SLIDER => 'sliders',
            self::WHY_US => 'why-us',
        ];
    }
}
