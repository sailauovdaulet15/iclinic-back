<?php

namespace App\Jobs\Soap;

use App\Enums\HandbookOrderTypeEnum;
use App\Models\Admin\AdminService;
use App\Models\HandbookOrderType;
use App\Models\HistoryVisit;
use App\Models\PostLink;
use App\Models\User;
use App\Soap\Actions\CreateHistoryClient;
use App\Soap\SoapService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateHistoryVisitJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var PostLink
     */
    public $postLink;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(PostLink $postLink)
    {
        $this->postLink = $postLink;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /** @var SoapService $soapService */
        $soapService = app(SoapService::class);
        $uid = $soapService->createHistoryVisit($this->postLink);
        $data = CreateHistoryClient::execute($this->postLink);
        $h = HistoryVisit::create([
            'uid' => $uid,
            'total_sum' => 0,
            'doctor_user_id' => User::whereName($data->getDoctorIIN())->first()->id,
            'user_id' => User::whereName($data->getClientIIN())->first()->id,
            'date' => $data->getDate(),
            'date_start' => $data->getDateStart(),
            'room' => $data->getRoom(),
            'is_active' => true
        ]);

        foreach (json_decode($data->getServices()) as $service) {
            $serviceExist        = AdminService::query()
                ->firstOrCreate(['uid' => $service->service], ['name' => $service->serviceName])
                ->id;
            $h->services()->attach($serviceExist, [
                'amount'                 => $service->amount,
                'sum'                    => $service->sum,
            ]);
        }

        $handbookOrderType = HandbookOrderType::query()
            ->firstOrCreate(['type' => HandbookOrderTypeEnum::ON_SITE]);
        $h->payments()->attach($handbookOrderType->id, [
            'sum'            => $data->getPaymentSum(),
        ]);
    }
}
