<?php

namespace App\Jobs\Soap;

use App\Constants\LanguageConstant;
use App\Enums\HandbookHealCategoryEnum;
use App\Enums\RoleEnum;
use App\Enums\SexEnum;
use App\Models\Admin\AdminDoctor;
use App\Models\Admin\AdminUser;
use App\Models\Country;
use App\Models\Doctor;
use App\Models\HandbookHealCategory;
use App\Models\Nationality;
use App\Models\Specialisation;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Hash;
use TCG\Voyager\Models\Role;

class AddPersonJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public array $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \App\Exceptions\WrongRoleIdException
     */
    public function handle()
    {
        $data          = $this->data;
        $validPassword =
            Hash::info($data['password'])['algoName'] === 'bcrypt' ? $data['password'] : Hash::make($data['password']);
        $insertData    = [
            'email'                  => $data['email'],
            'phone'                  => $data['phone'],
            'password'               => $validPassword,
            'role_id'                => Role::whereName(RoleEnum::getKeyFrom1C($data['group_id']))->firstOrFail()->id,
            'first_name'             => $data['name'],
            'second_name'            => $data['surname'],
            'third_name'             => $data['middlename'],
            'nationality_id'         => Country::where('code_alfa_2', mb_strtoupper($data['nationality']))->first()->id,
            'sex'                    => SexEnum::getKey($data['gender']),
            'language'               => LanguageConstant::getKey($data['language']),
            'is_limited_by_mobility' => $data['patientDisabled'],
            'postcode'               => $data['postcode'],
            'phone2'                 => $data['phone2'],
            'address'                => $data['address'],
            'date_birth'             => $data['dateBirth'],
            'country_id'             => optional(Country::where('code_alfa_2', mb_strtoupper($data['country']))
                ->first())->id,
            'region'                 => $data['region'],
            'city'                   => $data['city'],
            'legal_agent_id'         => optional(User::where('name', $data['legalAgent'])->first())->id,
            'is_active'              => true
            // todo переписать для валидации
        ];
        $IIN           = $data['IIN'];
        $user          = AdminUser::withoutEvents(function () use ($insertData, $IIN) {
            $user = AdminUser::updateOrCreate([
                'name' => $IIN,
            ], $insertData);
            $user->generateSlug();
            $user->save();

            return $user;
        }
        );

        if ($user->hasRole('doctor')) {
            $doctor = AdminDoctor::updateOrCreate(['user_id' => $user->id], [
                'experience'                => $data['stazh'],
                'is_visible'                => $data['visible'],
                'agent_percent'             => $data['agentPercent'],
                'duration'                  => $data['duration'],
                'active_status'             => $data['activeStatus'],
                'category'                  => $data['category'],
                'handbook_heal_category_id' => optional(HandbookHealCategory::whereType(
                    HandbookHealCategoryEnum::getFormattedType($data['ageHealsFrom'], $data['ageHealsTo'])
                )->first())->id,
            ]);

            $doctor->specialisations()
                ->sync(
                    Specialisation::query()
                        ->whereIn('uid', explode(';', $data['specialisation']))
                        ->pluck('id')
                );
        }

    }
}
