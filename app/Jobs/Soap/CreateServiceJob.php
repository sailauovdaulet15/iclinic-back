<?php

namespace App\Jobs\Soap;

use App\Constants\LanguageConstant;
use App\Models\Admin\AdminService;
use App\Models\ReceptionType;
use App\Models\Specialisation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateServiceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public array $data;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;

        $service = AdminService::updateOrCreate(['uid' => $data['uid']], [
            'title'             => $data['name_ru'],
            'specialisation_id' => optional(Specialisation::where('uid', $data['specialisation'])->first())->id,
            'is_visible'        => $data['visible'],
            'is_online'         => $data['is_online'],
            'is_appointment'    => $data['is_appointment'],
            'reception_type_id' => optional(ReceptionType::where('title', $data['reception_type'])->first())->id,
        ]);

        $service        = $service->translate(LanguageConstant::EN);
        $service->title = $data['name_en'];
        $service->save();
        $service        = $service->translate(LanguageConstant::KZ);
        $service->title = $data['name_kz'];
        $service->save();

    }
}
