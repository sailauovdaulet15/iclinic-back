<?php

namespace App\Jobs\Soap;

use App\Enums\StatusEnum;
use App\Models\DoctorService;
use App\Models\FormOfParticipation;
use App\Models\Service;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddUserToServiceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public array $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $d             = $this->data;
        $doctorService = DoctorService::query()->updateOrCreate([
            'doctor_user_id'           => User::where('name', $d['IIN'])->has('doctor')->firstOrFail()->id,
            'service_id'               => Service::where('uid', $d['services'])->firstOrFail()->id,
            'form_of_participation_id' => FormOfParticipation::where('title', mb_strtolower($d['form_of_participation']))
                ->firstOrFail()->id,
        ], [
            'service_percent' => $d['service_percent'],
            'start_date'      => $d['start_date'],
            'sum_of'          => $d['sum_of'],
            'status'          => StatusEnum::getKey(mb_strtolower($d['status'])),
        ]);

    }
}
