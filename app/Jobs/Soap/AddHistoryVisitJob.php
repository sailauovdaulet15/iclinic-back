<?php

namespace App\Jobs\Soap;

use App\Enums\RoleEnum;
use App\Models\Admin\AdminService;
use App\Models\Admin\AdminUser;
use App\Models\HandbookOrderType;
use App\Models\HistoryVisit;
use App\Models\HistoryVisitPayment;
use App\Models\Service;
use App\Models\User;
use Faker\Provider\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddHistoryVisitJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public array $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data         = $this->data;
        $services     = json_decode($data['services']);
        $payments     = json_decode($data['payments']);
        $doctor       = AdminUser::firstOrCreateNotActive($data['doctor_iin'], RoleEnum::DOCTOR)->id;
        $user         = AdminUser::firstOrCreateNotActive($data['client_iin'])->id;
        $historyVisit = HistoryVisit::query()
            ->updateOrCreate(
                ['uid' => $data['uid']],
                [
                    'total_sum'      => 0,
                    'doctor_user_id' => $doctor,
                    'user_id'        => $user,
                    'date'           => $data['date'],
                    'date_start'     => $data['date_start'],
                    'room'           => $data['room'],
                    'is_active'      => true
                ]
            );

        $servicesToSync = [];
        foreach ($services as $service) {
            $serviceExist = AdminService::query()
                ->firstOrCreate(['uid' => $service->service], ['name' => $service->serviceName])
                ->id;
            $servicesToSync[$serviceExist] = [
                'amount' => $service->amount,
                'sum'    => $service->sum,
            ];
        }
        $historyVisit->services()->sync($servicesToSync);

        $paymentsToSync = [];
        foreach ($payments as $payment) {
            $handbookOrderType = HandbookOrderType::query()
                ->firstOrCreate(['name' => $payment->paymentType]);

            $paymentsToSync[$handbookOrderType->id] = ['sum' => $payment->sum];

        }

        $historyVisit->payments()->sync($paymentsToSync);
    }
}
