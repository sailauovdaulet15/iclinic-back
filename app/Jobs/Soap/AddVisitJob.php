<?php

namespace App\Jobs\Soap;

use App\Models\Visit;
use App\Soap\Transformers\VisitTransform;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddVisitJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public array $data;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $d = $this->data;

        $data = json_decode($d['json'], true);
        foreach ($data as $iin => $item) {
            $visitData      = VisitTransform::fromAssociativeJson($iin, $item);
            $visitDataField = [
                'fields' => [
                    "date"        => "День",
                    "start_time"  => "Начало",
                    "end_time"    => "Окончание",
                    "cabinet_num" => "Номер кабинета",
                    "time_mode"   => "Рабочее время",
                ],
                'rows'   => $visitData->visits,
            ];
            $visitModel     = Visit::updateOrCreate([
                'uid'     => $d['uid'],
                'user_id' => $visitData->user_id,
            ], [
                // todo переписать в трансформер или мутатор
                'data' => json_encode($visitDataField),
            ]);
        }
    }
}
