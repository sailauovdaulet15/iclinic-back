<?php

namespace App\Jobs\Soap;

use App\Enums\AppointmentStatusEnum;
use App\Models\Admin\AdminService;
use App\Models\Admin\AdminUser;
use App\Models\Appointment;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class AddAppointmentJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public array $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;

        $d = AdminUser::firstOrCreateNotActive($data['doctor'])->id;
        $p =  AdminUser::firstOrCreateNotActive($data['patient'])->id;

        $insertData = [
            'doctor_user_id' => $d,
            'patient_id'     => $p,
            'created_at'     => $data['created_at'],
            'date'           => $data['starts_at'],
            'start_time'     => $data['starts_at'],
            'end_time'       => $data['ends_at'],
            'cabinet_num'    => empty($data['cabinet_num']) ? null : intval($data['cabinet_num']),
            'status' => AppointmentStatusEnum::BOOKED,
            'is_online_payment' => false
        ];

        $services = $data['services'];
        $uid = $data['uid'];
        $appointment = Appointment::withoutEvents(function () use ($services, $uid, $insertData) {
            $a = Appointment::firstOrCreate(
                [
                    'uid' => $uid,
                ],
                $insertData
            );
            if (!$a->wasRecentlyCreated) {
                $a->update($insertData);
            }

            $servicesList = AdminService::whereIn('uid', explode(';', $services))->pluck('id');
            $a->services()->sync($servicesList);

            return $a;
        }
        );
    }
}
