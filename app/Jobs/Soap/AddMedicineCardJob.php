<?php

namespace App\Jobs\Soap;

use App\Models\Appointment;
use App\Models\HistoryVisit;
use App\Models\MedicineCard;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class AddMedicineCardJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;

        $files            = json_decode($data['files']);
        $history_visit_id = !empty($data['historyId'])
            ? optional(HistoryVisit::whereUid($data['historyId'])->first())->id
            :
            null;
        /** @var MedicineCard $medicineCard */
        $medicineCard = MedicineCard::updateOrCreate([
            'uid' => $data['uid'],
        ], [
            'appointment_id'   => Appointment::whereUid($data['appointmentId'])->first()->id,
            'history_visit_id' => $history_visit_id,
        ]);

        if ($medicineCard->files()->exists()) {
            $medicineCard->files()->delete();
        }

        foreach ($files as $file) {
            $filePath = $this->generatePath('medicine-card', $file);

            Storage::disk(config('voyager.storage.disk'))
                ->put($filePath, base64_decode($file->base64), 'public');

            $medicineCard->files()->create([
                'file_name' => $file->sample_name,
                'file_path' => $filePath,
            ]);
        }
    }

    private function generatePath($name, $file): string
    {
        return $name . DIRECTORY_SEPARATOR . date('FY') . DIRECTORY_SEPARATOR . $file->file_name .
            $file->extension;
    }
}
