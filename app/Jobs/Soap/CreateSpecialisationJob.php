<?php

namespace App\Jobs\Soap;

use App\Constants\LanguageConstant;
use App\Models\Specialisation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CreateSpecialisationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public array $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;

        DB::transaction(function () use ($data) {
            $specialisation = Specialisation::updateOrCreate(['uid' => $data['uid']], [
                'title' => $data['name_ru'],
                'slug'  => Str::slug($data['name_ru']),
            ]);

            $specialisation        = $specialisation->translate(LanguageConstant::EN);
            $specialisation->title = $data['name_en'];
            $specialisation->slug  = Str::slug($data['name_en']);
            $specialisation->save();
            $specialisation        = $specialisation->translate(LanguageConstant::KZ);
            $specialisation->title = $data['name_kz'];
            $specialisation->slug  = Str::slug($data['name_kz']);
            $specialisation->save();
        });
    }
}
