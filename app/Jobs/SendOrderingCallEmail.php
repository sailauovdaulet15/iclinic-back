<?php

namespace App\Jobs;

use App\Models\OrderingCall;
use App\Services\SendpulseService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendOrderingCallEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public OrderingCall $orderingCall;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(OrderingCall $orderingCall)
    {
        $this->queue = 'Mailing';
        $this->orderingCall = $orderingCall;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        app(SendpulseService::class)->sendEmails($this->orderingCall);
    }
}
