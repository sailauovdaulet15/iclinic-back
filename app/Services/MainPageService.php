<?php


namespace App\Services;


use App\Enums\MainEnum;
use App\Http\Resources\MainPersonalResource;
use App\Http\Resources\MainServiceResource;
use App\Http\Resources\MainSliderResource;
use App\Http\Resources\ServicePointsCategoryResource;
use App\Models\Main;
use App\Models\MainPersonalArea;
use App\Models\MainService;
use App\Models\MainSlider;
use App\Models\ServicePointsCategory;

class MainPageService
{

    public function getUrls()
    {
        $list = Main::pluck('type', 'title');

        return $list->map(function ($val) {
            return route('v1.main.show', [$val]);
        });
    }

    public function takeMainSeparately($type)
    {
        switch ($type) {
            case MainEnum::MAIN:
                return MainSliderResource::collection(MainSlider::orderBy('position')->get());
            case MainEnum::SERVICES:
                return MainServiceResource::collection(MainService::all());
            case MainEnum::PERSONAL_AREA:
                return MainPersonalResource::collection(MainPersonalArea::all());
            case MainEnum::SERVICE_POINTS:
                $servicePointsWithoutDescription = ServicePointsCategory::query()
                    ->with(['servicePoints' => fn($q) => $q->orderBy('id', 'desc'), 'servicePoints.specialisation.serviceLanding'])
                    ->get();
                // берем все эти связи сразу eager loading'ом потому что используем их внутри
                // ServicePointResource который внутри ServicePointsCategoryResource
                return ServicePointsCategoryResource::collection($servicePointsWithoutDescription);
        }
    }
}
