<?php

namespace App\Services;

use App\Models\Stock;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Voyager;

class ChangeAvatarService
{
    /**
     * @param $request
     * @return string|null
     */
    public function changeAvatar($request): ?string
    {
        if ($file = $request->file('file')) {
            $path = 'users' . DIRECTORY_SEPARATOR . date('FY') . DIRECTORY_SEPARATOR;
            // generate safe file name in storage
            $avatarName = $this->generateFileName($file, $path);
            // make image correctly
            $image = InterventionImage::make($file)->orientate()->stream('jpg');
            // full path for file save
            $fullPath = $path.$avatarName.'.'.$file->getClientOriginalExtension();
            // save in storage in public for user
            Storage::disk(config('voyager.storage.disk'))->put($fullPath, $image, 'public');
            $user = auth('api')->user();
            $user->avatar = $fullPath;
            if ($user->save()) {
                $asset = Voyager::image($fullPath);
                return $asset;
            }
        }
    }

    private function generateFileName($file, $path): string
    {
        $filename = basename($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension());
        $filename_counter = 1;

        // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        while (Storage::disk(config('voyager.storage.disk'))->exists($path.$filename.'.'.$file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension()).(string) ($filename_counter++);
        }


        return $filename;
    }
}
