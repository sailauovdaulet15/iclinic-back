<?php

namespace App\Services;

use App\Enums\AboutUsEnum;
use App\Http\Resources\AboutUsAchievementResource;
use App\Http\Resources\AboutUsBranchResource;
use App\Http\Resources\AboutUsLicenseResource;
use App\Http\Resources\AboutUsSliderResource;
use App\Http\Resources\AboutUsWhyUsResource;
use App\Models\AboutUs;
use App\Models\AboutUsAchievement;
use App\Models\AboutUsBranch;
use App\Models\AboutUsLicense;
use App\Models\AboutUsSlider;
use App\Models\AboutUsWhyUs;

class AboutUsService
{
    public function getUrls()
    {
        $list = AboutUs::pluck('key', 'title');

        return $list->map(function ($val) {
            return route('v1.about-us.show', [$val]);
        });
    }

    public function takeAboutUsSeparately($type)
    {
        switch ($type) {
            case AboutUsEnum::ACHIEVEMENT:
                return AboutUsAchievementResource::collection(AboutUsAchievement::all());
            case AboutUsEnum::BRANCH:
                return AboutUsBranchResource::collection(AboutUsBranch::all());
            case AboutUsEnum::LICENSE:
                return AboutUsLicenseResource::collection(AboutUsLicense::all());
            case AboutUsEnum::SLIDER:
                return AboutUsSliderResource::collection(AboutUsSlider::all());
            case AboutUsEnum::WHY_US:
                return AboutUsWhyUsResource::collection(AboutUsWhyUs::all());
            default:
                return response('Not found', 404);
        }
    }
}
