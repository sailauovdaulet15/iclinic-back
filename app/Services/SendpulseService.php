<?php

namespace App\Services;

use App\Models\Mailing;
use App\Models\OrderingCall;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class SendpulseService
{
    private Carbon $tokenExpireTime;
    private string $token;

    /**
     * @return void
     */
    public function auth()
    {
        $response = Http::post(rtrim(config('sendpulse.api_url'), '/') . '/oauth/access_token', [
            'grant_type' => 'client_credentials',
            'client_id' => config('sendpulse.client_id'),
            'client_secret' => config('sendpulse.client_secret'),
        ]);
        $responseBody = json_decode($response->body());
        $this->token = $responseBody->access_token;
        $this->tokenExpireTime = Carbon::now()->addSeconds($responseBody->expires_in);
    }

    /**
     * @return bool
     */
    public function tokenExpired(): bool
    {
        if (!empty($this->token) && (!empty($this->tokenExpireTime))) {
            return Carbon::now() >= $this->tokenExpireTime;
        }
        return true;
    }

    /**
     * @param OrderingCall $orderingCall
     * @return bool
     */
    public function sendEmails(OrderingCall $orderingCall): bool
    {
        if ($this->tokenExpired()) {
            $this->auth();
        }
        if (Mailing::query()->exists()) {
            $mailingCollection = Mailing::query()->get();
            $emails = [];
            foreach ($mailingCollection as $mail) {
                $emails[] = [
                    'email' => $mail->email,
                    'name' => $mail->user()->find($mail->user_id)->first_name,
                ];
            }

            $doctorFullName = '';
            $specialisation = '';
            if (!empty($orderingCall->user_id)) {
                $user = $orderingCall->doctors()->find($orderingCall->user_id)->user()->find($orderingCall->user_id);
                $doctorFullName = "{$user->second_name} {$user->first_name} {$user->third_name}";
            }
            if (!empty($orderingCall->specialisation_id)) {
                $specialisation = $orderingCall->specialisation()->find($orderingCall->specialisation_id)->title;
            }

            $response = Http::withToken($this->token)->post(rtrim(config('sendpulse.smtp_api_url'), '/') . '/emails', [
                'email' => [
                    'subject' => config('sendpulse.email_subject'),
                    'template' =>
                        [
                            'id' => config('sendpulse.template_id'),
                            'variables' => [
                                'order_id' => $orderingCall->id,
                                'customer_full_name' => $orderingCall->user_name,
                                'doctor_full_name' => $doctorFullName,
                                'customer_phone' => preg_replace("/^1?(\d{3})(\d{3})(\d{2})(\d{2})$/", "+7 $1 $2-$3-$4", $orderingCall->user_phone),
                                'specialisation' => $specialisation,
                                'visit_date' => !empty($orderingCall->date) ? date('d.m.Y', strtotime($orderingCall->date)) : '',
                                'visit_time' => !empty($orderingCall->time) ? date('H:i', strtotime($orderingCall->time)) : '',
                                'customer_comment' => $orderingCall->comment,
                                'order_time' => !empty($orderingCall->created_at) ? date('d.m.Y, H:i:s', strtotime($orderingCall->created_at)) : '',
                            ]
                        ],
                    'from' => [
                        'name' => config('sendpulse.email_sender_name'),
                        'email' => config('sendpulse.email_sender'),
                    ],
                    'to' => $emails,
                ],
            ]);
            $response_body = json_decode($response->body());
            if (!empty($response_body->error_code)) {
                Log::alert('SendpulseService: mailing wasn\'t send. Response:', [$response->body()]);
            }
            if (!empty($response_body->result)) {
                return json_decode($response)->result;
            }
        }
        return false;
    }
}

