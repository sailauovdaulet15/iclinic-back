<?php

namespace App\Services;

use App\Models\Service;
use App\Models\Doctor;
use App\Models\Specialisation;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Spatie\QueryBuilder\QueryBuilder;

class ServiceService
{
    protected $query;

    public function __construct()
    {
        $this->query = QueryBuilder::for(Service::class)
            ->allowedFilters(['title'])
            ->allowedIncludes(['service_landing', 'recommend_services', 'specialisation']);
    }

    public function takeServicesTree()
    {
        return $this->query
            ->get()
            ->toTree();
    }

    public function takeOne($slug)
    {
        return $this->query
            ->whereSlug($slug)
            ->with('doctors', 'specialisation', 'recommendServices')
            ->firstOrFail();
    }

    public function takeServicesByDoctorSlug(string $slug)
    {
        $userId = User::whereSlug($slug)->first()->id;
        $doctor = Doctor::findOrFail($userId)->specialisations()->get()->pluck('id');
        return $this->query->whereIn('specialisation_id', $doctor)->get();
    }

    public function takeServicesBySpecialisation(int $id): Collection
    {
        $specialisationId = Specialisation::findOrFail($id)->pluck('id');
        return Service::whereIn('specialisation_id', $specialisationId)->get();
    }

    public function getCount(int $id)
    {
        return Service::whereHas('specialisation', function ($q) use ($id) {
            $q->where('specialisation_id', '=', $id);

        })->get()->count();

    }

    public function recommended(int $id): Collection
    {
        $service = Service::findOrFail($id);
        return Service::where('specialisation_id', '=', $service->specialisation_id)->where('id', '!=', $service->id)
            ->get();
    }
}
