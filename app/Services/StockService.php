<?php

namespace App\Services;

use App\Models\Stock;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StockService
{
    protected $query;

    protected $paginate = 5;

    public function __construct()
    {
        $this->query = QueryBuilder::for(Stock::class)->allowedFilters([
            AllowedFilter::scope('count')
        ])->allowedSorts([
            'start_time',
            'end_time',
            'position',
            'title'
        ])->with([
            'firstIcon', 'secondIcon'
        ]);
    }

    /**
     * Take all the Stock
     *
     * @query paginate
     * @return array|LengthAwarePaginator|Collection|QueryBuilder[]
     */
    public function takeAll()
    {
        if (request()->has('paginate')) {
            return $this->query->orderBy('position')->paginate(request('paginate') ?: $this->paginate)->appends(request()->query());
        }
        return $this->query->orderBy('position')->get();
    }

    public function takeOne($id)
    {
        return $this->query->where('id', $id)->first();
    }
}
