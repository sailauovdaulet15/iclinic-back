<?php

namespace App\Services\Appointment;

use App\Enums\AppointmentEnum;

use App\Enums\AppointmentStatusEnum;
use App\Exceptions\AppointmentCantBeSavedException;
use App\Http\Resources\AppointmentStoreResource;
use App\Http\Resources\ServiceResource;
use App\Jobs\CancelNotPayedAppointmentJob;
use App\Models\Appointment;
use App\Models\Doctor;
use App\Models\Service;
use App\Soap\Request\CreateAppointment;
use App\Soap\SoapService;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;
use function Clue\StreamFilter\fun;

class AppointmentService
{
    public $constants;

    public function __construct()
    {
        $this->constants = new AppointmentEnum();

    }

    public function storeArrayByStep(array $data)
    {
        if ($data['step'] == 1) {
            $data = array_merge($data, ['patient_id' => $this->constants->getAuthUser()]);
            Redis::hmset($this->constants->getKeyAppointment(), $data);
            $redisAppointment = Redis::hgetall($this->constants->getKeyAppointment());
            $doctor           = Doctor::findOrFail($redisAppointment['doctor_user_id']);

            return [
                'doctor_name'     => $doctor->user->full_name,
                'patient_name'    => auth('api')->user()->full_name,
                'doctor_category' => $doctor->category,
                'date'            => $redisAppointment['date'],
                'start_time'      => $redisAppointment['start_time'],
                'end_time'        => $redisAppointment['end_time'],
                'cabinet_num'     => $redisAppointment['cabinet_num'] ?? '',
            ];

        } elseif ($data['step'] == 2) {
            $data['services'] = json_encode($data['services']);
            Redis::hmset($this->constants->getKeyServices(), $data);
            $services = Service::whereIn('id', $this->getRedisServices())->get();

            return ServiceResource::collection($services);

        } elseif ($data['step'] == 3) {
            return $this->storeToSql($data);
        }
    }

    private function storeToSql(array $data)
    {
        $redisAppointment = Redis::hgetall($this->constants->getKeyAppointment());
        unset($redisAppointment['step']);
        $redisAppointment = array_merge($redisAppointment, [
            'status'            => AppointmentStatusEnum::BOOKED,
            'invoiceId'         => $data['invoiceId'],
            'is_online_payment' => $data['is_online_payment'],
        ]);

        $redisAppointmentServices = $this->getRedisServices();

        DB::beginTransaction();
        try {
            $appointment = Appointment::create($redisAppointment);
            $appointment->services()->attach($redisAppointmentServices);
            $appointment = $appointment->load('services', 'doctor.user');
            Redis::del($this->constants->getKeyAppointment());
            Redis::del($this->constants->getKeyServices());
            $appointment = $this->sendToSoap($appointment);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            return response()->json(['message' => 'Не удалось сохранить запись'], 200);
        }
        CancelNotPayedAppointmentJob::dispatch($appointment->id)->delay(now()->addMinutes(20));

        return new AppointmentStoreResource($appointment);
    }

    /**
     * @throws AppointmentCantBeSavedException
     */
    private function sendToSoap($appointment)
    {
        $uid              = app(SoapService::class)->createAppointment($appointment);
        $appointment->uid = $uid;
        if (!$appointment->save()) {
            throw new AppointmentCantBeSavedException();
        }

        return $appointment;
    }

    public function getResult(): array
    {
        $redisAppointment = Redis::hgetall($this->constants->getKeyAppointment());
        $services         = Service::whereIn('id', $this->getRedisServices())->get();
        $doctor           = Doctor::find($redisAppointment['doctor_user_id'] ?? null);

        return [
            'doctor'       => $doctor,
            'patient_name' => auth('api')->user()->full_name,
            'date'         => $redisAppointment['date'] ?? null,
            'start_time'   => $redisAppointment['start_time'] ?? null,
            'end_time'     => $redisAppointment['end_time'] ?? null,
            'cabinet_num'  => $redisAppointment['cabinet_num'] ?? '',
            'services'     => $services,
        ];
    }

    public function redisDelete()
    {
        Redis::del($this->constants->getKeyAppointment());
        Redis::del($this->constants->getKeyServices());

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function getRedisServices(): array
    {
        $redisAppointmentServices = Redis::hgetall($this->constants->getKeyServices());
        if (isset($redisAppointmentServices['services'])) {
            $array = json_decode($redisAppointmentServices['services']);

            return array_map('intval', $array);
        }

        return [];
    }

    public function getAppointmentByUser()
    {
        return Appointment::query()
            ->where('patient_id', '=', $this->constants->getAuthUser())
            ->where(function ($query) {
                $query->where('date', '>=', now())
                    ->where('start_time', '>=', now()->format('H:i'));
            })
            ->booked()
            ->orderBy('date')
            ->orderBy('start_time')
            ->get();
    }

    public function getArchive()
    {
        $doctor = Doctor::query()->where('user_id', '=', $this->constants->getAuthUser())->first();
        if (!empty($doctor)) {
            return Appointment::query()->whereHas('services', function ($q) {
                $q->where('is_online', '=', true);
            })
                ->where('doctor_user_id', '=', $this->constants->getAuthUser())
                ->where('date', '<', Carbon::now()->toDateString())
                ->orderBy('date', 'desc')
                ->orderBy('start_time', 'desc')
                ->paginate(5);
        }

        return Appointment::query()->whereHas('services', function ($q) {
            $q->where('is_online', '=', true);
        })
            ->where('patient_id', '=', $this->constants->getAuthUser())
            ->where('date', '<', Carbon::now()->toDateString())
            ->orderBy('date', 'desc')
            ->orderBy('start_time', 'desc')
            ->paginate(5);


    }

    public function getPlanned()
    {
        $doctor = Doctor::query()->where('user_id', '=', $this->constants->getAuthUser())->first();
        if (!empty($doctor)) {
            return Appointment::query()->whereHas('services', function ($q) {
                $q->where('is_online', '=', true);
            })
                ->where('doctor_user_id', '=', $this->constants->getAuthUser())
                ->where('date', '>=', Carbon::now()->toDateString())
                ->orderBy('date')
                ->orderBy('start_time')
                ->paginate(5);
        }

        return Appointment::query()->whereHas('services', function ($q) {
            $q->where('is_online', '=', true);
        })
            ->where('patient_id', '=', $this->constants->getAuthUser())
            ->where('date', '>=', Carbon::now()->toDateString())
            ->orderBy('date')
            ->orderBy('start_time')
            ->paginate(5);
    }

    public function delete(int $id)
    {
        $appointment = Appointment::find($id);
        $appointment->cancel(true);

        return \response(null, Response::HTTP_NO_CONTENT);
    }

}
