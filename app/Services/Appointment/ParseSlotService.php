<?php

namespace App\Services\Appointment;

use App\Enums\AppointmentStatusEnum;
use App\Enums\BookStatusEnum;
use App\Models\Appointment;
use App\Models\Visit;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Support\Collection;

class ParseSlotService
{
    private Visit $visit;

    public function __construct(Visit $visit)
    {
        $this->visit = $visit;
    }

    // TODO сделать exception если item не найдет или не парсится период
    public function takeVisitByDate($date = '*'): array
    {
        $visits = json_decode($this->visit->data)->rows;
        $items  = $date === '*' ? $visits : $this->findItemBy($visits, $date);
        $result = [];
        if ($items) {
            foreach ($items as $item) {
                $parseToPeriod = $this->parseToPeriod($item, $item->time_mode);
                $this->getSlotsParsedByBookedSlots($parseToPeriod, $item->date);
                $freeSlotsCount = array_reduce($parseToPeriod, function ($carry, $item) {
                    $carry += $item['status'];

                    return $carry;
                });
                $result[]       = [
                    'date'             => $item->date,
                    'free_slots_count' => $freeSlotsCount,
                    'interval_slots'   => $parseToPeriod,
                ];
            }
        }

        return $this->mergeSections($result);
    }

    /**
     * @param     $item
     * @param int $time_mode
     *
     * @return array
     */
    private function parseToPeriod($item, int $time_mode): array
    {
        $time    = CarbonInterval::minutes($this->visit->duration)
            ->toPeriod($item->date . " " . $item->start_time,
                Carbon::parse($item->date . " " . $item->end_time)
                    ->subMinutes($this->visit->duration));
        $periods = [];
        foreach ($time as $t) {
            // не показываем время которое уже прошло
            $status    = $t->gte(now()
                ->addHours(3))
                ?
                $time_mode
                :
                BookStatusEnum::BOOKED;
            $periods[] = [
                'time'         => $t->format('H:i'),
                'time_to'      => $t->addMinutes($this->visit->duration)->format('H:i'),
                'status'       => $status,
                'status_title' => BookStatusEnum::getTitle($status),
            ];
        }

        return $periods;
    }

    /**
     * @param $slots
     * @param $date
     */
    private function getSlotsParsedByBookedSlots(&$slots, $date)
    {
        $bookedSlots = $this->getBookedSlots($date);
        // TODO сделать оптимизацию чтобы работа шла по поиску из базы
        // можно сделать таблицу slots куда будем сохранять занятые слоты
        $slots = array_map(function ($item) use ($date, $bookedSlots) {
            if ($bookedSlots->contains($item['time'])) {
                $item['status']       = BookStatusEnum::BOOKED;
                $item['status_title'] = BookStatusEnum::getTitle(BookStatusEnum::BOOKED);

                return $item;
            }

            return $item;
        }, $slots);
    }

    /**
     * @param $date
     *
     * @return Collection
     */
    private function getBookedSlots($date): Collection
    {
        return Appointment::query()
            ->where('doctor_user_id', $this->visit->user_id)
            ->where('date', $date)
            ->booked()
            ->orderBy('date')
            ->pluck('start_time');
    }

    /**
     * Возвращаем слоты которые попали в этот месяц
     *
     * @param array  $items
     * @param string $equalVal example 2021-03
     * @param string $searchFormat
     *
     * @return array
     */
    private function findItemBy(array $items, string $equalVal, string $searchFormat = 'Y-m'): array
    {
        $result = [];
        foreach ($items as $item) {
            $date = Carbon::parse($item->date);
            // делаем сравнение по месяцам
            if ($date->gte(now()->format('Y-m-d')) && $date->format($searchFormat) == $equalVal) {
                $result[] = $item;
            }
        }

        return $result;
    }

    private function mergeSections($result): array
    {
        // берем даты, которые повторяются
        $array_count_values   = array_count_values(array_column($result, 'date'));
        $preparedToMergeDates = array_filter($array_count_values, function ($item) {
            return $item > 1;
        });


        $result = array_filter($result, function ($item) use (&$preparedToMergeDates) {
            if (isset($preparedToMergeDates[$item['date']])) {
                if (isset($preparedToMergeDates[$item['date']]['interval_slots'])) {
                    $item['interval_slots']   = array_merge($item['interval_slots'],
                        $preparedToMergeDates[$item['date']]['interval_slots']);
                    $item['free_slots_count'] =
                        count($item['interval_slots']);
                }
                $preparedToMergeDates[$item['date']] = $item;

                return false;
            }

            return true;
        });

        foreach ($preparedToMergeDates as &$item) {
            usort($item['interval_slots'], function ($a, $b) {
                if ($a['time'] == $b['time']) {
                    // если равны значит статусы разные, ставим исключительно тот статус который занят
                    return BookStatusEnum::BOOKED;
                }
                return $a['time'] > $b['time'];
            });

            // Удаляем время, которое повторяется. Сохраняем только те слоты у которых статус занят
            $tempArray     = [];
            $duplicateKeys = [];
            foreach ($item['interval_slots'] as $key => $slot) {
                if (!in_array($slot['time'], array_column($tempArray, 'time'))) {
                    $tempArray[] = $slot;
                } else {
                    $duplicateKeys[] = $key;
                }
            }
            foreach ($duplicateKeys as $key) {
                unset($item['interval_slots'][$key]);
            }
        }

        return array_merge($result, array_values($preparedToMergeDates));
    }
}
