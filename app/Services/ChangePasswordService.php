<?php

namespace App\Services;

use App\Models\SmsMessage;
use App\Models\User;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class ChangePasswordService
{
    public function passwordResetProcess($request): JsonResponse
    {
        $user = User::where('name', $request->name)->first();
        $userPhone = $user->phone;
        $password = $request->password;
        $token = $request->token;

        return $this->updatePasswordRow($userPhone, $token)->count() > 0 ?
            $this->resetPassword($user, $password, $token) : $this->tokenNotFoundError();
    }

    // Verify if token is valid
    private function updatePasswordRow($phone, $token): Builder
    {
        return DB::table('password_resets')->where([
            'phone' => $phone,
            'token' => $token
        ]);
    }

    // Token not found response
    private function tokenNotFoundError(): JsonResponse
    {
        return response()->json([
            'error' => 'Either your phone or token is wrong.'
        ], 422);
    }

    // Reset password
    private function resetPassword(User $user, $password, $token): JsonResponse
    {
        // update password
        $user->update([
            'password' => bcrypt($password)
        ]);
        // remove verification data from db
        $this->updatePasswordRow($user->phone, $token)->delete();
        // remove sms messages too
        SmsMessage::query()->where('phone', $user->phone)->delete();

        // reset password response
        return response()->json([
            'message' => 'Password has been updated.'
        ]);
    }
}
