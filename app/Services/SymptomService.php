<?php


namespace App\Services;


use App\Models\Symptoms;

class SymptomService
{
    public function getSymptom(int $id)
    {
        return Symptoms::where('specialisation_id', '=', $id)->get();
    }
}
