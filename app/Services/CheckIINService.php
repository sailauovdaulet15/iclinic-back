<?php

namespace App\Services;

use App\Soap\SoapService;

class CheckIINService
{
    public function checkIIN1c($iin)
    {
        return app(SoapService::class)->proverkaIIN($iin);
    }
}
