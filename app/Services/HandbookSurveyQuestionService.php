<?php


namespace App\Services;


use App\Models\HandbookSurveyQuestion;

class HandbookSurveyQuestionService
{
    public function getAll()
    {
        return HandbookSurveyQuestion::all();
    }
}
