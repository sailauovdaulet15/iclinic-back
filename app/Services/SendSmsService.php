<?php

namespace App\Services;

use App\Exceptions\WaitToSendSMSException;
use App\Models\SmsMessage;
use App\Soap\SoapService;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class SendSmsService
{
    // 0.5 minutes
    const DELAY_TIME = 30;
    // 10 minutes
    const MANY_ATTEMPT_DELAY = 600;
    const ATTEMPT_LIMIT = 4;

    /**
     * Execute send.
     *
     * @return void
     * @throws WaitToSendSMSException
     */
    public static function send($phone)
    {
        // todo validation with error
        // check if you can send message
        if (self::checkSendMessage($phone)) {
            // sms_code is available for the sms
            if ($sms_code = self::sendSMSMessage($phone)) {
                app(SoapService::class)->sendSMSMessage($phone, $sms_code);
            }
        }
    }

    private static function sendSMSMessage($phone): string
    {
        try {
            // random digits 01234
            $sms_code = sprintf('%05d', rand(0, 99999));
            SmsMessage::query()->create([
                'phone' => $phone,
                'sms_code' => $sms_code
            ]);
            return $sms_code;
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            // 1062 is status for duplicate entry
            if ($errorCode == 1062) {
                // recursion for sms message if not unique
                self::sendSMSMessage($phone);
            } else {
                Log::error($e);
            }
        }
        // if database error happened
        return 0;
    }

    /**
     * Проверка на отправку смс кода с ограничением с интервалом и попыток
     *
     * @param $phone
     * @return bool
     * @throws WaitToSendSMSException
     */
    private static function checkSendMessage($phone): bool
    {
        $messagesWithinDelay = SmsMessage::query()
            ->where('phone', $phone)
            ->where('created_at', '>', now()->subSeconds(self::MANY_ATTEMPT_DELAY))
            ->orderByDesc('created_at');
        if ($messagesWithinDelay->exists()) {
            $attemptCount = $messagesWithinDelay
                ->where('created_at', '>', now()->subSeconds(self::MANY_ATTEMPT_DELAY))
                ->count();
            $hasManyAttempts = $attemptCount < self::ATTEMPT_LIMIT;
            $secondsFromLastMessage = now()->diffInSeconds($messagesWithinDelay->first()->created_at);

            if (!$hasManyAttempts) {
                throw new WaitToSendSMSException('Wait for ' . (self::MANY_ATTEMPT_DELAY - $secondsFromLastMessage) . 's to send a message.');
            }
            else if ($secondsFromLastMessage < self::DELAY_TIME) {
                throw new WaitToSendSMSException('Wait for ' . (self::DELAY_TIME - $secondsFromLastMessage) . 's to send a message.');
            }
        }
        return true;
    }
}
