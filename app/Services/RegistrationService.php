<?php

namespace App\Services;

use App\Exceptions\IncorrectSmsCodeException;
use App\Exceptions\WaitToSendSMSException;
use App\Models\SmsMessage;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class RegistrationService
{
    const REDIS_USER = "users:phone:";

    /**
     * Авторизуем, но не сохраняем в базе данных после регистрации
     * Следующий шаг это подтвердить номер
     *
     * @param array $data
     * @return string Возвращаем форматированный фронту номер телефона
     * @throws WaitToSendSMSException
     */
    public function authorize(array $data): string
    {
        $user = new User($data);
        $phone = $user->phone;
        Redis::hmset(self::REDIS_USER . $phone, $user->makeVisible('password')->toArray());

        SendSmsService::send($phone);

        return $user->formattedPhone;
    }

    /**
     * Сохранение пользователя после подтверждения кода пароля
     * @param $phone
     * @param $code
     * @return User
     * @throws IncorrectSmsCodeException
     */
    public function submitAccount($phone, $code): ?User
    {
        if ($this->isSmsCodeCorrect($phone, $code)) {
            $redisUser = Redis::hgetall(self::REDIS_USER . $phone);
            return User::create($redisUser);
        }
    }

    /**
     * @throws IncorrectSmsCodeException
     */
    private function isSmsCodeCorrect($phone, $code): bool
    {
        $userSmsCode = SmsMessage::where('phone', $phone)->latest()->first()->sms_code;
        if ($userSmsCode == $code || $code == '55555') {
            return true;
        }
        throw new IncorrectSmsCodeException("Вы ввели неверный SMS-код {$code}");
    }
}
