<?php


namespace App\Services;


use App\Models\AnswerOnQuestion;

class AnswerOnQuestionService
{
    public function store(array $data)
    {
        return AnswerOnQuestion::create($data);
    }

    public function storeMany(array $data)
    {
        $questions = $data['questions'];
        AnswerOnQuestion::insert($questions);
        return true;
    }
}
