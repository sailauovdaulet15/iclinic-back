<?php

namespace App\Services;

use App\Enums\BookStatusEnum;
use App\Http\Resources\ServiceResource;
use App\Models\Doctor;
use App\Models\Service;
use App\Models\User;
use App\Services\Appointment\ParseSlotService;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use App\Models\DoctorService as ServiceDoctor;

class DoctorService
{
    protected $query;

    public function __construct()
    {
        $this->query = QueryBuilder::for(Doctor::class)
            ->allowedFilters([
                AllowedFilter::exact('specialisations.id')
            ])
            ->allowedIncludes(['services', 'specialisations', 'user']);
    }

    public function takeAll()
    {
        return $this->query->visibleOnSite()->get();
    }

    public function show($slug) {
        $doctor = $this->takeOne($slug);
        $services = Collection::make();
        $services = $services->merge(ServiceResource::collection($doctor->when($doctor->relationLoaded('services'), function () use ($doctor) {
            return $doctor->services->toTree();
        })));
        if ($doctor->relationLoaded('specialisations')) {
            foreach ($doctor->specialisations as $specialisation) {
                $services = $services->merge($specialisation->services->toTree());
            }
        }

        $doctor->services = $services;
        return $doctor;
    }

    public function takeOne($slug)
    {
        return $this->query
            ->whereHas('user', function ($query) use ($slug) {
                $query->whereSlug($slug);
            })
            ->with([
                'specialisations',
                'reviews',
                'services' => fn($query) => $query->orderBy('title'),
                'recommendServices' => fn($query) => $query->orderBy('title')
            ])
            ->firstOrFail();
    }

    public function takeDoctorsByServiceId(int $id)
    {
        $service = Service::findOrFail($id);

        return $this->query->whereHas('specialisations',
            function ($q) use ($service){
            $q->where('specialisation_id', $service->specialisation_id);
        })->visibleOnSite()->get();
    }

    public function takeDoctorsByServices()
    {
        $result = json_decode(request()->get('services'));
        $doctorServices = ServiceDoctor::whereIn('service_id', $result)->get()->pluck('doctor_user_id');
        return $this->query->whereIn('user_id', $doctorServices)->visibleOnSite()->get();
    }

    public function takeDoctorVisits(string $slug, array $filters): Collection
    {
        $month = $filters['month'] ?? now()->format('Y-m');
        // форматируем для whereLike в byDate
        $formattedMonth = $month . '-__';
        $visits = User::query()
            ->where('slug', $slug)
            ->firstOrFail()
            ->visits()
            ->byDate($formattedMonth)
            ->get(['data', 'user_id', 'duration']);
        $visitsParsed = [];

        foreach ($visits as $visit) {
            $service = new ParseSlotService($visit);
            $visitsParsed = array_merge($visitsParsed, $service->takeVisitByDate($month));
        }
        return collect(['data' => $visitsParsed]);
    }

    public function getCount(int $id)
    {
        return Doctor::visibleOnSite()->whereHas('specialisations', function ($q) use ($id) {
            $q->where('specialisation_id', '=', $id);

        })->get()->count();

    }


}
