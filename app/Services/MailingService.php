<?php

namespace App\Services;

use App\Models\Doctor;
use App\Models\Mailing;
use App\Models\Service;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Spatie\QueryBuilder\QueryBuilder;
use App\Models\DoctorService;

class MailingService
{
    public function updateMailing(array $input): Model
    {
        $input = array_merge($input, ['user_id' => auth('api')->user()->id]);
        return Mailing::query()->updateOrCreate(['user_id' => $input['user_id']], ['email' => $input['email']]);
    }
}
