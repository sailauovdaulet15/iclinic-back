<?php

namespace App\Services;

use App\Models\Specialisation;
use App\Repositories\Interfaces\SearchDoctorRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class SpecialisationService
{
    protected SearchDoctorRepositoryInterface $searchDoctorRepository;

    public function __construct(SearchDoctorRepositoryInterface $searchDoctorRepository)
    {
        $this->searchDoctorRepository = $searchDoctorRepository;
    }

    public function takeAll()
    {
        if ($search = request('search')) {
            if (isset($search['doctors'])) {
                return $this->takeBySearchDoctor($search['doctors']);
            }
            if (isset($search['services'])) {
                return $this->takeBySearchService($search['services'])->sortBy('title');
            }
        }

        if ($has = request('has')) {
            switch ($has) {
                case 'doctors':
                    return Specialisation::orderBy('title')->has('doctors')->get();
            }
        }

        return Specialisation::query()
            ->when(request('include'), function ($query, $search) {
                switch ($search) {
                    case 'services':
                        $query->with('services', fn($q) => $q->orderBy('title'));
                        break;
                    case 'doctors':
                        $query->with('doctors', fn($q) => $q->orderByName());
                        break;
                }
            })->orderBy('title')->get();
    }


    public function takeOne(string $slug)
    {
        return Specialisation::whereSlug($slug)->firstOrFail();
    }

    public function takeBySearchDoctor($search)
    {
        $doctorIds = $this->searchDoctorRepository->search($search)->pluck('id');
        return Specialisation::query()->whereHas('doctors', function($query) use ($doctorIds) {
            return $query->whereIn('user_id', $doctorIds);
        })->with('doctors', function($query) use ($doctorIds) {
            return $query->whereIn('user_id', $doctorIds);
        })->get();
    }

    public function takeBySearchService($search)
    {
        $searchLower = mb_strtolower($search);
//        $specialisations = Specialisation::query()
//            ->where(DB::raw('lower(title)'), 'like', "$searchLower%")
//            ->with('services')
//            ->has('services')
//            ->get();
        return (
            Specialisation::query()
                ->searchByServiceTitle($searchLower)
                ->get()
        );
    }

    /**
     * Возврат услуг по слагу специализации
     * @param $slug
     * @return Collection
     */
    public function takeServices(string $slug): Collection
    {
        return Specialisation::whereSlug($slug)
            ->with('doctors')
            ->firstOrFail()
            ->services()
            ->orderBy('title')
            ->get();

    }

    /** Возврат врачей по слагу специализации
     * @param $slug
     * @return Collection
     */
    public function takeDoctors($slug): Collection
    {
        return Specialisation::whereSlug($slug)
            ->with('doctors')
            ->firstOrFail()
            ->doctors()
            ->orderByName()
            ->get();
    }
}
