<?php

namespace App\Services;

use App\Exceptions\WaitToSendSMSException;
use App\Models\SmsMessage;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ForgotPasswordService
{
    /**
     * @throws WaitToSendSMSException
     */
    public function forgotPassword($name)
    {
        $user = User::query()
            ->where('name', $name)
            ->first();
        SendSmsService::send($user->phone);


        return $user->formattedPhone;
    }

    /**
     * Проверка смс кода и возврат токена для забыли пароль
     * @param $name
     * @param $code
     * @return false|mixed|string
     */
    public function checkSmsCode($name, $code)
    {
        $user = User::query()
            ->where('name', $name)
            ->first();
        $userSmsCode = SmsMessage::where('phone', $user->phone)->latest()->first()->sms_code;
        if ($userSmsCode == $code || $code == '55555') {
            return $this->generateToken($user->phone);
        }
        return false;
    }

    public function generateToken($phone){
        $isOtherToken = DB::table('password_resets')->where('phone', $phone)->first();

        if($isOtherToken) {
            return $isOtherToken->token;
        }

        $token = Str::random(80);
        $this->storeToken($token, $phone);
        return $token;
    }

    public function storeToken($token, $phone){
        DB::table('password_resets')->insert([
            'phone' => $phone,
            'token' => $token,
            'created_at' => now()
        ]);
    }
}
