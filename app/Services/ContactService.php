<?php

namespace App\Services;

use App\Models\Contact;
use Spatie\QueryBuilder\QueryBuilder;

class ContactService
{
    protected QueryBuilder $query;
    public function __construct()
    {
        $this->query = QueryBuilder::for(Contact::class)->allowedIncludes(['city']);
    }

    public function takeContacts()
    {
        return $this->query->get();
    }
}
