<?php

namespace App\Services;

use App\Models\HistoryVisit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Client\Request;
use Illuminate\Http\Response;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class HistoryVisitService
{
    protected $query;

    public function __construct()
    {
        $this->query = QueryBuilder::for(HistoryVisit::class)
            ->with('services')
            ->active()
            ->allowedFilters([
                AllowedFilter::scope('date_start'),
                AllowedFilter::scope('date_end'),
                AllowedFilter::scope('doctor', 'whereHasDoctorName'),
                AllowedFilter::scope('service_id'),
                AllowedFilter::scope('doctor_id'),
                AllowedFilter::scope('specialisation_id', 'whereHasSpecialisation'),
            ])
            ->allowedSorts([
                AllowedSort::field('reg_date', 'date'),
                AllowedSort::field('total', 'total_sum'),
                AllowedSort::field('total_bonuses', 'sum_bonus'),
            ])
            ->defaultSort('date');

        $this->query->when(request('sort'), function ($query, $field) {
            $order = $field[0] === '-' ? 'desc' : 'asc';
            if ($order === 'desc') {
                $field = substr($field, 1);
            }
            switch($field) {
                case 'service':
                    $query->sortByServiceTitle($order);
                    break;
                case 'doctor':
                    $query->sortByDoctorName($order);
                    break;
            }
        });
    }

    public function takeAll()
    {
        return $this->query->get();
    }

    public function takeByUserId($user_id)
    {
        return $this->query->where('user_id', $user_id)->paginate();
    }
}
