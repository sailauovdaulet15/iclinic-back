<?php

namespace App\Services;

use App\Models\OrderingCall;

class OrderingCallService
{
    public function store(array $input)
    {
        return OrderingCall::query()->create($input);
    }
}
