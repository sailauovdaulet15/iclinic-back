<?php

namespace App\Services;

use App\Models\News;

class NewsService
{

    public function takeAll()
    {
        return News::query()
            ->orderBy('created_at')
            ->orderBy('updated_at')
            ->paginate(12, [
                'id', 'title', 'created_at',
                'updated_at', 'annotated', 'image_uri',
                'slug', 'news_category_id', 'published_at'
            ]);
    }

    public function takeOne($slug)
    {
        return News::query()
            ->whereTranslation('slug', $slug)
            ->with([
                'recommendNews' => fn($query) => $query->exceptCurrentNews($slug)->limit(5)
            ])
            ->firstOrFail();
    }
}
