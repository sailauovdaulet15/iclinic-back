<?php

namespace App\Http\Resources;

use App\Models\Service;
use App\Soap\Types\ServiceAppointment;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;


/**
 * @mixin ServiceAppointment
 */
class SoapAppointmentServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $service = $this->when((bool) $this->getService(), function() {
            return $this->getService()->id;
        }, function() {
            return Service::all()->random();
        });
        return [
            'id' => $service->id,
            'title' => $service->title,
            'amount' => $this->getAmount(),
            'sum' => $this->getSum()
        ];
    }
}
