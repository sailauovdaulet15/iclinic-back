<?php

namespace App\Http\Resources;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Contact
 */
class ContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'address' => $this->getTranslatedAttribute('address'),
            'map' => $this->getCoordinates(),
            'position' => $this->position,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'phones' => $this->formattedPhones,
            'schedule' => optional(json_decode($this->schedule))->rows,
            'city' => new CityResource($this->whenLoaded('city')),
        ];
    }
}
