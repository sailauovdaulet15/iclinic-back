<?php

namespace App\Http\Resources;

use App\Models\ServiceLanding;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;
use Voyager;

/**
 * Class ServiceLanding
 *
 * @mixin ServiceLanding
 * */
class ServiceLandingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->getTranslatedAttribute('title'),
            'annotation' => $this->getTranslatedAttribute('annotation'),
            'image' => Voyager::image($this->image_uri),
            'slug' => $this->getTranslatedAttribute('slug'),
            'specialisation_id' => $this->specialisation_id,
            'specialisation_count_details' => optional(json_decode($this->specialisation_count_details))->rows,
            'specialisation' => new SpecialisationResource($this->specialisation),
            'symptoms' => $this->when(
                $this->whenLoaded('specialisation') &&
                $this->specialisation->relationLoaded('symptoms'),
                function() {
                    return SymptomResource::collection($this->specialisation->symptoms);
                }
            ),
            'reviews' => ReviewResource::collection($this->whenLoaded('reviews')),
            'recommendServices' => ServiceResource::collection($this->whenLoaded('recommendServices')),
            'exclusive_services' => ServiceResource::collection($this->specialisation->exclusiveServices),

        ];
    }
}
