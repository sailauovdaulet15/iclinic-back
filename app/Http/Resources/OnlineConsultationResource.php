<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OnlineConsultationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource($this->user->load('role')),
            'doctor' => new DoctorListResource($this->doctor),
            'date' => $this->date,
            'date_start' => $this->date_start,
            'has_medicine_card' => $this->hasMedicineCard,
            'medicine_card_id' => optional($this->medicineCard)->id
        ];
    }
}
