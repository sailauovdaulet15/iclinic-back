<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin User
 */
class ProfileResource extends JsonResource
{
    private const UTF_8 = 'UTF-8';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'first_name' => mb_convert_case($this->first_name, MB_CASE_TITLE, self::UTF_8),
            'second_name' => mb_convert_case($this->second_name, MB_CASE_TITLE, self::UTF_8),
            'third_name' => mb_convert_case($this->third_name, MB_CASE_TITLE, self::UTF_8),
            'nationality_id' => $this->nationality_id,
            'sex' => $this->sex,
            'language' => $this->language,
            'is_limited_by_mobility' => $this->is_limited_by_mobility,
            'is_notification' => $this->is_notification,
            'is_push_notification' => $this->is_push_notification,
            'postcode' => $this->postcode,
            'date_birth' => $this->date_birth,
            'phone2' => $this->formattedPhone2,
            'address' => $this->address,
            'region' => $this->region,
            'city' => $this->city,
            'legal_agent_id' => $this->legal_agent_id,
            'handbook_blood_group_id' => $this->handbook_blood_group_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'country' => $this->country,
            'legal_agent' => new UserResource($this->whenLoaded('legalAgent', function () {
                return $this->legalAgent->load('role');
            })),
            'handbook_blood_group' => new HandbookBloodGroupResource($this->whenLoaded('handbookBloodGroup')),
            'nationality' => $this->whenLoaded('nationality'),
        ];
    }
}
