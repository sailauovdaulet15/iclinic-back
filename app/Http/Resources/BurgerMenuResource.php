<?php


namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class BurgerMenuResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->getTranslatedAttribute('title'),
            'href' => $this->href,
        ];
    }
}
