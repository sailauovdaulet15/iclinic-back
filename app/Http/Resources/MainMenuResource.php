<?php


namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class MainMenuResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->getTranslatedAttribute('title'),
            'href' => $this->href,
        ];
    }
}
