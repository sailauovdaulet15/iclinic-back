<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ServicePointResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'key' => $this->key,
            'title' => isset($this->specialisation->title) ? $this->specialisation->getTranslatedAttribute('title'):'',
            'category' => $this->category->title,
            'description' => $this->description,
            'slug' => optional(optional($this->specialisation)->serviceLanding)->slug
        ];
    }
}
