<?php

namespace App\Http\Resources;

use App\Models\Doctor;
use App\Soap\Types\AppointmentHistory;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin AppointmentHistory
 */
class AppointmentHistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'date' => $this->getDate()->format('Y-m-d H:i'),
            'doctor' => new DoctorListResource($this->when(optional($this->getDoctorUser())->doctor, function() {
                return $this->getDoctorUser()->doctor;
            }, function () {
                return Doctor::all()->random();
            })),
            'payment_type' => $this->getPaymentType(),
            'services' => SoapAppointmentServiceResource::collection($this->getServices()),
            'total_sum' => $this->getTotalSum()
        ];
    }
}
