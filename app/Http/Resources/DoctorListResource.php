<?php

namespace App\Http\Resources;

use App\Models\Doctor;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

/**
 * Class Doctor
 *
 * @mixin Doctor
 */
class DoctorListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'user_id' => $this->user_id,
            'slug' => $this->user->slug,
            'full_name' => $this->user->full_name,
            'title' => $this->user->full_name, // для дропдауна в фронтенде там компонент требует такое поле
            'category' => $this->category,
            'experience' => $this->experience,
            'heals' => optional($this->handbookHealCategory)->title ?? '',
            'image' => $this->image,
            'specialisations_labels' => $this->specialisations->implode('title', ', '),
            'specialisations' => SpecialisationResource::collection($this->specialisations),
        ];
    }
}
