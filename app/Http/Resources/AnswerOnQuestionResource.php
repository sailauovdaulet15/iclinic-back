<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AnswerOnQuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'history_visit' => $this->historyVisit->uid,
            'handbook_survey_question' => $this->handbookSurveyQuestion->title,
            'patient' => $this->patient->full_name,
            'doctor' => $this->doctor->user->full_name,
            'answer' => $this->answer,
        ];
    }
}
