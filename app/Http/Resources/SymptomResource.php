<?php

namespace App\Http\Resources;

use App\Models\Symptoms;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

/**
 * Class Symptoms
 *
 * @mixin Symptoms
 */
class SymptomResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->getTranslatedAttribute('title'),
            'specialisation' => $this->specialisation->title ?? '',
            'description' => $this->getTranslatedAttribute('description'),
            'position' => $this->position
        ];
    }
}
