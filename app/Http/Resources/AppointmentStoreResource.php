<?php

namespace App\Http\Resources;

use App\Models\Appointment;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Appointment
 */
class AppointmentStoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'doctor_name' => $this->doctor->user->full_name,
            'patient_name' => $this->user->full_name,
            'doctor_category' => $this->doctor->category,
            'date_of_receipt' => $this->start_time,
            'date' => $this->date,
            'end_time' => $this->end_time,
            'services' => ServiceResource::collection($this->services)
        ];
    }
}
