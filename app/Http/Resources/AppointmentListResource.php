<?php

namespace App\Http\Resources;

use App\Models\Appointment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Appointment
 */
class AppointmentListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'doctor' => new DoctorListResource($this->doctor),
            'patient' => new UserResource($this->user->load('role')),
            'medicine_card_id' => optional(optional($this->medicineCard)->files)[0]->id ?? '',
            'is_online' => $this->is_online_payment,
            'cabinet_num' => $this->cabinet_num,
            'status' => $this->status,
            'date' => Carbon::parse($this->date)->format('d-m-Y'),
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
        ];
    }
}
