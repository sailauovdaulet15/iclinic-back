<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use TCG\Voyager\Models\Role;

/**
 * @mixin User
 */
class UserResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->formattedPhone,
            'email' => $this->email,
            'avatar' => $this->avatarImage,
            'role' => optional($this->whenLoaded('role'))->name,
            $this->mergeResource($request, ProfileResource::class),
            'doctor' => new DoctorResource($this->whenLoaded('doctor')),

        ];
    }
}
