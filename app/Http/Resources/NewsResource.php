<?php


namespace App\Http\Resources;


use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin News
 */
class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->getTranslatedAttribute('title'),
            'slug' => $this->getTranslatedAttribute('slug'),
            'image' => $this->image,
            'annotated' => $this->getTranslatedAttribute('annotated'),
            'news_category' => $this->news_category_id,
            'description' => $this->when($this->description !== null, $this->getTranslatedAttribute('description')),
            'published_at' => date('Y-m-d', strtotime($this->published_at)),
            'recommend_news' => NewsResource::collection($this->whenLoaded('recommendNews')),

        ];
    }
}
