<?php

namespace App\Http\Resources;

use App\Models\HistoryVisit;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

/**
 * @mixin HistoryVisit
 */
class HistoryVisitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reg_date' => Carbon::parse($this->date_start)->format('d.m.Y H:i'),
            'doctor' => new DoctorListResource($this->doctor),
            'services' => ServiceResource::collection($this->services),
            'total' => $this->calculatedTotal,
            'total_bonuses' => $this->sum_bonus,
            // todo сделать функционал бонус константы после опроса
            'bonus_after_survey' => '',
            'has_survey' => true,
        ];
    }
}
