<?php

namespace App\Http\Resources;

use App\Models\Specialisation;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

/**
 * Class Specialisation
 *
 * @mixin Specialisation
 **/
class SpecialisationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->getTranslatedAttribute('title'),
            'annotation' => $this->serviceLanding->annotation ?? '',
            'slug' => $this->getTranslatedAttribute('slug'),
            'path' => '/'.$this->getTranslatedAttribute('slug'),
            'services' => ServiceResource::collection($this->whenLoaded('services', function () {
                return $this->services->toTree();
            })),
            'doctors' => DoctorListResource::collection($this->whenLoaded('doctors')),
        ];
    }
}
