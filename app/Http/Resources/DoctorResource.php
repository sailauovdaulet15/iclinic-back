<?php

namespace App\Http\Resources;

use App\Models\Doctor;
use App\Models\Specialisation;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Kalnoy\Nestedset\Collection;

/**
 * Class Doctor
 *
 * @mixin Doctor
 */
class DoctorResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            $this->mergeResource($request, DoctorListResource::class),
            'about' => $this->getTranslatedAttribute('about'),
            'about2' => $this->getTranslatedAttribute('about2'),
            'link' => $this->embeddedLinks,
            'work_experience' => optional(json_decode($this->work_experience))->rows,
            'education_experience' => optional(json_decode($this->education_experience))->rows,
            'agent_percent' => $this->agent_percent,
            'duration' => $this->duration,
            'services' => $this->services->sortBy('title')->values(),
            'recommend_services' => ServiceResource::collection($this->whenLoaded('recommendServices', function () {
                return $this->recommendServices->toTree();
            })),
            'services_count' => $this->when(isset($this->services_count), $this->services_count),
            'user' => new UserResource($this->whenLoaded('user', function () {
                return $this->user->load('role');
            })),
            'reviews' => ReviewResource::collection($this->whenLoaded('reviews')),
        ];
    }
}
