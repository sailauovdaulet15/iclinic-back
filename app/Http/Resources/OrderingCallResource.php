<?php

namespace App\Http\Resources;


class OrderingCallResource extends BaseResource
{
    public function toArray($request): array
    {
        return [
            'user_name' => $this->user_name,
            'user_phone' => "+7" . $this->user_phone,
            'doctor' => new DoctorListResource($this->doctors),
            'specialisation' => new SpecialisationResource($this->specialisation),
            'date' => $this->date,
            'time' => $this->time,
            'comment' => $this->comment
        ];
    }
}
