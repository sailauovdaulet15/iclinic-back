<?php

namespace App\Http\Resources;

use App\Models\Service;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Service
 *
 * @mixin Service
 */

class ServiceResource extends BaseResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->getTranslatedAttribute('title'),
            'slug' => $this->getTranslatedAttribute('slug'),
            'is_online' => $this->is_online,
            'annotation' => $this->getTranslatedAttribute('annotation'),
            'description' => $this->getTranslatedAttribute('description'),
            'price_original' => $this->price_original,
            'price_percent' => $this->price_percent,
            'price_with_discount' => $this->price_with_discount,
            'price_bonus' => $this->price_bonus,
//            'stocks' => StockResource::collection($this->stocks),
            'doctors' => DoctorListResource::collection($this->whenLoaded('doctors')),
//            'is_visible' => $this->is_visible,
            'parent_id' => $this->parent_id,
            'link' => optional(json_decode($this->link))->rows,
            'service_landing' => $this->whenLoaded('serviceLanding'),
            'specialisation' => $this->whenLoaded('specialisation'),
            'recommend_services' => ServiceResource::collection($this->whenLoaded('recommendServices')),
            'children' => ServiceResource::collection($this->whenLoaded('children')),
        ];
    }
}
