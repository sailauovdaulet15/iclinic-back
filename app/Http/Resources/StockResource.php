<?php

namespace App\Http\Resources;

use App\Models\Stock;
use Illuminate\Http\Resources\Json\JsonResource;
use Voyager;

/**
 * Class Stock
 *
 * @mixin Stock
 */
class StockResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image' => Voyager::image($this->image_uri),
            'card_color' => $this->cardRgbColor,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'first_icon' => Voyager::image($this->firstIcon->image_uri),
            'second_icon' => Voyager::image($this->secondIcon->image_uri),
        ];
    }
}
