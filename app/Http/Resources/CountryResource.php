<?php

namespace App\Http\Resources;

use App\Models\Country;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

/**
 * @mixin Country
 */
class CountryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => mb_convert_case($this->title, MB_CASE_TITLE, "UTF-8"),
            'full_title' => mb_convert_case($this->full_title, MB_CASE_TITLE, "UTF-8")
        ];
    }
}
