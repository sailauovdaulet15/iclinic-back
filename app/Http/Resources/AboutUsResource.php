<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AboutUsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'key' => $this->key,
            'title' => $this->title,
            'image' => $this->image,
            'position' => $this->position,
            'is_active' => $this->is_active,
            'list' => $this->list
        ];
    }
}
