<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'doctor' => $this['doctor'] ? new DoctorListResource($this['doctor']) : null,
            'patient_name' => $this['patient_name'] ?? null,
            'date' => $this['date'] ?? null,
            'start_time' => $this['start_time'] ?? null,
            'end_time' => $this['end_time'] ?? null,
            'cabinet_num' => $this['cabinet_num'] ?? null,
            'is_online' => $this['is_online_payment'] ?? null,
            'services' => isset($this['services']) ? ServiceResource::collection($this['services']) : []
        ];
    }
}
