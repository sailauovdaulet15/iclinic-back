<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MainResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => $this->type,
            'title' => $this->title,
            'next_page_title' => $this->next_page_title,
            'description' => $this->description,
            'image_uri' => $this->image_uri,
            'list' => $this->list,
        ];
    }
}
