<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\URL;

class AboutUsLicenseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'about_us_id' => $this->about_us_id,
            'position' => $this->position,
            'image' => $this->image_uri ? url('/')."/storage/".$this->image_uri : '',
            'file' => $this->getFile(),
            'description' => $this->getTranslatedAttribute('description')
        ];


    }
}
