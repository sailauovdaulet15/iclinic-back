<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MailingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'user' => new UserResource($this->whenLoaded('user', function () {
                return $this->user->load('role');
            })),
            'mailing_email' => $this->email,
        ];
    }
}
