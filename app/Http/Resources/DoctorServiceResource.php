<?php

namespace App\Http\Resources;

use App\Models\DoctorService;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class DoctorService
 *
 * @mixin DoctorService
 */
class DoctorServiceResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'doctor_id' => $this->doctor_user_id,
            'service_id' => $this->service_id,
        ];
    }
}
