<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAnswerOnQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'history_visit_id' => 'required|integer|exists:history_visits,id',
            'handbook_survey_question_id' => 'required|integer|exists:handbook_survey_questions,id',
            'patient_id' => 'required|integer|exists:users,id',
            'doctor_user_id' => 'required|integer|exists:doctors,user_id',
            'answer' => 'required|integer|between:1,10',
        ];
    }
}
