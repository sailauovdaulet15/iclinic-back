<?php

namespace App\Http\Requests\OrderingCall;

use Illuminate\Foundation\Http\FormRequest;

class OrderingCallStoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'user_name' => ['required', 'string'],
            'user_phone' => ['required', 'string', 'digits_between:10,10', 'numeric'],
            'user_id' => ['nullable', 'integer', 'exists:doctors,user_id'],
            'specialisation_id' => ['nullable', 'integer', 'exists:specialisations,id'],
            'date' => ['nullable', 'date_format:Y-m-d'],
            'time' => ['nullable', 'date_format:H:i'],
            'comment' => ['nullable', 'string']
        ];
    }
}
