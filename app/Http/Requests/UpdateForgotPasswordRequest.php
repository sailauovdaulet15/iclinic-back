<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class UpdateForgotPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'token' => 'required',
            'name' => 'required|exists:users,name',
            'password' => [
                'required',
                'string',
                'confirmed',
                'different:old_password',
                Password::min(8)
                    ->mixedCase()
                    ->numbers()
            ],
        ];
    }
}
