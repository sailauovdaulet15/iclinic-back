<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MakeAppointmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'doctor_id' => 'required|exists:doctors,user_id',
            'services' => 'required|array',
            'services.*' => 'required|integer|distinct|exists:services,id',
            'date' => 'required|date',
            'start_time' => 'required|time',
            'end_time' => 'required|time',
            'cabinet_num' => 'nullable|integer'
        ];
    }
}
