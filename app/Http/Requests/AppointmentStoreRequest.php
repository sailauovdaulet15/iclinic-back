<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AppointmentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->step == 1) {
            return [
                'doctor_user_id' => 'required|exists:doctors,user_id',
                'step' => 'required|integer',
                'date' => 'required|date',
                'start_time' => 'required|date_format:H:i:s',
                'end_time' => 'required|date_format:H:i:s',
                'cabinet_num' => 'nullable|integer'
            ];
        } elseif ($this->step == 2) {
            return [
                'services' => 'required|array',
                'services.*' => 'required|integer|distinct|exists:services,id',
                'step' => 'required|integer'
            ];
        }
        elseif ($this->step == 3) {
            return [
                'step' => 'required|integer',
                'invoiceId' => 'nullable|integer',
                'is_online_payment' => 'required|boolean',
            ];
        }
        return [];

    }
}
