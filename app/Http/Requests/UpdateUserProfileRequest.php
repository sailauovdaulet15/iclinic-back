<?php

namespace App\Http\Requests;

use App\Enums\NationalityEnum;
use App\Enums\SexEnum;
use App\Models\HandbookBloodGroup;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class UpdateUserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'nullable',
            'second_name' => 'nullable',
            'third_name' => 'nullable',
            'language', [
                Rule::in(NationalityEnum::getConstants()),
            ],
            'is_limited_by_mobility' => 'nullable|boolean',
            'is_notification' => 'nullable|boolean',
            'is_push_notification' => 'nullable|boolean',
            'handbook_blood_group_id' => 'nullable|exists:'.HandbookBloodGroup::class.',id',
            'email' => 'nullable|email',
            'phone' => 'nullable|digits_between:10,10|numeric'
        ];
    }
}
