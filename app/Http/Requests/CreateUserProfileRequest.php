<?php

namespace App\Http\Requests;

use App\Enums\NationalityEnum;
use App\Enums\SexEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class CreateUserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->user() == null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'unique:users',
                'required_if:nationality_id,!=,1'
            ],
            'password' => [
                'required',
                'string',
                'confirmed',
                Password::min(8)
                    ->mixedCase()
                    ->numbers()
            ],
            'phone' => 'required|digits_between:10,10|numeric',
            'first_name' => 'required',
            'second_name' => 'required',
            'third_name' => 'nullable',
            'nationality_id' => 'required|exists:countries,id',
            'date_birth' => 'required',
            'sex' => [
                Rule::in(SexEnum::getConstants()),
            ],
            'language', [
                Rule::in(NationalityEnum::getConstants())
            ],
            'is_limited_by_mobility' => 'nullable|boolean'
        ];
    }

    public function messages()
    {
        return array_merge(parent::messages(), [
            'name.required_if:nationality_id,!=,1' => 'Поле ИИН должно быть заполнено для пользователей Казахстана'
        ]);
    }
}
