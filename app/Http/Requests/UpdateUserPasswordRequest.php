<?php

namespace App\Http\Requests;

use App\Rules\OldPasswordRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class UpdateUserPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required|password:api',
            'password' => [
                'required',
                'string',
                'confirmed',
                'different:old_password',
                Password::min(8)
                    ->mixedCase()
                    ->numbers()
            ],
        ];
    }
}
