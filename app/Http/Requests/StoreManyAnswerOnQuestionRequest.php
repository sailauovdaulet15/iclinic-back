<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreManyAnswerOnQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'questions.*.history_visit_id' => 'required|integer|exists:history_visits,id',
            'questions.*.handbook_survey_question_id' => 'required|integer|exists:handbook_survey_questions,id',
            'questions.*.patient_id' => 'required|integer|exists:users,id',
            'questions.*.doctor_user_id' => 'required|integer|exists:doctors,user_id',
            'questions.*.answer' => 'required|integer|between:1,10',
        ];
    }
}
