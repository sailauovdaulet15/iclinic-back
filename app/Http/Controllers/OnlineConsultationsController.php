<?php

namespace App\Http\Controllers;

use App\Enums\RoleEnum;
use App\Http\Resources\OnlineConsultationResource;
use App\Models\User;
use Illuminate\Http\Request;

class OnlineConsultationsController extends Controller
{
    public function index(Request $request)
    {
        /** @var User $user */
        $user = auth('api')->user();

        $consultations = $user
            ->historyVisits()
            ->when($request->get('archived'), function ($query) {
                return $query->archived();
            }, function ($query) {
                return $query->activeVisits();
            })
            ->consultations()
            ->orderBy('date', 'asc')
            ->orderBy('date_start', 'asc')
            ->get();

        return OnlineConsultationResource::collection($consultations);
    }

    public function indexDoctor(Request $request)
    {
        /** @var User $user */
        $user = auth('api')->user();

        $consultations = $user
            ->doctor
            ->historyVisits()
            ->when($request->get('archived'), function ($query) {
                return $query->archived();
            }, function ($query) {
                return $query->activeVisits();
            })
            ->consultations()
            ->orderBy('date', 'asc')
            ->orderBy('date_start', 'asc')
            ->get();

        return OnlineConsultationResource::collection($consultations);
    }
}
