<?php

namespace App\Http\Controllers\Voyager;

use App\Http\Controllers\VoyagerExtension\VoyagerExtensionBaseController;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class VoyagerDoctorController extends VoyagerExtensionBaseController
{
    protected $customOrderByFields = ['firstSecondName', 'slug', 'username'];

    public function customSearches($query, $model, $field, $search)
    {
        switch ($field) {
            case 'doctors.firstSecondName':
                $attributes = ['first_name', 'second_name'];
                return $query->whereIn(
                    'user_id',
                    User::query()
                        ->doctors()
                        ->where(function ($query) use ($attributes, $search) {
                            foreach ($attributes as $attribute) {
                                $query->orWhere(DB::raw("{$attribute}"), 'LIKE', "{$search}");
                            }
                        })->pluck('id')
                );
            case 'doctors.userName':
                return $query->whereHas('user', function ($query) use ($search) {
                    $query->where('name', 'like', "$search");
                });
        }
    }


    protected function customOrderBy($query, $model, $field, $sortOrder)
    {
        switch ($field) {
            case 'firstSecondName':
                return $query->join('users', 'users.id', '=', 'doctors.user_id')
                    ->orderBy(DB::raw("CONCAT(users.second_name, ' ', users.first_name)"), $sortOrder);
            case 'slug':
                return $query->join('users', 'users.id', '=', 'doctors.user_id')
                    ->orderBy(DB::raw("users.slug"), $sortOrder);
            case 'userName':
                return $query->join('users', 'users.id', '=', 'doctors.user_id')
                    ->orderBy(DB::raw("users.name"), $sortOrder);
        }
    }
}
