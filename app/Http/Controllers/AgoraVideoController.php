<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\HistoryVisit;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Events\MakeAgoraCall;

//use App\Models\Session;
use Illuminate\Support\Carbon;
use Willywes\AgoraSDK\RtcTokenBuilder;
use Willywes\AgoraSDK\RtmTokenBuilder;

class AgoraVideoController extends Controller
{

    public function token(Request $request)
    {
        $user = auth('api')->user();

        $consultation = Appointment::findOrFail($request->consultation_id);


        $appID               = config('agora.app_id');
        $appCertificate      = config('agora.app_certificate');
        $channelName         = 'channel_' . $consultation->id;
        $uid                 = $user->id;
        $uidStr              = ($user->id) . '';
        $role                = RtcTokenBuilder::RoleAttendee;
        $expireTimeInSeconds = 5400;
        $currentTimestamp    = Carbon::parse("$consultation->date $consultation->start_time")
            ->getTimestamp();
        $currentTimestamp    = now()->getTimestamp();
        $privilegeExpiredTs  = $currentTimestamp + $expireTimeInSeconds;

        $token = RtcTokenBuilder::buildTokenWithUid(
            $appID, $appCertificate, $channelName,
            $uid, $role, $privilegeExpiredTs
        );


        $values = [
            'appId'        => $appID,
            'token'        => $token,
            'userId'       => $uid,
            'remoteUserId' => $uid == $consultation->doctor_user_id ? $consultation->patient_id
                : $consultation->doctor_user_id,
            'channel_name' => $channelName,
        ];

        return $values;
    }


    public function tokenChat(Request $request)
    {
        $user = auth('api')->user();

        $consultation = Appointment::findOrFail($request->consultation_id);


        $appID               = config('agora.app_id');
        $appCertificate      = config('agora.app_certificate');
        $channelName         = 'channel_' . $consultation->id;
        $uid                 = $user->id;
        $uidStr              = ($user->id) . '';
        $role                = RtcTokenBuilder::RoleAttendee;
        $expireTimeInSeconds = 5400;
        $currentTimestamp    = Carbon::parse("$consultation->date $consultation->start_time")
            ->getTimestamp();
        $currentTimestamp    = now()->getTimestamp();
        $privilegeExpiredTs  = $currentTimestamp + $expireTimeInSeconds;
        $token               =
            RtmTokenBuilder::buildToken($appID, $appCertificate, $uidStr, $role, $privilegeExpiredTs);

        $values = [
            'appId'        => $appID,
            'token'        => $token,
            'userId'       => $uid,
            'remoteUserId' => $uid == $consultation->doctor_user_id ? $consultation->patient_id
                : $consultation->doctor_user_id,
            'channel_name' => $channelName,
        ];

        return $values;
    }

    public function callUser(Request $request)
    {
        $user = User::find(auth('api')->user()->id);

        $data['userToCall']  = $request->user_to_call;
        $data['channelName'] = $request->channel_name;
        $data['from']        = $user->id;

        broadcast(new MakeAgoraCall($data))->toOthers();
    }
}
