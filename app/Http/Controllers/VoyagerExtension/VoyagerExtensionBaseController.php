<?php

namespace App\Http\Controllers\VoyagerExtension;

use App\Models\Doctor;
use App\Models\User;
use App\Voyager\ContentTypes\CoordinateType;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use MonstreX\VoyagerExtension\Controllers\VoyagerExtensionBaseController as BaseController;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;

class VoyagerExtensionBaseController extends BaseController
{
    private $notEditableFields = ['polymorphic', 'slots'];
    protected $customOrderByFields = [];

    public function insertUpdateData($request, $slug, $rows, $data)
    {
        $filtered = $rows->reject(function ($value, $key) {
            return in_array($value->type, $this->notEditableFields);
        });

        $result             = parent::insertUpdateData($request, $slug, $filtered, $data);
        $multi_select_morph = [];

        foreach ($rows as $row) {
            if ($row->type == 'polymorphic' && $row->details->type == 'morphedByMany') {
                $multi_select_morph[] = [
                    'model'      => $row->details->model,
                    'content'    => $this->getContentBasedOnType($request, $slug, $row, $row->details),
                    'pivot_name' => $row->details->pivot_name,
                ];
            }
        }

        foreach ($multi_select_morph as $sync_data) {
            $data->morphedByMany(
                $sync_data['model'],
                $sync_data['pivot_name'],
            )->sync($sync_data['content']);
        }

        return $result;
    }

    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter  = $dataType->server_side ? 'paginate' : 'get';
        $perPage = $request->get('per_page') ?? null;

        $search = (object) [
            'value'  => $request->get('s'),
            'key'    => $request->get('key'),
            'filter' => $request->get('filter'),
        ];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchNames = $dataType->browseRows->mapWithKeys(function ($row) {
                return [$row['field'] => $row->getTranslatedAttribute('display_name')];
            });
        }

        $orderGroup = [];
        if (isset($dataType->details->order_group) && is_array($dataType->details->order_group)) {
            foreach ($dataType->details->order_group as $item) {
                $orderGroup[] = $item;
            }
        }

        $orderBy         = $request->get('order_by', $dataType->order_column);
        $sortOrder       = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            $query = $model::select($dataType->name . '.*');

            if ($dataType->scope && $dataType->scope != ''
                && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
                $query->{$dataType->scope}();
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))
                && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query           = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            $query = $this->indexSearch($search, $dataType, $query);

            $row = $dataType->rows->where('field', $orderBy)->firstWhere('type', 'relationship');

            $dataTypeContent = $this->indexOrderBy(
                $query, $dataType, $row,
                $orderBy, $sortOrder, $orderGroup,
                $getter, $perPage, $model
            );
            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter], $perPage);
            $model           = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'browse', $isModelTranslatable);

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index       =
                $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        // Define list of columns that can be sorted server side
        $sortableColumns = $this->getSortableColumns($dataType->browseRows);

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        return Voyager::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortableColumns',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn',
            'orderGroup'
        ));
    }

    public function getContentBasedOnType(Request $request, $slug, $row, $options = null)
    {
        switch ($row->type) {
            case 'coordinates':
                return (new CoordinateType($request, $slug, $row, $options))->handle();
            default:
                return parent::getContentBasedOnType($request, $slug, $row, $options);
        }
    }

    public function update(Request $request, $id)
    {
        set_session_redirect($request);
        $slug = $this->getSlug($request);
        View::share(['page_slug' => $slug, 'page_id' => $id]);

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        $query = $model->query();
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope' . ucfirst($dataType->scope))) {
            $query = $query->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
            $query = $query->withTrashed();
        }

        $data = $query->findOrFail($id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();

        // Get fields with images to remove before updating and make a copy of $data
        $to_remove     = $dataType->editRows->where('type', 'image')
            ->filter(function ($item, $key) use ($request) {
                return $request->hasFile($item->field);
            });
        $original_data = clone($data);

        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        // Delete Images
        $this->deleteBreadImages($original_data, $to_remove);

        event(new BreadDataUpdated($dataType, $data));

        if (auth()->user()->can('browse', app($dataType->model_name))) {
            if (!empty($request->get('redirect_to'))) {
                $redirect = redirect()->to($request->get('redirect_to'));
            } else {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            }
        } else {
            $redirect = redirect()->back();
        }

        return $redirect->with([
            'message'    => __('voyager::generic.successfully_updated')
                . " {$dataType->getTranslatedAttribute('display_name_singular')}",
            'alert-type' => 'success',
        ]);
    }

    public function store(Request $request)
    {
        set_session_redirect($request);
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val  = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        event(new BreadDataAdded($dataType, $data));

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', $data)) {
                if (!empty($request->get('redirect_to'))) {
                    $redirect = redirect()->to($request->get('redirect_to'));
                } else {
                    $redirect = redirect()->route("voyager.{$dataType->slug}.index");
                }
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_added_new')
                    . " {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    public function indexSearch($search, $dataType, $query)
    {
        if ($search->value != '' && $search->key && $search->filter) {
            $search_filter = ($search->filter == 'equals') ? '=' : 'ILIKE';
            $search_value  = ($search->filter == 'equals') ? $search->value : '%' . $search->value . '%';

            $searchField = $dataType->name . '.' . $search->key;
            if ($customQuery = $this->customSearches($query, $dataType->model_name, $searchField, $search_value)) {
                return $customQuery;
            }
            if ($row = $this->findSearchableRelationshipRow($dataType->rows->where('type', 'relationship'),
                $search->key)) {
                return $query->whereIn(
                    $searchField,
                    $row->details->model::where($row->details->label, $search_filter, $search_value)
                        ->pluck('id')
                        ->toArray()
                );
            } else {
                if ($dataType->browseRows->pluck('field')->contains($search->key)) {
                    return $query->where($searchField, $search_filter, $search_value);
                }
            }
        }

        return $query;
    }

    protected function customSearches($query, $model, $field, $search)
    {
        return null;
    }

    public function indexOrderBy(
        $query,
        $dataType,
        $row,
        $orderBy,
        $sortOrder,
        $orderGroup,
        $getter,
        $perPage,
        $model
    ) {
        if ($orderBy
            && (in_array($orderBy, array_merge($dataType->fields(), $this->customOrderByFields))
                || !empty($row))) {
            $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
            if ($customQuery =
                $this->customOrderBy($query, '', $orderBy, $querySortOrder)) {

                return call_user_func([$customQuery, $getter], $perPage);
            }
            if (!empty($row)) {
                $query->select([
                    $dataType->name . '.*',
                    'joined.' . $row->details->label . ' as ' . $orderBy,
                ])->leftJoin(
                    $row->details->table . ' as joined',
                    $dataType->name . '.' . $row->details->column,
                    'joined.' . $row->details->key
                );
            }
            foreach ($orderGroup as $item) {
                $query->orderBy($item, $querySortOrder);
            }

            return call_user_func([
                $query->orderBy($orderBy, $querySortOrder),
                $getter,
            ], $perPage);
        } elseif ($model->timestamps) {
            return call_user_func([$query->latest($model::CREATED_AT), $getter], $perPage);
        }

        return call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter], $perPage);
    }

    protected function customOrderBy($query, $model, $field, $sortOrder)
    {
        return null;
    }

}
