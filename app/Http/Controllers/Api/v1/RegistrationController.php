<?php

namespace App\Http\Controllers\Api\v1;

use App\Exceptions\IncorrectSmsCodeException;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheckSmsRequest;
use App\Http\Requests\CreateUserProfileRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\CheckIINService;
use App\Services\RegistrationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class RegistrationController extends Controller
{
    public RegistrationService $registrationService;

    public function __construct(RegistrationService $registrationService){
        $this->registrationService = $registrationService;
    }

    public function checkIIN($iin, CheckIINService $service)
    {
        return response([
            // существует ли у нас в базе
            'exists' => User::whereName($iin)->exists(),
            // существует ли в 1с
            'data' => $service->checkIIN1c($iin)
        ]);
    }

    public function register(CreateUserProfileRequest $request)
    {

        $request->merge(['password' => Hash::make($request->password)]);

        $phone = $this->registrationService->authorize($request->all());

        return response([
            'phone' => $phone,
            'message' => 'Successfully saved',
        ]);
    }

    /**
     * Сохранение пользователя
     * @param CheckSmsRequest $request
     * @return JsonResponse
     * @throws IncorrectSmsCodeException
     */
    public function submitAccount(CheckSmsRequest $request)
    {
        $user = $this->registrationService->submitAccount($request->phone, $request->code);

        return (new UserResource($user))->response([
            'message' => 'Account has been submitted!'
        ]);
    }
}
