<?php

namespace App\Http\Controllers\Api\v1;

use App\Exceptions\WaitToSendSMSException;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateForgotPasswordRequest;
use App\Models\User;
use App\Services\ChangePasswordService;
use App\Services\ForgotPasswordService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    protected ForgotPasswordService $forgotPasswordService;

    public function __construct(ForgotPasswordService $forgotPasswordService)
    {
        $this->forgotPasswordService = $forgotPasswordService;
    }

    /**
     * @throws WaitToSendSMSException
     */
    public function forgotPassword(Request $request)
    {
        // todo validate request
        $request->validate([
            'name' => 'required|exists:users,name',
        ]);

        if ($phone = $this->forgotPasswordService->forgotPassword($request->name)) {
            return response([
                'message' => 'Message sent successfully!',
                'phone' => $phone,
            ], 201);
        }
        return response([
            'error' => 'Wait a minute to send a message!',
        ], 425);
    }

    public function checkSmsCode()
    {
        // todo validate request
        $name = request('name');
        $sms_code = request('sms_code');
        if ($token = $this->forgotPasswordService->checkSmsCode($name, $sms_code)) {
            return response([
                'message' => 'Sms code is correct!',
                'password_token' => $token
            ]);
        }
        return response([
            'error' => 'Incorrect SMS Code'
        ], 422);
    }

    public function changePassword(UpdateForgotPasswordRequest $request, ChangePasswordService $service): JsonResponse
    {
        return $service->passwordResetProcess($request);
    }
}
