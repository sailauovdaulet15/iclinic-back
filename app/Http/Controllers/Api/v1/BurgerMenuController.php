<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\BurgerMenuResource;
use App\Models\BurgerMenu;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BurgerMenuController extends Controller
{
    public function index(): AnonymousResourceCollection
    {
        return BurgerMenuResource::collection(BurgerMenu::all());
    }
}
