<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderingCall\OrderingCallStoreRequest;
use App\Http\Resources\OrderingCallResource;
use App\Services\OrderingCallService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderingCallController extends Controller
{
    private OrderingCallService $orderingCallService;

    public function __construct(OrderingCallService $orderingCallService)
    {
        $this->orderingCallService = $orderingCallService;
    }

    public function store(OrderingCallStoreRequest $request): OrderingCallResource
    {
        $orderingCall = $this->orderingCallService->store($request->all());
        return new OrderingCallResource($orderingCall);
    }
}
