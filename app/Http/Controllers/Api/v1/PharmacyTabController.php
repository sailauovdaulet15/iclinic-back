<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\PharmacyTab;

class PharmacyTabController extends Controller
{

    public function index()
    {
        $pharmacyTabs = PharmacyTab::all();

        return response($pharmacyTabs)
            ->header('Cache-Control', 'public, max-age: 3600');
    }
}
