<?php

namespace App\Http\Controllers\Api\v1;

use App\Enums\AboutUsEnum;
use App\Http\Controllers\Controller;
use App\Services\AboutUsService;

class AboutUsController extends Controller
{
    private AboutUsService $aboutUsService;

    public function __construct(AboutUsService $aboutUsService)
    {
        $this->aboutUsService= $aboutUsService;
    }


    public function index()
    {
        $aboutUsList = $this->aboutUsService->getUrls();
        return response($aboutUsList)
            ->header('Cache-Control', 'public, max-age: 86400');
    }

    public function show($slug)
    {
        abort_if(!in_array($slug, AboutUsEnum::getConstants()), 404);
        return $this->aboutUsService->takeAboutUsSeparately($slug);
    }
}
