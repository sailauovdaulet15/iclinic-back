<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\HandbookBloodGroupResource;
use App\Models\HandbookBloodGroup;

class HandbookBloodGroupController extends Controller
{
    public function index()
    {
        $handbookBloodGroups = HandbookBloodGroup::query()->get();
        return HandbookBloodGroupResource::collection($handbookBloodGroups);
    }

    public function show($id)
    {
        $handbookBloodGroup = HandbookBloodGroup::find($id);

        return new HandbookBloodGroupResource($handbookBloodGroup);
    }
}
