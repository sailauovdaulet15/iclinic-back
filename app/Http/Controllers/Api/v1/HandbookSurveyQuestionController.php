<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\SurveyQuestionCollection;
use App\Services\HandbookSurveyQuestionService;

class HandbookSurveyQuestionController extends Controller
{
    private HandbookSurveyQuestionService $handbookSurveyQuestionService;

    public function __construct(HandbookSurveyQuestionService $handbookSurveyQuestionService)
    {
        $this->handbookSurveyQuestionService = $handbookSurveyQuestionService;
    }

    public function index()
    {
        $result = $this->handbookSurveyQuestionService->getAll();
        return new SurveyQuestionCollection($result);
    }
}
