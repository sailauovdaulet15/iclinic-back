<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Controller;
use App\Http\Resources\ServiceCollection;
use App\Http\Resources\ServiceResource;
use App\Services\ServiceService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ServiceController extends Controller
{
    protected ServiceService $serviceService;

    public function __construct(ServiceService $serviceService)
    {
        $this->serviceService = $serviceService;
    }

    public function index(): ServiceCollection
    {
        $services = $this->serviceService->takeServicesTree();
        return new ServiceCollection($services);
    }

    public function show($slug): ServiceResource
    {
        $service = $this->serviceService->takeOne($slug);
        return new ServiceResource($service);
    }

    public function getByDoctor(string $slug): AnonymousResourceCollection
    {
        $data = $this->serviceService->takeServicesByDoctorSlug($slug);
        return ServiceResource::collection($data);
    }

    public function getBySpecialisation(int $id)
    {
        $data = $this->serviceService->takeServicesBySpecialisation($id);
        return ServiceResource::collection($data);
    }

    public function getCountBySpecialisation(int $id)
    {
        return $this->serviceService->getCount($id) ?? 0;
    }

    public function getRecommended(int $id): AnonymousResourceCollection
    {
        $result = $this->serviceService->recommended($id);
        return ServiceResource::collection($result);
    }
}
