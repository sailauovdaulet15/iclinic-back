<?php

namespace App\Http\Controllers\Api\v1;

use App\Enums\MainEnum;
use App\Http\Controllers\Controller;
use App\Services\MainPageService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

class MainController extends Controller
{
    private MainPageService $mainPageService;

    public function __construct(MainPageService $mainPageService)
    {
        $this->mainPageService = $mainPageService;
    }

    /**
     * Urls for Main Page
     *
     * @return Application|ResponseFactory|Response
     */
    public function index()
    {
        $mainListWithUrls = $this->mainPageService->getUrls();

        return response($mainListWithUrls)
            ->header('Cache-Control', 'public, max-age: 86400');
    }


    /**
     * Shows by constant in MainEnum
     * @param $slug
     * @return AnonymousResourceCollection
     */
    public function show($slug)
    {
        return $this->mainPageService->takeMainSeparately($slug);
    }
}
