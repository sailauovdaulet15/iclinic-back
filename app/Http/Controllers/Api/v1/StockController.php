<?php

namespace App\Http\Controllers\Api\v1;

use App\Enums\AboutUsEnum;
use App\Http\Controllers\Controller;
use App\Http\Resources\StockResource;
use App\Services\AboutUsService;
use App\Services\StockService;

class StockController extends Controller
{
    protected StockService $stockService;

    public function __construct(StockService $stockService)
    {
        $this->stockService = $stockService;
    }

    public function index()
    {
        return StockResource::collection($this->stockService->takeAll());
    }
}
