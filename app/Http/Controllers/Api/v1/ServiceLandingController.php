<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\ServiceLandingResource;
use App\Models\ServiceLanding;

class ServiceLandingController extends Controller
{
    public function show($slug)
    {
        return (new ServiceLandingResource(
            ServiceLanding::whereSlug($slug)
                ->with([
                    'reviews', 'recommendServices',
                    'specialisation.services' => fn($q) => $q->orderBy('title')->limit(7),
                    'specialisation.doctors' => fn($q) => $q->limit(12),
                    'specialisation.symptoms' => fn($q) => $q->orderBy('position'),
                ])
                ->firstOrFail()
        ));
    }

}
