<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserProfileRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\CheckIINService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'checkIIN']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return Application|ResponseFactory|JsonResponse|\Illuminate\Http\Response
     */
    public function login()
    {
        $credentials = request()->validate([
            'name' => 'required|exists:users,name',
            'password' => 'required',
        ]);

        $ttl = request()->has('remember') ? 20160 : config("jwt.ttl");
        if (!$token = auth('api')->setTtl($ttl)->attempt($credentials)) {
            return response([
                "message" => __('validation.password'),
                "errors" => [
                    "password" => array(__('validation.pass'))
                ]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return UserResource
     */
    public function me()
    {
        $user = auth('api')->user()->load('role');
        return (new UserResource($user));
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}
