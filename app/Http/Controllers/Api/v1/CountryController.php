<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\CountryResource;
use App\Models\Country;
use Illuminate\Http\Request;

class
CountryController extends Controller
{
    public function index()
    {
        $countries = Country::query()->get();
        return CountryResource::collection($countries);
    }

    public function show($id)
    {
        $country = Country::find($id);

        return new CountryResource($country);
    }
}
