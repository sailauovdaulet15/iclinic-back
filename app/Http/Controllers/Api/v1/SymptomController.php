<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\SymptomResource;
use App\Services\SymptomService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class SymptomController extends Controller
{
    private SymptomService $symptomService;

    public function __construct(SymptomService $symptomService)
    {
        $this->symptomService = $symptomService;
    }

    public function show(int $id):AnonymousResourceCollection
    {
        return SymptomResource::collection($this->symptomService->getSymptom($id));
    }
}
