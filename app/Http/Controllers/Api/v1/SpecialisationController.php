<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\DoctorListResource;
use App\Http\Resources\ServiceCollection;
use App\Http\Resources\SpecialisationResource;
use App\Services\SpecialisationService;

class SpecialisationController extends Controller
{
    private SpecialisationService $specialisationService;

    public function __construct(SpecialisationService $specialisationService)
    {
        $this->specialisationService= $specialisationService;
    }


    public function index()
    {
        return SpecialisationResource::collection($this->specialisationService->takeAll());
    }

    public function show(string $slug)
    {
        return new SpecialisationResource($this->specialisationService->takeOne($slug));
    }

    public function services(string $slug)
    {
        return new ServiceCollection($this->specialisationService->takeServices($slug));
    }

    public function doctors(string $slug)
    {
        return DoctorListResource::collection($this->specialisationService->takeDoctors($slug));
    }
}
