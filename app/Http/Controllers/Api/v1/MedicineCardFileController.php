<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\MedicineCardFile;
use Illuminate\Http\Request;

class MedicineCardFileController extends Controller
{
    public function isReadOn(int $id): \Illuminate\Http\JsonResponse
    {
        try {
            MedicineCardFile::query()->findOrFail($id)->update([
                'is_read' => true
            ]);
            return response()->json('Is read is true', 200);

        } catch (\Exception $exception) {
            return response()->json($exception, 500);

        }
    }

    public function isReadOff(int $id): \Illuminate\Http\JsonResponse
    {
        try {
            MedicineCardFile::query()->findOrFail($id)->update([
                'is_read' => false
            ]);
            return response()->json('Is read is false', 200);
        } catch (\Exception $exception) {
            return response()->json($exception, 500);

        }
    }
}
