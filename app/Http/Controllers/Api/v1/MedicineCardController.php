<?php

namespace App\Http\Controllers\Api\v1;

use App\Enums\AboutUsEnum;
use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\Doctor;
use App\Models\HistoryVisit;
use App\Models\User;
use App\Services\AboutUsService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class MedicineCardController extends Controller
{

    public function index(Request $request)
    {
        /** @var User $user */
        $user = auth('api')->user();

        if ($request->get('group_by') == 'doctors') {
            return [];
        } else {
            /** @var Appointment[] $consultationMedicineCards */
            $appointmentMedicineCards = $user
                ->appointments()
                ->whereHas('medicineCards', function ($query) {
                    $query->has('files');
                })
                ->has('doctor')
                ->with(['medicineCards' => function ($query) {
                    $query->with('files');
                }])
                ->orderBy('date')
                ->orderBy('start_time')
                ->get();

            $result = [];
            foreach ($appointmentMedicineCards as $appointmentMedicineCard) {
                $files = [];
                foreach ($appointmentMedicineCard->medicineCards as $card) {
                    foreach ($card->files as $file) {
                        $files[] = [
                            'id'      => $file->id,
                            'time'    => Carbon::parse($appointmentMedicineCard->start_time)
                                ->format('H:i'),
                            'title'   => $file->file_name,
                            'link'    => $file->file,
                            'is_read' => $file->is_read,
                        ];
                    }
                }

                if (!empty($files)) {
                    $date                     = \Carbon\Carbon::parse($appointmentMedicineCard->date)
                        ->format('d.m.Y');
                    $doctorId                 = $appointmentMedicineCard->doctor_user_id;
                    if (isset($result[$date]) && isset($result[$date][$doctorId])) {
                        $result[$date][$doctorId] = array_merge($result[$date][$doctorId], $files);
                    } else {
                        $result[$date][$doctorId] = $files;
                    }
                }

            }
            $gg = [];
            foreach ($result as $key => $value) {
                $doctors = [];
                foreach ($value as $k => $v) {
                    $doctors[] = [
                        'doctor_name'    => Doctor::whereUserId($k)->first()->nameAndSpecialisation,
                        'doctor_user_id' => $k,
                        'appointments'   => $v,
                    ];
                }
                $gg[] = ['date' => $key, 'doctors' => $doctors];

            }
        }

        return [
            'data' => $gg,
        ];
    }
}
