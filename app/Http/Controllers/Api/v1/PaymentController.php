<?php


namespace App\Http\Controllers\Api\v1;


use App\Http\Controllers\Controller;
use App\Jobs\Soap\CreateHistoryVisitJob;
use App\Models\Appointment;
use App\Models\FailurePostLink;
use App\Models\PostLink;
use App\Soap\Request\HistoryVisit;
use Clockwork\Request\Log;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function getToken(Request $request): string
    {
        $postfields = array(
            'grant_type' => 'client_credentials',
            'scope' => 'webapi usermanagement email_send verification statement statistics payment',
            'invoiceID' =>	$request->invoiceId ?? rand(100000, 100000000000),
            'amount'  => $request->amount ?? 0,
            'currency' => "KZT",
            'client_id' => env('PAYMENT_CLIENT_ID'),
            'client_secret' => env('PAYMENT_CLIENT_SECRET'),
            'terminal' => env('PAYMENT_TERMINAL_ID'));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://epay-oauth.homebank.kz/oauth2/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        return curl_exec($ch);
    }

    public function getPaymentInfo(): string
    {
        $rand = rand(100000, 100000000000);
        while ($rand) {
            if (!Appointment::where('invoiceId', '=', $rand)->first()) {
                break;
            }
            $rand = rand(100000, 100000000000);
        }

        $data = [
            'terminal' => env('PAYMENT_TERMINAL_ID'),
            'invoiceId' => $rand
        ];

        return json_encode($data);
    }

    public function postLink(Request $request): void
    {
        $postLink = PostLink::query()->create($request->all());
        CreateHistoryVisitJob::dispatch($postLink);
    }


    public function failurePostLink(Request $request): void
    {
        FailurePostLink::query()->create($request->all());
    }


}
