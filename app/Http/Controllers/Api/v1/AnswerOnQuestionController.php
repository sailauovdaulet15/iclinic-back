<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAnswerOnQuestionRequest;
use App\Http\Requests\StoreManyAnswerOnQuestionRequest;
use App\Http\Resources\AnswerOnQuestionResource;
use App\Services\AnswerOnQuestionService;
use Symfony\Component\HttpFoundation\Response;


class AnswerOnQuestionController extends Controller
{
    private AnswerOnQuestionService $answerOnQuestionService;

    public function __construct(AnswerOnQuestionService $answerOnQuestionService)
    {
        $this->answerOnQuestionService = $answerOnQuestionService;
    }

    public function store(StoreAnswerOnQuestionRequest $request): AnswerOnQuestionResource
    {
        $data = $request->validated();
        return new AnswerOnQuestionResource($this->answerOnQuestionService->store($data));
    }

    public function storeMany(StoreManyAnswerOnQuestionRequest $request)
    {
        if ($this->answerOnQuestionService->storeMany($request->all())) {
            return response('Успешно сохранены', Response::HTTP_CREATED);
        }
        return response('Данные не сохранились', Response::HTTP_BAD_REQUEST);
    }

}
