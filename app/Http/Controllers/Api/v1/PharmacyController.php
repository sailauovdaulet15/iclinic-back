<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Pharmacy;

class PharmacyController extends Controller
{

    public function index()
    {
        $pharmacies = Pharmacy::when(request('pharmacy_tab_id'), function ($query, $search) {
            return $query->where('pharmacy_tab_id', $search);
        })->paginate();

        return response($pharmacies)
            ->header('Cache-Control', 'public, max-age: 3600');
    }
}
