<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\DoctorListResource;
use App\Http\Resources\DoctorResource;
use App\Services\DoctorService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class DoctorController extends Controller
{
    private DoctorService $doctorService;

    public function __construct(DoctorService $doctorService)
    {
        $this->doctorService = $doctorService;
    }

    public function index()
    {
        return DoctorResource::collection($this->doctorService->takeAll());
    }

    public function show($slug): DoctorResource
    {
        return new DoctorResource($this->doctorService->show($slug));
    }

    public function visits(Request $request, $slug)
    {
        $visits = $this->doctorService->takeDoctorVisits($slug, $request->all());
        return $visits;
    }

    public function getDoctorsByService(int $id): AnonymousResourceCollection
    {
        return DoctorListResource::collection($this->doctorService->takeDoctorsByServiceId($id));
    }

    public function getDoctorsByServices(): AnonymousResourceCollection
    {

        return DoctorListResource::collection($this->doctorService->takeDoctorsByServices());
    }

    public function countBySpecialisation(int $id): int
    {
        return $this->doctorService->getCount($id) ?? 0;
    }

}
