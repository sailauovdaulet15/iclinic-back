<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\NewsResource;
use App\Services\NewsService;

class NewsController extends Controller
{
    protected NewsService $newsService;

    public function __construct(NewsService $newsService)
    {
        $this->newsService = $newsService;
    }


    public function index() {
        $news = $this->newsService->takeAll();
        return NewsResource::collection($news);
    }

    public function show($slug) {
        $news = $this->newsService->takeOne($slug);
        return new NewsResource($news);
    }
}
