<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\FooterResource;
use App\Models\Footer;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class FooterController extends Controller
{
    public function index(): AnonymousResourceCollection
    {
        return FooterResource::collection(Footer::all());
    }
}
