<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use SoapClient;

class FaqController extends Controller
{

    public function index()
    {
        $faqs = Faq::query()->orderBy('position')->get()->translate();

        return response($faqs)
            ->header('Cache-Control', 'public, max-age: 86400');
    }

    public function test()
    {
        $client = new SoapClient("https://test.iclinic.kz/pub/ws/iClinic?wsdl");
        dd($client);
    }
}
