<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangeAvatarRequest;
use App\Http\Requests\CreateUserProfileRequest;
use App\Http\Requests\UpdateUserPasswordRequest;
use App\Http\Requests\UpdateUserProfileRequest;
use App\Http\Resources\HistoryVisitResource;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\ChangeAvatarService;
use App\Services\HistoryVisitService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function update(UpdateUserProfileRequest $request): JsonResponse
    {
        $profile = auth('api')->user()->load('role');

        $profile->update($request->all());

        return (new UserResource($profile))->response([
            'message' => 'Profile updated',
        ]);
    }

    public function updatePassword(UpdateUserPasswordRequest $request): JsonResponse
    {
        $profile = auth('api')->user()->load('role');
        $profile->update([
            'password' => bcrypt($request->password)
        ]);

        return (new UserResource($profile))->response([
            'message' => 'Ваш пароль успешно изменен',
        ]);
    }

    public function changeAvatar(ChangeAvatarRequest $request, ChangeAvatarService $service)
    {
        $avatarImagePath = $service->changeAvatar($request);
        return response([
            'data' => [
                'avatar' => $avatarImagePath
            ]
        ]);
    }

    public function history(HistoryVisitService $historyVisitService)
    {
        return HistoryVisitResource::collection($historyVisitService->takeByUserId(auth('api')->id()));
    }
}
