<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppointmentStoreRequest;
use App\Http\Resources\AppointmentHistoryResource;
use App\Http\Resources\AppointmentListResource;
use App\Http\Resources\AppointmentResource;
use App\Services\Appointment\AppointmentService;
use App\Soap\SoapService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;


class AppointmentController extends Controller
{
    protected AppointmentService $appointmentService;

    public function __construct(AppointmentService $appointmentService)
    {
        $this->appointmentService = $appointmentService;
    }

    public function store(AppointmentStoreRequest $request)
    {
        $data = $request->all();
        return $this->appointmentService->storeArrayByStep($data);
    }

    public function view()
    {
        return new AppointmentResource($this->appointmentService->getResult());
    }

    public function destroyRedis()
    {
        $this->appointmentService->redisDelete();
    }

    public function showClientHistory()
    {
        $userIIN = auth('api')->user()->name ?? request('iin');
        $clientHistory = app(SoapService::class)->getClientVisitHistory($userIIN);

        return AppointmentHistoryResource::collection($clientHistory);
    }

    public function userAppointment(): AnonymousResourceCollection
    {
        $result = $this->appointmentService->getAppointmentByUser();
        return AppointmentListResource::collection($result);
    }

    public function destroy(int $id)
    {
        $this->appointmentService->delete($id);
    }

    public function onlineArchiveAppointment(): AnonymousResourceCollection
    {
        return AppointmentListResource::collection($this->appointmentService->getArchive());
    }

    public function onlineScheduledAppointment(): AnonymousResourceCollection
    {
        return AppointmentListResource::collection($this->appointmentService->getPlanned());

    }
}
