<?php

namespace App\Http\Controllers;

use App\Http\Requests\MailingStoreRequest;
use App\Http\Resources\MailingResource;
use App\Http\Resources\UserResource;
use App\Services\MailingService;
use Illuminate\Http\Request;

class MailingController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    protected MailingService $mailingService;

    public function __construct(MailingService $mailingService)
    {
        $this->middleware('auth:api');
        $this->mailingService = $mailingService;
    }

    public function updateMailing(MailingStoreRequest $request)
    {
        $mailing = $this->mailingService->updateMailing($request->all());
        return new MailingResource($mailing->load('user'));
    }
}
