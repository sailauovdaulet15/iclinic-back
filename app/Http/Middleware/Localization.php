<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Localization
{

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $locale = $request->header('Content-Language');

        // if the header is missed
        if(!$locale && config('voyager.multilingual.enabled')){
            // take the default local language
            $locale = config('voyager.multilingual.default');
        }

        // check the languages defined is supported
        if (!in_array($locale, config('voyager.multilingual.locales'))) {
            // respond with error
            return abort(403, 'Language not supported.');
        }

        app()->setLocale($locale);

        // get the response after the request is done
        $response = $next($request);

        $response->headers->set('Content-Language', $locale);

        return $response;
    }
}
