<?php

namespace App\Voyager\ContentTypes;

use TCG\Voyager\Http\Controllers\ContentTypes\BaseType;

class JsonOneFieldOptionContentType extends BaseType
{
    public function handle()
    {
        $value = $this->request->input($this->row->field);


        $new_parameters = [];
        foreach ($value as $key => $val) {
            if ($val['field_one']) {
                $new_parameters[] = array_merge($val, ['index' => $key + 1]);
            }
        }


        return $new_parameters;
    }
}
