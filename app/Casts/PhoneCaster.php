<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsInboundAttributes;

class PhoneCaster implements CastsInboundAttributes
{
    public function __construct()
    {
    }

    public function set($model, string $key, $value, array $attributes)
    {
        if (preg_match('/^\d-(\d{3})-(\d{3})-(\d{2})-(\d{2})$/', $value, $matches)) {
            $value = "{$matches[1]}){$matches[2]}{$matches[3]}{$matches[4]}";
        }
        return [$key => $value];
    }
}
