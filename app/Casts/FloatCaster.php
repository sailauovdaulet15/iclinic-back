<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsInboundAttributes;

class FloatCaster implements CastsInboundAttributes
{
    public function __construct($length = 2)
    {
        $this->length = $length;
    }

    public function set($model, string $key, $value, array $attributes)
    {
        return [$key => round($value, $this->length)];
    }
}
