<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

trait SluggableTrait
{
    public static function bootSluggableTrait()
    {
        static::creating(function ($model) {
            $model->generateSlug();
        });
    }

    public function generateSlug()
    {
        $containValues = [];
        foreach ($this->getSluggablesProperty() as $item) {
            $containValues[$item] = $this->$item;
        }

        $value = $this->makeUniqueSlug($containValues);
        $sluggable_column = $this->getSlugColumn();
        $this->$sluggable_column = empty($value) ? null : $value;
    }

    public function makeUniqueSlug($values)
    {
        $slug = Str::slug(implode('-', $values));
        $user = null;
        //'SIMILAR TO' is not compatible with sqlite, so using LIKE.
        if (DB::getDriverName() === 'sqlite') {
            $userNumber = static::where(static::getSlugColumn(), $slug)->count();
            if ($userNumber === 1) {
                $user = static::where(static::getSlugColumn(), $slug)->first();
            } else if ($userNumber > 1) {
                $user = static::where(static::getSlugColumn(), 'LIKE', $slug . '-%')->orderBy('id', 'DESC')->first();
            }
        } else {
            $user = static::orderBy('id', 'DESC')->where(static::getSlugColumn(), 'SIMILAR TO', $slug . '(-\d)?')->orderBy('id', 'DESC')->first();
        }

        if (!empty($user)) {
            $max = $user->slug;
            if (!empty($max)) {
                if (is_numeric($max[-1])) {
                    return preg_replace_callback('/(\d+)$/', function ($matches) {
                        return $matches[1] + 1;
                    }, $max);
                }
            }
            return "{$slug}-2";
        }
        return $slug;
    }


    public function getSluggablesProperty(): array
    {
        return $this->sluggables ?? [];
    }

    public function getSlugColumn(): string
    {
        return $this->slug_column ?? 'slug';
    }
}
