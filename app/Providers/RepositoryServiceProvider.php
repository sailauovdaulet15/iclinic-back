<?php

namespace App\Providers;

use App\Repositories\DatabaseSearchDoctorRepositoryRepository;
use App\Repositories\Interfaces\SearchDoctorRepositoryInterface;
use App\Repositories\ScoutSearchDoctorRepositoryRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SearchDoctorRepositoryInterface::class, DatabaseSearchDoctorRepositoryRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
