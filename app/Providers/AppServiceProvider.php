<?php

namespace App\Providers;

use App\Services\SendpulseService;
use App\Soap\SoapService;
use App\Voyager\FormFields\PolymorphicFormField;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use MonstreX\VoyagerExtension\Controllers\VoyagerExtensionBaseController;
use Voyager;

class  AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        Voyager::addFormField(JsonOneFieldOptionFormField::class);
//        Voyager::addFormField(JsonTwoFieldOptionFormField::class);
//
//        $this->app->bind(
//            'TCG\Voyager\Http\Controllers\VoyagerBaseController',
//            'App\Voyager\Controllers\JsonOneFieldOptionController'
//        );
//
//        $this->app->bind(
//            'TCG\Voyager\Http\Controllers\VoyagerBaseController',
//            'App\Voyager\Controllers\JsonTwoFieldOptionController'
//        );

        $this->app->bind(
            VoyagerExtensionBaseController::class,
            \App\Http\Controllers\VoyagerExtension\VoyagerExtensionBaseController::class
        );
        Voyager::addFormField(PolymorphicFormField::class);
        // singleton for SoapService
        $this->app->singleton(SoapService::class, function ($app) {
            return new SoapService();
        });

        $this->app->singleton(SendpulseService::class, function ($app) {
            return new SendpulseService();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
