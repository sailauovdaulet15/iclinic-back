<?php


namespace App\Constants;


class LanguageConstant
{
    public const RU = 'ru';
    public const KZ = 'kz';
    public const EN = 'en';

    public static function getTitle($key): string
    {
        switch ($key){
            case self::RU:
                return 'Русский';
            case self::KZ:
                return 'Казахский';
            case self::EN:
                return 'Английский';
            default:
                return 'НеУказан';
        }
    }

    public static function getTitleWithKey(): array
    {
        return [
            self::RU => self::getTitle(self::RU),
            self::KZ => self::getTitle(self::KZ),
            self::EN => self::getTitle(self::EN)
        ];
    }


    public static function getKey(string $title): ?string
    {
        foreach (self::getTitleWithKey() as $key => $value) {
            if ($value == $title) {
                return $key;
            }
        }
        return null;
    }
}
