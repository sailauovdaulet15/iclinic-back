<?php

namespace App\Constants;

class EnvironmentConstant
{
    public const TESTING = 'testing';
    public const LOCAL = 'local';
    public const PRODUCTION = 'production';
}
