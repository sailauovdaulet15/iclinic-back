<?php

namespace Tests\Feature;

use App\Models\Doctor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use function PHPUnit\Framework\assertNotNull;
use function PHPUnit\Framework\assertTrue;

class DoctorTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_it_works()
    {
        $this->withoutExceptionHandling();
        $response = $this->get('/api/v1/doctors');
        $response->assertStatus(200);
    }

    public function test_it_has_description()
    {
        $doctor = Doctor::factory()->create();
        assertNotNull(Doctor::find($doctor->user_id));
    }
}
