<?php

namespace Tests\Feature;

use App\Models\SmsMessage;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class ForgotPasswordTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_forgot_password()
    {
        $data = User::factory()->make()->getAttributes();
        $data['password'] = Hash::make($data['password']);
        User::create($data);
        $newPassword = $this->faker->password(4) . $this->faker->regexify('[A-Z]{2}[0-9]{2}');
        $this->post('/api/v1/auth/forgot-password', ['name' => $data['name']])->assertStatus(201);
        $smsCode = SmsMessage::query()->orderByDesc('id')->where('phone', $data['phone'])->first()->getAttribute('sms_code');
        $passwordToken = json_decode($this->json('post', '/api/v1/auth/check-sms-code', ['name' => $data['name'], 'sms_code' => $smsCode])->assertStatus(200)->assertJsonStructure(['message', 'password_token'])->getContent())->password_token;
        $this->json('POST', '/api/v1/auth/change-password', ['name' => $data['name'], 'token' => $passwordToken, 'password' => $newPassword, 'password_confirmation' => $newPassword])->assertJsonStructure(['message']);
    }

}
