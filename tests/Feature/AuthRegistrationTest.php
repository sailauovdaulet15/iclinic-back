<?php

namespace Tests\Feature;

use App\Models\SmsMessage;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redis;
use phpDocumentor\Reflection\Types\Null_;
use Tests\TestCase;

class AuthRegistrationTest extends TestCase
{

    use RefreshDatabase;


    public function setUp(): void
    {
        parent::setUp();

    }

    /**
     * A basic feature test example.
     *
     * @return array
     */
    public function test_auth_registration(): array
    {
        $user = User::factory()->make()->getAttributes();
        $phone = '+7' . $user['phone'];
        $user['password_confirmation'] = $user['password'];
        $response = $this->json('post', '/api/v1/auth/register', $user)->assertStatus(200)->assertJsonStructure(['phone', 'message']);
        self::assertTrue($phone == json_decode($response->getContent())->phone);
        dump($response->getContent());
        return $user;
    }

    /**
     * @depends test_auth_registration
     * @return array
     */
    public function test_submit($user): array
    {
        $sms_code = sprintf('%05d', rand(0, 99999));
        SmsMessage::query()->create([
            'phone' => $user['phone'],
            'sms_code' => $sms_code
        ]);
        $this->json('post', '/api/v1/auth/register/submit-account', ['phone' => $user['phone'], 'code' => $sms_code])->assertStatus(201)->assertJsonStructure(['data' => ["id", "name", "phone", "email", "avatar", "first_name", "second_name", "third_name", "nationality_id", "sex", "language", "is_limited_by_mobility", "is_notification", "is_push_notification", "postcode", "date_birth", "phone2", "address", "country", "region", "city", "legal_agent_id", "handbook_blood_group_id", "created_at", "updated_at"]]);
        return $user;
    }

    /**
     * @param $data
     * @return void
     * @depends test_submit
     */
    public function test_login($data): void
    {
        $password = $data['password'];
        $data['password'] = Hash::make($data['password']);
        User::create($data);
        $this->json('post', '/api/v1/auth/login', ['name' => $data['name'], 'password' => $password])->assertStatus(200)->assertJsonStructure(['access_token', 'token_type', 'expires_in']);
    }
}

