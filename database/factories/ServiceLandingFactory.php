<?php

namespace Database\Factories;

use App\Models\Service;
use App\Models\ServiceLanding;
use App\Models\Specialisation;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;

class ServiceLandingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ServiceLanding::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $gg = $this->faker->sentence;
        $image_path = 'service_landings'.DIRECTORY_SEPARATOR.date('FY').DIRECTORY_SEPARATOR.uniqid().".png";
        Storage::disk(config('voyager.storage.disk'))
            ->put($image_path,
                file_get_contents($this->faker->imageUrl()), 'public');
        return [
            'title' => $gg,
            'annotation' => $this->faker->sentence,
            'slug' => \Str::slug($gg),
            'image_uri' => $image_path,
            'specialisation_id' => Specialisation::factory()
        ];
    }
}
