<?php

namespace Database\Factories;

use App\Models\Service;
use App\Models\Specialisation;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Service::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence,
            'description' => $this->faker->sentence,
            'price_original' => $this->faker->numberBetween(0, 100000),
            'price_percent' => $this->faker->numberBetween(0, 20),
            'price_bonus' => $this->faker->numberBetween(0, 1000),
            'specialisation_id' => Specialisation::factory()
        ];
    }
}
