<?php

namespace Database\Factories;

use App\Models\MainServicePoint;
use App\Models\ServiceLanding;
use Illuminate\Database\Eloquent\Factories\Factory;

class MainServicePointFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MainServicePoint::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'key' => $this->faker->sentence,
            'description' => $this->faker->text,
            'specialisation_id' => ServiceLanding::factory(),
        ];
    }
}
