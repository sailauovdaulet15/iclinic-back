<?php

namespace Database\Factories;

use App\Models\Doctor;
use App\Models\HistoryVisit;
use App\Models\Service;
use App\Models\User;
use Closure;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;

class HistoryVisitFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = HistoryVisit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $doctor = Doctor::has('specialisations')->get()->random();
        return [
            'uid' => $this->faker->uuid(),
            'doctor_user_id' => $doctor->user_id,
            'user_id' => User::all()->random(),
            'date_of_receipt' => $this->faker->date(),
            'total_sum' => $this->faker->numberBetween(1000, 100000)
        ];
    }

    public function configure()
    {
        return $this->afterCreating(function (HistoryVisit $history) {
            /** @var Collection $services */
            $services = $history->doctor->specialisations()->with('services')->get()->pluck('services.*.id')->flatten();
            $history->services()
                ->sync($services
                    ->random(
                        rand(0, $services->count())
                    ));
        });
    }
}
