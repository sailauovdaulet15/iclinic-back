<?php

namespace Database\Factories;

use App\Enums\RoleEnum;
use App\Models\Doctor;
use App\Models\Profile;
use App\Models\Service;
use App\Models\Specialisation;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use TCG\Voyager\Models\Role;

class DoctorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Doctor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'user_id' => User::factory()->state([
                'role_id' => Role::where('name', RoleEnum::DOCTOR)->first()->id
            ]),
            'category' => $this->faker->jobTitle(),
            'experience' => 5
        ];
    }
}
