<?php

namespace Database\Seeders\data\originals;

use App\Models\ReceptionType;
use Illuminate\Database\Seeder;

class ReceptionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReceptionType::query()->updateOrCreate([
            'title' => 'Первичный прием',
            'type' => 'first_time_reception'
        ]);

        ReceptionType::query()->updateOrCreate([
            'title' => 'Повторный прием',
            'type' => 'repeated_reception'
        ]);


        ReceptionType::query()->updateOrCreate([
            'title' => 'Не указано',
            'type' => 'not_specified'
        ]);
    }
}
