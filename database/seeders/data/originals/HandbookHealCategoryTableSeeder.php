<?php

namespace Database\Seeders\data\originals;

use App\Enums\HandbookHealCategoryEnum;
use App\Models\HandbookHealCategory;
use Illuminate\Database\Seeder;

class HandbookHealCategoryTableSeeder extends Seeder
{
    public function run()
    {
        foreach (HandbookHealCategoryEnum::getConstants() as $type) {
            HandbookHealCategory::updateOrCreate(['type' => $type], [
                'title' => HandbookHealCategoryEnum::getTitle($type)
            ]);
        }
    }
}
