<?php

namespace Database\Seeders\data\originals;

use App\Enums\HandbookBloodGroupEnum;
use App\Models\HandbookBloodGroup;
use Illuminate\Database\Seeder;

class HandbookBloodGroupTableSeeder extends Seeder
{
    public function run()
    {
        if (HandbookBloodGroup::all()->isEmpty()) {
            foreach (HandbookBloodGroupEnum::getConstants() as $item) {
                HandbookBloodGroup::create(['title' => $item]);
            }
        }
    }
}
