<?php

namespace Database\Seeders\data\originals;

use App\Enums\HandbookOrderTypeEnum;
use App\Models\HandbookOrderType;
use Illuminate\Database\Seeder;

class HandbookOrderTypeTableSeeder extends Seeder
{
    public function run()
    {
        if (HandbookOrderType::all()->isEmpty()) {
            foreach (HandbookOrderTypeEnum::getTitleWithKey() as $key => $value) {
                HandbookOrderType::create(['name' => $value, 'type' => $key]);
            }
        }
    }
}
