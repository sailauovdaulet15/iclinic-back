<?php

namespace Database\Seeders\data\originals;

use App\Enums\HandbookBloodGroupEnum;
use App\Models\HandbookBloodGroup;
use App\Models\Specialisation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SpecialisationTableSeeder extends Seeder
{
    public function run()
    {
        if (Specialisation::count() < 22) {
            $str = "Акушер-гинеколог
                    Аллерголог
                    Гастроэнтеролог
                    Гематолог
                    Гинеколог
                    Дерматокосметолог
                    Кардиолог
                    Маммолог
                    Невролог
                    Нефролог
                    Ортопед
                    Оториноларинголог
                    Офтальмолог
                    Педиатр
                    Пластический хирург
                    Проктолог
                    Ревматолог
                    Сосудистый хирург
                    Терапевт
                    УЗИ-диагностика
                    Уролог-андролог
                    Эндокринолог";

            $data = (explode("\n", $str));
            foreach ($data as &$val) $val = ['title' => $val, 'slug' => Str::slug($val)];

            Specialisation::insert($data, ['title'], ['title']);
        }
    }
}
