<?php

namespace Database\Seeders\data\originals;

use App\Enums\FormOfParticipationEnum;
use App\Models\Country;
use App\Models\FormOfParticipation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormOfParticipationSeeder extends Seeder
{
    public function run()
    {
        foreach (FormOfParticipationEnum::getConstants() as $type => $title) {
            FormOfParticipation::updateOrCreate(['type' => $type], ['title' => $title]);
        }
    }
}
