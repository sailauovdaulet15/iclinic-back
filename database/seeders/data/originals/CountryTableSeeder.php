<?php

namespace Database\Seeders\data\originals;

use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountryTableSeeder extends Seeder
{
    public function run()
    {
        if (Country::count()) {
            return;
        }

        $file = fopen(storage_path('data/CountryList.csv'), "r");
        $importData_arr = [];
        $i = 1; //
        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
            $num = count($filedata);

            for ($c = 0; $c < $num; $c++) {
                $importData_arr[$i][] = $filedata[$c];
            }
            $i++;
        }
        fclose($file);
        $j = 1;
        foreach ($importData_arr as $importData) {
            $j++;
            try {
                DB::beginTransaction();
                Country::create([
                    'title' => $importData[0],
                    'code' => $importData[1],
                    'code_alfa_2' => $importData[2],
                    'code_alfa_3' => $importData[3],
                    'full_title' => $importData[4],
                ]);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
            }
        }
    }
}
