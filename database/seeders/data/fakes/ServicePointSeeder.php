<?php

namespace Database\Seeders\data\fakes;

use App\Models\Doctor;
use App\Models\MainServicePoint;
use App\Models\Service;
use App\Models\ServiceLanding;
use App\Models\Specialisation;
use App\Models\Symptoms;
use Illuminate\Database\Seeder;

class ServicePointSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (MainServicePoint::count() == 0) {
            foreach (Specialisation::take(18)->get() as $item) {
                MainServicePoint::factory()->state([
                    'category_id' => 1,
                    'specialisation_id' => $item
                ])->create();
                ServiceLanding::factory()->state([
                    'specialisation_id' => $item
                ])->has(Symptoms::factory()->count(5))->create();
            }
            foreach (Specialisation::skip(18)->take(4)->get() as $item) {
                MainServicePoint::factory()->state([
                    'category_id' => 2,
                    'specialisation_id' => $item
                ])->create();
                ServiceLanding::factory()->state([
                    'specialisation_id' => $item,
                ])->has(Symptoms::factory()->count(5))->create();
            }
        }


    }
}
