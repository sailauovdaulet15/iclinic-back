<?php

namespace Database\Seeders\data\fakes;

use App\Enums\NationalityEnum;
use App\Models\Doctor;
use App\Models\Nationality;
use App\Models\Service;
use App\Models\Specialisation;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class NationalitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Doctor::count() == 0) {
            $services = Service::factory()->count(50)->create();
            $specialisations = Specialisation::all();
            Doctor::factory()->count(20)->create()->each(function ($doctor) use ($services, $specialisations) {
                $doctor->specialisations()->sync($specialisations->random(rand(1, 5)));
                $doctor->services()->sync($services->random(rand(1,6)));
            });
        }

        if (User::whereEmail('admin@rocketfirm.net')->doesntExist()) {
            User::create([
                'email' => 'admin@rocketfirm.net',
                'name' => 'admin',
                'password' => Hash::make('123456'),
                'role_id' => 1,
            ]);
        }
    }
}
