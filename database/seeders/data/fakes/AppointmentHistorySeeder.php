<?php

namespace Database\Seeders\data\fakes;

use App\Enums\NationalityEnum;
use App\Models\Doctor;
use App\Models\HistoryVisit;
use App\Models\Nationality;
use App\Models\Specialisation;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AppointmentHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (HistoryVisit::count() == 0) {
            User::limit(2)->get()->each(function($user) {
                HistoryVisit::factory()->count(20)->state([
                    'user_id' => $user->id
                ])->create();
            });
        }
    }
}
