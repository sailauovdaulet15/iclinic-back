<?php

namespace Database\Seeders;

use Database\Seeders\voyagers\basis\PermissionRoleSeeder;
use Database\Seeders\voyagers\basis\RolesTableSeeder;
use Illuminate\Database\Seeder;

class TestDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionRoleSeeder::class);
        $this->call(AdditionalDataSeeder::class);
        $this->call(\Database\Seeders\data\fakes\NationalitySeeder::class);
        $this->call(\Database\Seeders\data\fakes\ServicePointSeeder::class);
    }
}
