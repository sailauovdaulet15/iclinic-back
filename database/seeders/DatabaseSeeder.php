<?php

namespace Database\Seeders;

use Database\Seeders\voyagers\{basis\MenuItemsTableSeeder,
    basis\MenusTableSeeder,
    basis\PermissionRoleSeeder,
    basis\PermissionsTableSeeder,
    basis\RolesTableSeeder,
    basis\SettingsTableSeeder,
    breads\AboutUsAchievementTableSeeder,
    breads\AboutUsBranchTableSeeder,
    breads\AboutUsLicenseTableSeeder,
    breads\AboutUsSliderTableSeeder,
    breads\AboutUsTableSeeder,
    breads\AboutUsWhyUsTableSeeder,
    breads\AnswerOnQuestionTableSeeder,
    breads\AppointmentTableSeeder,
    breads\CanceledAppointmentTableSeeder,
    breads\CountryTableSeeder,
    breads\DoctorServiceTableSeeder,
    breads\BonusTableSeeder,
    breads\BurgerMenuTableSeeder,
    breads\FooterTableSeeder,
    breads\HandbookBloodGroupTableSeeder,
    breads\CityTableSeeder,
    breads\ContactTableSeeder,
    breads\DoctorTableSeeder,
    breads\FaqTableSeeder,
    breads\HandbookOrderStatusTableSeeder,
    breads\HandbookOrderTypeTableSeeder,
    breads\HistoryVisitPaymentTableSeeder,
    breads\HistoryVisitServiceTableSeeder,
    breads\HistoryVisitsTableSeeder,
    breads\HandbookSurveyQuestionTableSeeder,
    breads\IconTableSeeder,
    breads\MailingTableSeeder,
    breads\MainMenuTableSeeder,
    breads\MainPersonalAreaTableSeeder,
    breads\MainServiceTableSeeder,
    breads\MainServicePointTableSeeder,
    breads\MainSliderTableSeeder,
    breads\MainTableSeeder,
    breads\MedicineCardFileTableSeeder,
    breads\NationalityTableSeeder,
    breads\NewsCategoryTableSeeder,
    breads\NewsTableSeeder,
    breads\OrderCallTableSeeder,
    breads\OrderTableSeeder,
    breads\PharmacyTableSeeder,
    breads\PharmacyTabTableSeeder,
    breads\PharmacyTypeTableSeeder,
    breads\PostLinkTableSeeder,
    breads\ProfileTableSeeder,
    breads\ReviewTableSeeder,
    breads\ServiceLandingTableSeeder,
    breads\ServiceTableSeeder,
    breads\SpecialisationTableSeeder,
    breads\StockTableSeeder,
    breads\SymptomsTableSeeder,
    breads\UserBonusesTableSeeder,
    breads\UserTableSeeder,
    breads\MedicineCardTableSeeder,
    breads\VisitTableSeeder};
use Database\Seeders\data\fakes\AppointmentHistorySeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateVoyagerSeed();
        $this->callVoyagerBreadTables();
        $this->callVoyagerBasicTables();
        $this->callDataOriginals();
        $this->callDataFakes();
    }

    /**
     * Очистка старых данных для voyager.
     */
    private function truncateVoyagerSeed()
    {
        DB::table('menu_items')->truncate();
        DB::table('settings')->truncate();
        DB::table('permissions')->truncate();
        DB::table('permission_role')->truncate();
        DB::table('menus')->truncate();
        DB::table('data_rows')->truncate();
        DB::table('data_types')->truncate();
    }

    /**
     * Вызов объектов по namespace "\Database\Seeders\voyagers\breads"
     */
    private function callVoyagerBreadTables()
    {
        // for landings and just data
        $this->call(AdditionalDataSeeder::class);
        $this->call(HandbookSurveyQuestionTableSeeder::class);
        $this->call(AboutUsTableSeeder::class);
        $this->call(AboutUsAchievementTableSeeder::class);
        $this->call(AboutUsLicenseTableSeeder::class);
        $this->call(AboutUsSliderTableSeeder::class);
        $this->call(AboutUsBranchTableSeeder::class);
        $this->call(AboutUsWhyUsTableSeeder::class);
        $this->call(MailingTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(ContactTableSeeder::class);
        $this->call(ServiceTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(FaqTableSeeder::class);
        $this->call(PharmacyTypeTableSeeder::class);
        $this->call(PharmacyTabTableSeeder::class);
        $this->call(PharmacyTableSeeder::class);
        $this->call(MainTableSeeder::class);
        $this->call(MainSliderTableSeeder::class);
        $this->call(MainServiceTableSeeder::class);
        $this->call(MainServicePointTableSeeder::class);
        $this->call(MainPersonalAreaTableSeeder::class);
        $this->call(ServiceLandingTableSeeder::class);
//        $this->call(NationalityTableSeeder::class);
        $this->call(DoctorTableSeeder::class);
        $this->call(IconTableSeeder::class);
        $this->call(StockTableSeeder::class);
        $this->call(VisitTableSeeder::class);
        $this->call(ReviewTableSeeder::class);
        $this->call(HandbookBloodGroupTableSeeder::class);
        $this->call(SymptomsTableSeeder::class);
        $this->call(ProfileTableSeeder::class);
        $this->call(DoctorServiceTableSeeder::class);
        $this->call(FooterTableSeeder::class);
        $this->call(MainMenuTableSeeder::class);
        $this->call(BurgerMenuTableSeeder::class);
        $this->call(BonusTableSeeder::class);
        $this->call(OrderCallTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(SpecialisationTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(NewsCategoryTableSeeder::class);
        $this->call(AppointmentTableSeeder::class);
        $this->call(CanceledAppointmentTableSeeder::class);
        $this->call(UserBonusesTableSeeder::class);
        $this->call(HandbookOrderTypeTableSeeder::class);
        $this->call(HandbookOrderStatusTableSeeder::class);
        $this->call(OrderTableSeeder::class);
        $this->call(MedicineCardTableSeeder::class);
        $this->call(HistoryVisitsTableSeeder::class);
        $this->call(AnswerOnQuestionTableSeeder::class);
        $this->call(HistoryVisitPaymentTableSeeder::class);
        $this->call(HistoryVisitServiceTableSeeder::class);
        $this->call(PostLinkTableSeeder::class);
        $this->call(MedicineCardFileTableSeeder::class);
    }

    /**
     * Вызов объектов по namespace "Database\Seeders\voyagers\basis"
     */
    private function callVoyagerBasicTables()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(MenuItemsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PermissionRoleSeeder::class);
        $this->call(SettingsTableSeeder::class);
    }

    /**
     * Вызов объектов по namespace "\Database\Seeders\data\fakes"
     */
    private function callDataFakes()
    {
//        $this->call(\Database\Seeders\data\fakes\NationalitySeeder::class);
//        $this->call(AppointmentHistorySeeder::class);
//        $this->call(\Database\Seeders\data\fakes\ServicePointSeeder::class);
    }

    /**
     * Вызов объектов по namespace "\Database\Seeders\data\originals"
     */
    private function callDataOriginals()
    {
        $this->call(\Database\Seeders\data\originals\HandbookOrderTypeTableSeeder::class);
//        $this->call(data\originals\HandbookBloodGroupTableSeeder::class);
//        $this->call(data\originals\HandbookHealCategoryTableSeeder::class);
//        $this->call(data\originals\SpecialisationTableSeeder::class);
//        $this->call(data\originals\CountryTableSeeder::class);
//        $this->call(data\originals\ReceptionTypeSeeder::class);
//        $this->call(data\originals\FormOfParticipationSeeder::class);
    }
}
