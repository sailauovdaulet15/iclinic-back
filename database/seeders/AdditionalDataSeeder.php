<?php

namespace Database\Seeders;

use App\Enums\AboutUsEnum;
use App\Enums\MainEnum;
use App\Enums\ServicePointsCategoryEnum;
use App\Models\AboutUs;
use App\Models\Doctor;
use App\Models\Main;
use App\Models\ServicePointsCategory;
use App\Models\User;
use Illuminate\Database\Seeder;

class AdditionalDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (AboutUsEnum::getConstants() as $aboutUsKey) {
            $aboutUs = AboutUs::updateOrCreate(['key' => $aboutUsKey],[
                'title' => AboutUsEnum::getTitle($aboutUsKey),
                'position' => 1,
            ]);
        }

        foreach (MainEnum::getConstants() as $mainKey) {
            $main = Main::updateOrCreate(['type' => $mainKey], [
                'title' => MainEnum::getTitle($mainKey),
                'position' => 1,
            ]);
        }


        if (ServicePointsCategory::count() < 2) {
            $result = array();
            foreach (ServicePointsCategoryEnum::getConstants() as $type => $title) {
                $result[] = compact('type', 'title');
            }
            ServicePointsCategory::insert($result);
        }

        if (User::count() == 1) {
//            Doctor::factory()->create();
        }
    }
}
