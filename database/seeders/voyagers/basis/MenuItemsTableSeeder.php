<?php

namespace Database\Seeders\voyagers\basis;

use App\Models\AboutUs;
use App\Models\Main;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;

class MenuItemsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::where('name', 'admin')->firstOrFail();

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Главная страница',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-browser',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Основное',
            'url' => '',
            'route' => 'voyager.mains.index',
            'target' => '_self',
            'icon_class' => 'voyager-browser',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        foreach (Main::all() as $mainItem) {
            MenuItem::create([
                'menu_id' => $menu->id,
                'title' => $mainItem->title,
                'url' => '',
                'route' => "voyager.{$mainItem->dbTableName}.index",
                'target' => '_self',
                'icon_class' => 'voyager-browser',
                'color' => null,
                'parent_id' => $menuItemParent->id,
                'order' => 1,
            ]);
        }

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Ответы на вопросы',
            'route' => "voyager.answer_on_questions.index",
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-info-circled',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'О нас',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-info-circled',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'О нас',
            'route' => "voyager.about_us.index",
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-info-circled',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);


        foreach (AboutUs::all() as $aboutUsItem) {
            MenuItem::create([
                'menu_id' => $menu->id,
                'title' => $aboutUsItem->title,
                'url' => '',
                'route' => "voyager.{$aboutUsItem->dbTableName}.index",
                'target' => '_self',
                'icon_class' => 'voyager-info-circled',
                'color' => null,
                'parent_id' => $menuItemParent->id,
                'order' => 1,
            ]);
        }

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'О бонусах',
            'route' => 'voyager.bonuses.index',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-medal-rank-star',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Бонусы пользователя',
            'route' => "voyager.user_bonuses.index",
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-info-circled',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);


        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Приемы',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-info-circled',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Записи на прием',
            'url' => '',
            'route' => 'voyager.appointments.index',
            'target' => '_self',
            'icon_class' => 'voyager-location',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);


        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Отмененные записи',
            'url' => '',
            'route' => 'voyager.canceled_appointments.index',
            'target' => '_self',
            'icon_class' => 'voyager-location',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Лендинги услуг',
            'url' => '',
            'route' => 'voyager.service_landings.index',
            'target' => '_self',
            'icon_class' => 'voyager-location',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Медицинская карта',
            'url' => '',
            'route' => 'voyager.medicine_cards.index',
            'target' => '_self',
            'icon_class' => 'voyager-list',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);


        $menuItem = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'История посещений',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-list',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'История посещений',
            'url' => '',
            'route' => 'voyager.history_visits.index',
            'target' => '_self',
            'icon_class' => 'voyager-location',
            'color' => null,
            'parent_id' => $menuItem->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Оплаты истории посещений',
            'url' => '',
            'route' => 'voyager.history_visit_payments.index',
            'target' => '_self',
            'icon_class' => 'voyager-location',
            'color' => null,
            'parent_id' => $menuItem->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Услуги истории посещений',
            'url' => '',
            'route' => 'voyager.history_visit_services.index',
            'target' => '_self',
            'icon_class' => 'voyager-location',
            'color' => null,
            'parent_id' => $menuItem->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Услуги врачей',
            'url' => '',
            'route' => 'voyager.doctor_service.index',
            'target' => '_self',
            'icon_class' => 'voyager-location',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Пользователи',
            'url' => '',
            'route' => 'voyager.users.index',
            'target' => '_self',
            'icon_class' => 'voyager-location',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Заказать звонок',
            'url' => '',
            'route' => 'voyager.ordering_calls.index',
            'target' => '_self',
            'icon_class' => 'voyager-telephone',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Врачи',
            'url' => '',
            'route' => 'voyager.doctors.index',
            'target' => '_self',
            'icon_class' => 'voyager-location',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);


        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Аптеки',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-shop',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Основное',
            'url' => '',
            'route' => 'voyager.pharmacies.index',
            'target' => '_self',
            'icon_class' => 'voyager-shop',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Вкладки',
            'url' => '',
            'route' => 'voyager.pharmacy_tabs.index',
            'target' => '_self',
            'icon_class' => 'voyager-shop',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Типы товаров',
            'url' => '',
            'route' => 'voyager.pharmacy_types.index',
            'target' => '_self',
            'icon_class' => 'voyager-shop',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Услуги',
            'url' => '',
            'route' => 'voyager.services.index',
            'target' => '_self',
            'icon_class' => 'voyager-window-list',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Контакты',
            'url' => '',
            'route' => 'voyager.contacts.index',
            'target' => '_self',
            'icon_class' => 'voyager-world',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Новости',
            'url' => '',
            'route' => 'voyager.news.index',
            'target' => '_self',
            'icon_class' => 'voyager-news',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Рассылка',
            'url' => '',
            'route' => 'voyager.mailings.index',
            'target' => '_self',
            'icon_class' => 'voyager-mail',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Меню',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-ship',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Основное меню',
            'url' => '',
            'route' => 'voyager.main_menus.index',
            'target' => '_self',
            'icon_class' => 'voyager-ship',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Бургер меню',
            'url' => '',
            'route' => 'voyager.burger_menus.index',
            'target' => '_self',
            'icon_class' => 'voyager-ship',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);


        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Отзывы',
            'url' => '',
            'route' => 'voyager.reviews.index',
            'target' => '_self',
            'icon_class' => 'voyager-smile',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Футер',
            'url' => '',
            'route' => 'voyager.footers.index',
            'target' => '_self',
            'icon_class' => 'voyager-paper-plane',
            'color' => '',
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'FAQs',
            'url' => '',
            'route' => 'voyager.faqs.index',
            'target' => '_self',
            'icon_class' => 'voyager-pen',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Акции',
            'url' => '',
            'route' => 'voyager.stocks.index',
            'target' => '_self',
            'icon_class' => 'voyager-pen',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'График приема',
            'url' => '',
            'route' => 'voyager.visits.index',
            'target' => '_self',
            'icon_class' => 'voyager-pen',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Платежи',
            'url' => '',
            'route' => 'voyager.post_links.index',
            'target' => '_self',
            'icon_class' => 'voyager-buy',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Справочник',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-info-circled',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Способ оплаты',
            'url' => '',
            'route' => 'voyager.handbook_order_types.index',
            'target' => '_self',
            'icon_class' => 'voyager-buy',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Статус оплаты',
            'url' => '',
            'route' => 'voyager.handbook_order_statuses.index',
            'target' => '_self',
            'icon_class' => 'voyager-buy',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Специализации',
            'url' => '',
            'route' => 'voyager.specialisations.index',
            'target' => '_self',
            'icon_class' => 'voyager-droplet',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Страны',
            'url' => '',
            'route' => 'voyager.countries.index',
            'target' => '_self',
            'icon_class' => 'voyager-droplet',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Группа крови',
            'url' => '',
            'route' => 'voyager.handbook_blood_groups.index',
            'target' => '_self',
            'icon_class' => 'voyager-droplet',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Вопросы',
            'url' => '',
            'route' => 'voyager.handbook_survey_questions.index',
            'target' => '_self',
            'icon_class' => 'voyager-droplet',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Категории новостей',
            'url' => '',
            'route' => 'voyager.news_categories.index',
            'target' => '_self',
            'icon_class' => 'voyager-droplet',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);


        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Симптомы',
            'url' => '',
            'route' => 'voyager.symptoms.index',
            'target' => '_self',
            'icon_class' => 'voyager-eye',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Города',
            'url' => '',
            'route' => 'voyager.cities.index',
            'target' => '_self',
            'icon_class' => 'voyager-location',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        /*MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Национальности',
            'url' => '',
            'route' => 'voyager.nationalities.index',
            'target' => '_self',
            'icon_class' => 'voyager-location',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);*/

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Иконки',
            'url' => '',
            'route' => 'voyager.icons.index',
            'target' => '_self',
            'icon_class' => 'voyager-location',
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Tools',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-tools',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Menu Builder',
            'url' => '',
            'route' => 'voyager.menus.index',
            'target' => '_self',
            'icon_class' => 'voyager-list',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Database',
            'url' => '',
            'route' => 'voyager.database.index',
            'target' => '_self',
            'icon_class' => 'voyager-data',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Compass',
            'url' => '',
            'route' => 'voyager.compass.index',
            'target' => '_self',
            'icon_class' => 'voyager-compass',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Bread',
            'url' => '',
            'route' => 'voyager.bread.index',
            'target' => '_self',
            'icon_class' => 'voyager-bread',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        $menuItemParent = MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Settings',
            'url' => '',
            'target' => '_self',
            'icon_class' => 'voyager-tools',
            'color' => null,
            'parent_id' => null,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Project',
            'url' => '',
            'route' => 'voyager.settings.index',
            'target' => '_self',
            'icon_class' => 'voyager-settings',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Roles',
            'url' => '',
            'route' => 'voyager.roles.index',
            'target' => '_self',
            'icon_class' => 'voyager-lock',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);

        MenuItem::create([
            'menu_id' => $menu->id,
            'title' => 'Media',
            'url' => '',
            'route' => 'voyager.media.index',
            'target' => '_self',
            'icon_class' => 'voyager-images',
            'color' => null,
            'parent_id' => $menuItemParent->id,
            'order' => 1,
        ]);
    }
}
