<?php

namespace Database\Seeders\voyagers\breads;

use App\Enums\SexEnum;
use App\Models\Country;
use App\Models\HandbookBloodGroup;
use App\Models\HandbookHealCategory;
use App\Models\Nationality;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;

class UserTableSeeder extends Seeder
{
    private const TABLE_NAME = 'users';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = DataType::create([
            'slug' => self::TABLE_NAME,
            'name' => self::TABLE_NAME,
            'display_name_singular' => 'Пользователь',
            'display_name_plural' => 'Пользователи',
            'icon' => 'voyager-window-list',
            'model_name' => 'App\\Models\\User',
            'controller' => 'App\\Http\\Controllers\\Voyager\\VoyagerUserController',
            'server_side' => 1,
            'generate_permissions' => 1,
            'description' => '',
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'id',
            'type' => 'number',
            'display_name' => 'ID',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'name',
            'type' => 'text',
            'display_name' => 'ИИН',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'url' => 'edit',
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'avatar',
            'type' => 'image',
            'display_name' => 'Изображение',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'resize' => [
                    'width' => '1200',
                    'height' => 'null',
                ],
                'upsize' => true,
                'thumbnails' => [
                    [
                        'name' => 'medium',
                        'scale' => '50%',
                    ],
                    [
                        'name' => 'small',
                        'scale' => '25%',
                    ],
                    [
                        'name' => 'cropped',
                        'crop' => [
                            'width' => '300',
                            'height' => '300',
                        ],
                    ],
                ],
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'email',
            'type' => 'text',
            'display_name' => 'Почта',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'password',
            'type' => 'password',
            'display_name' => 'Пароль',
            'required' => 1,
            'browse' => 0,
            'read' => 0,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'role_id',
            'type' => 'number',
            'display_name' => 'role_id',
            'required' => 1,
            'browse' => 1,
            'read' => 0,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_role_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Роль',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => Role::class,
                'table' => 'roles',
                'type' => 'belongsTo',
                'column' => 'role_id',
                'key' => 'id',
                'label' => 'name',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'fullName',
            'type' => 'text',
            'display_name' => 'Полное имя',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'first_name',
            'type' => 'text',
            'display_name' => 'Имя',
            'required' => 1,
            'browse' => 0,
            'read' => 0,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'tab_title' => 'Дополнительная'
            ]
        ]);
        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'second_name',
            'type' => 'text',
            'display_name' => 'Фамилия',
            'required' => 1,
            'browse' => 0,
            'read' => 0,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);
        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'third_name',
            'type' => 'text',
            'display_name' => 'Отчество',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'nationality_id',
            'type' => 'number',
            'display_name' => 'Национальность',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_nationalities_relationship',
            'type' => 'relationship',
            'display_name' => 'Национальность',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => Nationality::class,
                'table' => 'countries',
                'type' => 'belongsTo',
                'column' => 'nationality_id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);



        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'handbook_blood_group_id',
            'type' => 'number',
            'display_name' => 'handbook_blood_group_id',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,

        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_handbook_blood_groups_relationship',
            'type' => 'relationship',
            'display_name' => 'Группа крови',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => HandbookBloodGroup::class,
                'table' => 'handbook_blood_groups',
                'type' => 'belongsTo',
                'column' => 'handbook_blood_group_id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'sex',
            'type' => 'select_dropdown',
            'display_name' => 'Пол',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'options' => SexEnum::getTitleWithKey(),
            ]
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'language',
            'type' => 'select_dropdown',
            'display_name' => 'Язык',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'default' => 'kz',
                'options' => [
                    'kz' => 'kz', 'en' => 'en', 'ru' => 'ru'
                ]
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'is_limited_by_mobility',
            'type' => 'checkbox',
            'display_name' => 'С ограниченными возможностями',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "on" => "Да",
                "off" => "Нет",
                "checked" => false
            ]
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'is_notification',
            'type' => 'checkbox',
            'display_name' => 'Уведомления',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "on" => "Да",
                "off" => "Нет",
                "checked" => false
            ]
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'bonus_count',
            'type' => 'number',
            'display_name' => 'Количество накопленных бонусов',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'general_discount',
            'type' => 'number',
            'display_name' => 'Скидка на все услуги',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'phone',
            'type' => 'number',
            'display_name' => 'Телефон',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'validation' => [
                    'rule' => 'digits_between:10,10|numeric',
                ],
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'date_birth',
            'type' => 'date',
            'display_name' => 'День рождения',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'legal_agent_id',
            'type' => 'number',
            'display_name' => 'legal_agent_id',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_legal_agent_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Ответственное лицо',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => 'App\\Models\\User',
                'table' => 'users',
                'type' => 'belongsTo',
                'column' => 'legal_agent_id',
                'key' => 'id',
                'label' => 'name',
            ],
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'country_id',
            'type' => 'number',
            'display_name' => 'Страна',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'tab_title' => 'Адрес'
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_countries_relationship',
            'type' => 'relationship',
            'display_name' => 'Страна',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => Country::class,
                'table' => 'countries',
                'type' => 'belongsTo',
                'column' => 'country_id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'region',
            'type' => 'text',
            'display_name' => 'Регион',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'city',
            'type' => 'text',
            'display_name' => 'Город',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'address',
            'type' => 'text',
            'display_name' => 'Адрес',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'postcode',
            'type' => 'text',
            'display_name' => 'Индекс',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'created_at',
            'type' => 'timestamp',
            'display_name' => 'Дата создания',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'updated_at',
            'type' => 'timestamp',
            'display_name' => 'Дата обновления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'deleted_at',
            'type' => 'timestamp',
            'display_name' => 'Дата удаления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        Permission::firstOrCreate(['key' => 'browse_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
        // todo delete after deploy
        Permission::firstOrCreate(['key' => 'delete_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
        Permission::firstOrCreate(['key' => 'read_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
        Permission::firstOrCreate(['key' => 'edit_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
        Permission::firstOrCreate(['key' => 'add_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
    }
}
