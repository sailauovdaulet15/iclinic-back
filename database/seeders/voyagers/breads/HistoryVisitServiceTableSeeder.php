<?php

namespace Database\Seeders\voyagers\breads;

use App\Models\HandbookOrderType;
use App\Models\HistoryVisit;
use App\Models\HistoryVisitPayment;
use App\Models\HistoryVisitService;
use App\Models\Service;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;

class HistoryVisitServiceTableSeeder extends Seeder
{
    private const TABLE_NAME = 'history_visit_services';

    public function run()
    {
        $dataType = DataType::create([
            'slug' => self::TABLE_NAME,
            'name' => self::TABLE_NAME,
            'display_name_singular' => 'Услуги истории посещений',
            'display_name_plural' => 'Услуги истории посещений',
            'icon' => 'voyager-location',
            'model_name' => HistoryVisitService::class,
            'controller' => '',
            'generate_permissions' => 1,
            'description' => '',
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'id',
            'type' => 'number',
            'display_name' => 'Id',
            'required' => 1,
            'browse' => 1,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'service_id',
            'type' => 'number',
            'display_name' => 'Услуга',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);
        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'service_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Услуга',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => Service::class,
                'table' => 'services',
                'type' => 'belongsTo',
                'column' => 'service_id',
                'key' => 'id',
                'label' => 'uid',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'service_id_relationship_label',
            'type' => 'relationship',
            'display_name' => 'Услуга',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'model' => Service::class,
                'table' => 'services',
                'type' => 'belongsTo',
                'column' => 'service_id',
                'key' => 'id',
                'label' => 'name',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'amount',
            'type' => 'number',
            'display_name' => 'Количество',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'sum',
            'type' => 'number',
            'display_name' => 'Сумма',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'history_visit_id',
            'type' => 'number',
            'display_name' => 'История посещения',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);
        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'history_visit_id_relationship',
            'type' => 'relationship',
            'display_name' => 'История посещения',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => HistoryVisit::class,
                'table' => 'history_visits',
                'type' => 'belongsTo',
                'column' => 'history_visit_id',
                'key' => 'id',
                'label' => 'uid',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ],
        ]);


//        DataRow::create([
//            'data_type_id' => $dataType->id,
//            'field' => 'payment_type',
//            'type' => 'text',
//            'display_name' => 'Тип оплаты',
//            'required' => 1,
//            'browse' => 1,
//            'read' => 1,
//            'edit' => 1,
//            'add' => 1,
//            'delete' => 1,
//        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'payment_sum',
            'type' => 'text',
            'display_name' => 'Сумма оплаты',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'created_at',
            'type' => 'timestamp',
            'display_name' => 'Дата создания',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'updated_at',
            'type' => 'timestamp',
            'display_name' => 'Дата обновления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'deleted_at',
            'type' => 'timestamp',
            'display_name' => 'Дата удаления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        Permission::generateFor(self::TABLE_NAME);
    }
}
