<?php

namespace Database\Seeders\voyagers\breads;

use App\Models\ReceptionType;
use App\Models\RecommendService;
use App\Models\Service;
use App\Models\Specialisation;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;

class ServiceTableSeeder extends Seeder
{
    private const TABLE_NAME = 'services';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = DataType::create([
            'slug' => self::TABLE_NAME,
            'name' => self::TABLE_NAME,
            'display_name_singular' => 'Услуга',
            'display_name_plural' => 'Услуги',
            'icon' => 'voyager-window-list',
            'model_name' => 'App\\Models\\Admin\\AdminService',
            'controller' => '',
            'generate_permissions' => 1,
            'server_side' => 1,
            'description' => '',
            'details' => [
                'order_column' => 'position',
                'order_direction' => 'asc',
                'order_display_column' => 'title',
                "browse_tree" => true,
                "browse_tree_push_right" => true
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'id',
            'type' => 'number',
            'display_name' => 'Id',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'uid',
            'type' => 'text',
            'display_name' => 'UID',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'title',
            'type' => 'text',
            'display_name' => 'Заголовок',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'slug',
            'type' => 'text',
            'display_name' => 'Slug',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'slugify' => [
                    'origin' => 'title',
                    "forceUpdate" => true
                ],
                'validation' => [
                    'rule' => 'unique:services,slug',
                ],
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'annotation',
            'type' => 'text',
            'display_name' => 'Аннотация',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'description',
            'type' => 'rich_text_box',
            'display_name' => 'Об услуге',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'link',
            'type' => 'adv_json',
            'display_name' => 'Ссылка',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "json_fields" => [
                    "value" => "Youtube ссылка"
                ]
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'specialisation_id',
            'type' => 'number',
            'display_name' => 'specialisation_id',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_specialisation_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Специализация',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => Specialisation::class,
                'table' => 'specialisations',
                'type' => 'belongsTo',
                'column' => 'specialisation_id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'reception_type_id',
            'type' => 'number',
            'display_name' => 'reception_type_id',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_reception_type_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Вид Приема',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => ReceptionType::class,
                'table' => 'reception_types',
                'type' => 'belongsTo',
                'column' => 'reception_type_id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);


//        DataRow::create([
//            'data_type_id' => $dataType->id,
//            'field' => 'parent_id',
//            'type' => 'select_dropdown',
//            'display_name' => 'Родная услуга',
//            'required' => 0,
//            'browse' => 1,
//            'read' => 1,
//            'edit' => 1,
//            'add' => 1,
//            'delete' => 1,
//            'details' => [
//                'browse_tree' => true,
//                'default' => '',
//                'null' => '',
//                'options' => [
//                    '' => '-- Не выбрано --',
//                ],
//                'relationship' => [
//                    'key' => 'id',
//                    'label' => 'title',
//                ],
//            ]
//        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'services_belongs_to_many_recommend_services_relationship',
            'type' => 'relationship',
            'display_name' => 'Дополнительные услуги',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 0,
            'details' => [
                'model' => RecommendService::class,
                'table' => 'services',
                'type' => 'belongsToMany',
                'column' => 'service_id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'recommend_services',
                'pivot' => true,
                'taggable' => true,
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'is_visible',
            'type' => 'checkbox',
            'display_name' => 'Показать на сайте',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "on" => "Да",
                "off" => "Нет",
                "checked" => false
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'is_online',
            'type' => 'checkbox',
            'display_name' => 'Онлайн оплата',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "on" => "Да",
                "off" => "Нет",
                "checked" => false
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'price_original',
            'type' => 'number',
            'display_name' => 'Цена оригинальная',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'tab_title' => 'Цены'
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'price_percent',
            'type' => 'number',
            'display_name' => 'Процент скидки',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'min' => 0,
                'max' => 100,
                "validation" => [
                    "rule" => "min:0|max:100"
                ]
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'price_bonus',
            'type' => 'number',
            'display_name' => 'Размеров бонусов (в %)',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'price_with_discount',
            'type' => 'number',
            'display_name' => 'Цена со скидкой',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);


//        DataRow::create([
//            'data_type_id' => $dataType->id,
//            'field' => 'parent_id_relationship',
//            'type' => 'relationship',
//            'display_name' => 'Родная услуга',
//            'required' => 0,
//            'browse' => 1,
//            'read' => 1,
//            'edit' => 1,
//            'add' => 1,
//            'delete' => 1,
//            'details' => [
//                'model' => 'App\\Models\\Service',
//                'table' => 'services',
//                'type' => 'belongsTo',
//                'column' => 'parent_id',
//                'key' => 'id',
//                'label' => 'title',
//                'pivot_table' => 'migrations',
//                'pivot' => '0',
//                'taggable' => null,
//                'browse_tree' => true
//            ],
//        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'position',
            'type' => 'number',
            'display_name' => 'Позиция',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 1,
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'created_at',
            'type' => 'timestamp',
            'display_name' => 'Дата создания',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'updated_at',
            'type' => 'timestamp',
            'display_name' => 'Дата обновления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);


        Permission::generateFor(self::TABLE_NAME);
    }
}
