<?php

namespace Database\Seeders\voyagers\breads;

use App\Enums\MainEnum;
use App\Enums\ServicePointsCategoryEnum;
use App\Models\Main;
use App\Models\ServicePointsCategory;
use App\Models\Specialisation;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;

class MainServicePointTableSeeder extends Seeder
{
    private const TABLE_NAME = 'main_service_points';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mainKey = MainEnum::SERVICE_POINTS;
        $mainServicePoints = Main::firstOrNew(['type' => $mainKey]);

        if (!$mainServicePoints->exists) {
            $mainServicePoints->fill([
                'title' => MainEnum::getTitle($mainKey),
                'position' => 1,
            ])->save();
        }



        $dataType = DataType::create([
            'slug' => self::TABLE_NAME,
            'name' => self::TABLE_NAME,
            'display_name_singular' => 'Точка услуги',
            'display_name_plural' => 'Точки услуг',
            'icon' => 'voyager-browser',
            'model_name' => 'App\\Models\\MainServicePoint',
            'controller' => '',
            'generate_permissions' => 0,
            'description' => '',
            'details' => [
                'order_column' => 'position',
                'order_direction' => 'asc',
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'id',
            'type' => 'number',
            'display_name' => 'Id',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'position',
            'type' => 'number',
            'display_name' => 'Позиция',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'key',
            'type' => 'text',
            'display_name' => 'Ключ точки на главной странице',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'description',
            'type' => 'text_area',
            'display_name' => 'Описание',
            'required' => 1,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'category_id',
            'type' => 'number',
            'display_name' => 'category_id',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'category_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Категория',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'model' => 'App\\Models\\ServicePointsCategory',
                'table' => 'service_points_categories',
                'type' => 'belongsTo',
                'column' => 'category_id',
                'key' => 'id',
                'label' => 'title',
                'pivot' => '0',
                'taggable' => false,
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'specialisation_id',
            'type' => 'number',
            'display_name' => 'specialisation_id',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_specialisation_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Специализация',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => Specialisation::class,
                'table' => 'specialisations',
                'type' => 'belongsTo',
                'column' => 'specialisation_id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
                'scope' => 'hasLanding'
            ]
        ]);




        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'created_at',
            'type' => 'timestamp',
            'display_name' => 'Дата создания',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'updated_at',
            'type' => 'timestamp',
            'display_name' => 'Дата обновления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'deleted_at',
            'type' => 'timestamp',
            'display_name' => 'Дата удаления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        Permission::firstOrCreate(['key' => 'browse_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
        Permission::firstOrCreate(['key' => 'read_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
        Permission::firstOrCreate(['key' => 'edit_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
    }
}
