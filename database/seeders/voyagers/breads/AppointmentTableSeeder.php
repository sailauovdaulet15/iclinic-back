<?php

namespace Database\Seeders\voyagers\breads;

use App\Models\User;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;

class AppointmentTableSeeder extends Seeder
{
    private const TABLE_NAME = 'appointments';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = DataType::create([
            'slug' => self::TABLE_NAME,
            'name' => self::TABLE_NAME,
            'display_name_singular' => 'Запись на прием',
            'display_name_plural' => 'Записи на прием',
            'icon' => 'voyager-window-list',
            'model_name' => 'App\\Models\\Appointment',
            'controller' => '',
            'server_side' => 1,
            'generate_permissions' => 1,
            'description' => '',
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'id',
            'type' => 'number',
            'display_name' => 'Id',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'uid',
            'type' => 'text',
            'display_name' => 'UID',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'cabinet_num',
            'type' => 'number',
            'display_name' => 'Номер кабинета',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'is_online_payment',
            'type' => 'checkbox',
            'display_name' => 'Оплачено онлайн',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'isOnlineConsultation',
            'type' => 'checkbox',
            'display_name' => 'Онлайн Консультация',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);
        
        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'patient_id',
            'type' => 'number',
            'display_name' => 'patient_id',
            'required' => 1,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_patient_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Пациент',
            'required' => 1,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => User::class,
                'table' => 'users',
                'type' => 'belongsTo',
                'column' => 'patient_id',
                'key' => 'id',
                'label' => 'full_name',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'patientFullName',
            'type' => 'text',
            'display_name' => 'Пациент',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'url' => 'show',
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'doctor_user_id',
            'type' => 'number',
            'display_name' => 'doctor_user_id',
            'required' => 1,
            'browse' => 0,
            'read' => 0,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_doctor_user_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Врач',
            'required' => 1,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => User::class,
                'table' => 'users',
                'type' => 'belongsTo',
                'column' => 'doctor_user_id',
                'key' => 'id',
                'label' => 'full_name',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
                'scope' => 'hasDoctorProfile',
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'doctorFullName',
            'type' => 'text',
            'display_name' => 'Врач',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'url' => 'show',
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'status',
            'type' => 'number',
            'display_name' => 'Статус',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'date',
            'type' => 'date',
            'display_name' => 'День приема',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'start_time',
            'type' => 'time',
            'display_name' => 'Время начала приема',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'end_time',
            'type' => 'time',
            'display_name' => 'Время окончания приема',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'appointments_belongstomany_appointment_service_relationship',
            'type' => 'relationship',
            'display_name' => 'Услуги',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => 'App\\Models\\Service',
                'table' => 'services',
                'type' => 'belongsToMany',
                'column' => 'id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'appointment_service',
                'pivot' => '1',
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'createdAtTime',
            'type' => 'timestamp',
            'display_name' => 'Дата создания заявки',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'updated_at',
            'type' => 'timestamp',
            'display_name' => 'Дата обновления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);


        Permission::generateFor(self::TABLE_NAME);
    }
}
