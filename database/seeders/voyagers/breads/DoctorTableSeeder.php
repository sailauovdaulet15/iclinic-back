<?php

namespace Database\Seeders\voyagers\breads;

use App\Http\Controllers\Voyager\VoyagerDoctorController;
use App\Models\HandbookHealCategory;
use App\Models\Specialisation;
use App\Models\User;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;

class DoctorTableSeeder extends Seeder
{
    private const TABLE_NAME = 'doctors';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = DataType::create([
            'slug' => self::TABLE_NAME,
            'name' => self::TABLE_NAME,
            'display_name_singular' => 'Врач',
            'display_name_plural' => 'Врачи',
            'icon' => 'voyager-window-list',
            'model_name' => 'App\\Models\\Admin\\AdminDoctor',
            'controller' => VoyagerDoctorController::class,
            'server_side' => 1,
            'generate_permissions' => 1,
            'description' => '',
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'user_id',
            'type' => 'number',
            'display_name' => 'ID',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_user_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Пользователь(Пользователь с Ролью врача)',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => User::class,
                'table' => 'users',
                'type' => 'belongsTo',
                'column' => 'user_id',
                'key' => 'id',
                'label' => 'name',
                'scope' => 'doctorToCreate',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'userName',
            'type' => 'text',
            'display_name' => 'ИИН',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'url' => 'edit',
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'firstSecondName',
            'type' => 'text',
            'display_name' => 'Фамилия Имя',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'userSlug',
            'type' => 'text',
            'display_name' => 'Slug',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'image_uri',
            'type' => 'image',
            'display_name' => 'Изображение',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'resize' => [
                    'width' => '1200',
                    'height' => 'null',
                ],
                'upsize' => true,
                'thumbnails' => [
                    [
                        'name' => 'medium',
                        'scale' => '50%',
                    ],
                    [
                        'name' => 'small',
                        'scale' => '25%',
                    ],
                    [
                        'name' => 'cropped',
                        'crop' => [
                            'width' => '300',
                            'height' => '300',
                        ],
                    ],
                ],
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'about',
            'type' => 'text_area',
            'display_name' => 'Про врача',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'validation' => [
                    'rule' => 'max:500',
                ],
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'about2',
            'type' => 'text_area',
            'display_name' => 'Про врача 2',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'validation' => [
                    'rule' => 'max:500',
                ],
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'category',
            'type' => 'text',
            'display_name' => 'Категория',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'handbook_heal_category_id',
            'type' => 'number',
            'display_name' => 'Лечит',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "browse_width"=> "15px",
                "browse_align" => "right",
                "browse_font_size" => "0.8em",
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_handbook_heal_category_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Лечит',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "browse_width"=> "15px",
                "browse_align" => "right",
                "browse_font_size" => "0.8em",
                'model' => HandbookHealCategory::class,
                'table' => 'handbook_heal_categories',
                'type' => 'belongsTo',
                'column' => 'handbook_heal_category_id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'duration',
            'type' => 'number',
            'display_name' => 'Продолжительность(в минутах)',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'position',
            'type' => 'number',
            'display_name' => 'Позиция',
            'required' => 1,
            'browse' => 1,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'experience',
            'type' => 'text',
            'display_name' => 'Стаж',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'link',
            'type' => 'adv_json',
            'display_name' => 'Ссылки на видео',
            'required' => 1,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "json_fields" => [
                    "value" => "Ссылки"
                ]
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'work_experience',
            'type' => 'adv_json',
            'display_name' => 'Опыт работы',
            'required' => 1,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "json_fields" => [
                    "years" => "Года работы",
                    "description" => "Описание"
                ]
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'education_experience',
            'type' => 'adv_json',
            'display_name' => 'Образование',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "json_fields" => [
                    "years" => "Года учебы",
                    "description" => "Описание"
                ]
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'doctor_belongs_to_many_specialisations_relationship',
            'type' => 'relationship',
            'display_name' => 'Специализации',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'model' => 'App\\Models\\Specialisation',
                'table' => 'specialisations',
                'type' => 'belongsToMany',
                'column' => 'id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'doctor_specialisation',
                'pivot' => '1',
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'doctor_belongs_to_many_services_relationship',
            'type' => 'relationship',
            'display_name' => 'Услуги врача',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'model' => 'App\\Models\\Service',
                'table' => 'services',
                'type' => 'belongsToMany',
                'column' => 'id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'doctor_service',
                'pivot' => '1',
                'taggable' => '1',
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'doctor_belongs_to_many_recommend_services_relationship',
            'type' => 'relationship',
            'display_name' => 'Дополнительные услуги',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'model' => 'App\\Models\\RecommendService',
                'table' => 'services',
                'type' => 'belongsToMany',
                'column' => 'id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'doctor_recommend_service',
                'pivot' => '1',
                'taggable' => '1',
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'doctor_has_many_reviews_relationship',
            'type' => 'relationship',
            'display_name' => 'Отзывы',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'model' => 'App\\Models\\Review',
                'table' => 'reviews',
                'type' => 'hasMany',
                'column' => 'doctor_user_id',
                'key' => 'user_id',
                'label' => 'name',
            ],
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'is_visible',
            'type' => 'checkbox',
            'display_name' => 'Показать на сайте',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "on" => "Да",
                "off" => "Нет",
                "checked" => false
            ]
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'created_at',
            'type' => 'timestamp',
            'display_name' => 'Дата создания',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'updated_at',
            'type' => 'timestamp',
            'display_name' => 'Дата обновления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);


        Permission::firstOrCreate(['key' => 'browse_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
        Permission::firstOrCreate(['key' => 'read_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
        Permission::firstOrCreate(['key' => 'edit_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
        Permission::firstOrCreate(['key' => 'add_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
    }
}
