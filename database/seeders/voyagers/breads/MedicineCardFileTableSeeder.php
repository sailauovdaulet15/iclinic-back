<?php

namespace Database\Seeders\voyagers\breads;

use App\Models\HistoryVisitService;
use App\Models\MedicineCard;
use App\Models\MedicineCardFile;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;

class MedicineCardFileTableSeeder extends Seeder
{
    private const TABLE_NAME = 'medicine_card_files';

    public function run()
    {
        $dataType = DataType::create([
            'slug' => self::TABLE_NAME,
            'name' => self::TABLE_NAME,
            'display_name_singular' => 'Файл Медицинской карты',
            'display_name_plural' => 'Файлы Медицинской карты',
            'icon' => 'voyager-location',
            'model_name' => MedicineCardFile::class,
            'controller' => '',
            'generate_permissions' => 1,
            'description' => '',
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'id',
            'type' => 'number',
            'display_name' => 'Id',
            'required' => 1,
            'browse' => 1,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'file_name',
            'type' => 'text',
            'display_name' => 'Имя Шаблона',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'file_path',
            'type' => 'file',
            'display_name' => 'Файл',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'medicine_card_id',
            'type' => 'number',
            'display_name' => 'Медицинская карта',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);
        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'medicine_card_file_belongs_to_medicine_card_id',
            'type' => 'relationship',
            'display_name' => 'Медицинская карта',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => MedicineCard::class,
                'table' => 'medicine_cards',
                'type' => 'belongsTo',
                'column' => 'medicine_card_id',
                'key' => 'id',
                'label' => 'id',
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'is_read',
            'type' => 'checkbox',
            'display_name' => 'Прочитано(не прочитано)',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "on" => "Да",
                "off" => "Нет",
                "checked" => false
            ]
        ]);





        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'created_at',
            'type' => 'timestamp',
            'display_name' => 'Дата создания',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'updated_at',
            'type' => 'timestamp',
            'display_name' => 'Дата обновления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'deleted_at',
            'type' => 'timestamp',
            'display_name' => 'Дата удаления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        Permission::generateFor(self::TABLE_NAME);
    }
}
