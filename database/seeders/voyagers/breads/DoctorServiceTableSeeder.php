<?php

namespace Database\Seeders\voyagers\breads;

use App\Enums\StatusEnum;
use App\Models\Doctor;
use App\Models\FormOfParticipation;
use App\Models\RecommendService;
use App\Models\Service;
use App\Models\User;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;

class DoctorServiceTableSeeder extends Seeder
{
    private const TABLE_NAME = 'doctor_service';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = DataType::create([
            'slug' => self::TABLE_NAME,
            'name' => self::TABLE_NAME,
            'display_name_singular' => 'Услуга врача',
            'display_name_plural' => 'Услуги врачей',
            'icon' => 'voyager-window-list',
            'model_name' => 'App\\Models\\DoctorService',
            'controller' => '',
            'generate_permissions' => 1,
            'description' => '',
            'details' => [
                'order_column' => 'id',
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'id',
            'type' => 'number',
            'display_name' => 'Id',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'doctor_user_id',
            'type' => 'number',
            'display_name' => 'doctor_user_id',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_doctor_user_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Врач',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => Doctor::class,
                'table' => 'doctors',
                'type' => 'belongsTo',
                'column' => 'doctor_user_id',
                'key' => 'user_id',
                'label' => 'firstSecondName',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'service_id',
            'type' => 'number',
            'display_name' => 'service_id',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_service_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Услуга',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => Service::class,
                'table' => 'services',
                'type' => 'belongsTo',
                'column' => 'service_id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_form_of_participation_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Форма участия',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => FormOfParticipation::class,
                'table' => 'form_of_participations',
                'type' => 'belongsTo',
                'column' => 'form_of_participation_id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'service_percent',
            'type' => 'number',
            'display_name' => '% от услуги',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'status',
            'type' => 'select_dropdown',
            'display_name' => 'Статус',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'options' => StatusEnum::getConstants()
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'start_date',
            'type' => 'date',
            'display_name' => "Дата начала действия (оказания услуги)",
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'sum_of',
            'type' => 'number',
            'display_name' => 'Сумма',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'is_exclusive',
            'type' => 'checkbox',
            'display_name' => 'Эклюзивная услуга',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "on" => "Да",
                "off" => "Нет",
                "checked" => false
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'created_at',
            'type' => 'timestamp',
            'display_name' => 'Дата создания',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'updated_at',
            'type' => 'timestamp',
            'display_name' => 'Дата обновления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        Permission::firstOrCreate(['key' => 'browse_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
        Permission::firstOrCreate(['key' => 'read_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
    }
}
