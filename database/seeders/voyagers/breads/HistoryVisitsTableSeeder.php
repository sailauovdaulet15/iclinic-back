<?php

namespace Database\Seeders\voyagers\breads;

use App\Models\Doctor;
use App\Models\User;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;

class HistoryVisitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private const TABLE_NAME = 'history_visits';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = DataType::create([
            'slug' => self::TABLE_NAME,
            'name' => self::TABLE_NAME,
            'display_name_singular' => 'История посещений',
            'display_name_plural' => 'Истории посещений',
            'icon' => '',
            'model_name' => 'App\\Models\\HistoryVisit',
            'controller' => '',
            'generate_permissions' => 1,
            'description' => '',
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'id',
            'type' => 'number',
            'display_name' => 'ID',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'uid',
            'type' => 'text',
            'display_name' => 'UID',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'user_id',
            'type' => 'number',
            'display_name' => 'user',
            'required' => 1,
            'browse' => 1,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_user_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Имя',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'model' => User::class,
                'table' => 'users',
                'type' => 'belongsTo',
                'column' => 'user_id',
                'key' => 'id',
                'label' => 'full_name',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'doctor_user_id',
            'type' => 'number',
            'display_name' => 'doctor_user_id',
            'required' => 1,
            'browse' => 1,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_doctor_user_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Врач',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'model' => Doctor::class,
                'table' => 'doctors',
                'type' => 'belongsTo',
                'column' => 'doctor_user_id',
                'key' => 'user_id',
                'label' => 'firstSecondName',
                'pivot_table' => 'migrations',
                'pivot' => '0',
                'taggable' => null,
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'history_visit_belongstomany_history_visit_service_relationship',
            'type' => 'relationship',
            'display_name' => 'Услуги',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'model' => 'App\\Models\\Service',
                'table' => 'services',
                'type' => 'belongsToMany',
                'column' => 'id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'history_visit_service',
                'pivot' => '1',
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'date',
            'type' => 'timestamp',
            'display_name' => 'Дата оказания услуги',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'room',
            'type' => 'text',
            'display_name' => 'Кабинет',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'date_start',
            'type' => 'timestamp',
            'display_name' => 'Дата и время начала приема',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'href',
            'type' => 'text',
            'display_name' => 'Оплаты',
            'required' => 0,
            'browse' => 1,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'route' => [
                    'name' => "voyager.history_visit_payments.index",
                    'param_field' => "hrefParam",
                ],
            ]
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'href',
            'type' => 'text',
            'display_name' => 'Услуги',
            'required' => 0,
            'browse' => 1,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
            'details' => [
                'route' => [
                    'name' => "voyager.history_visit_services.index",
                    'param_field' => "hrefParam",
                ],
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'created_at',
            'type' => 'timestamp',
            'display_name' => 'Дата создания',
            'required' => 0,
            'browse' => 1,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'updated_at',
            'type' => 'timestamp',
            'display_name' => 'Дата обновления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        Permission::firstOrCreate(['key' => 'browse_' . self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
        Permission::firstOrCreate(['key' => 'read_' . self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
    }
}
