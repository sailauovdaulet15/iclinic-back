<?php

namespace Database\Seeders\voyagers\breads;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;

class ServiceLandingTableSeeder extends Seeder
{
    private const TABLE_NAME = 'service_landings';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = DataType::create([
            'slug' => self::TABLE_NAME,
            'name' => self::TABLE_NAME,
            'display_name_singular' => 'Лендинг Услуги',
            'display_name_plural' => 'Лендинги Услуг',
            'icon' => 'voyager-window-list',
            'model_name' => 'App\\Models\\ServiceLanding',
            'controller' => '',
            'generate_permissions' => 1,
            'description' => '',
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'id',
            'type' => 'number',
            'display_name' => 'Id',
            'required' => 1,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'title',
            'type' => 'text',
            'display_name' => 'Заголовок',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'annotation',
            'type' => 'text_area',
            'display_name' => 'Аннотация',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'specialisation_count_details',
            'type' => 'adv_json',
            'display_name' => 'Детали подсчета специализаций',
            'required' => 1,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                "json_fields" => [
                    'description' => 'Описание',
                    "number" => "Цифры",
                ]
            ]
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'slug',
            'type' => 'text',
            'display_name' => 'Slug',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'slugify' => [
                    'origin' => 'title',
                    "forceUpdate" => true
                ],
                'validation' => [
                    'rule' => 'unique:service_landings,slug',
                ],
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'specialisation_id',
            'type' => 'number',
            'display_name' => 'specialisation_id',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'belongs_to_specialisation_id_relationship',
            'type' => 'relationship',
            'display_name' => 'Специализация',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => 'App\\Models\\Specialisation',
                'table' => 'specialisations',
                'type' => 'belongsTo',
                'column' => 'specialisation_id',
                'key' => 'id',
                'label' => 'title',
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'morphed_by_many_review_id_relationship',
            'type' => 'polymorphic',
            'display_name' => 'Отзывы',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => 'App\\Models\\Review',
                'table' => 'reviews',
                'type' => 'morphedByMany',
                'column' => 'service_landing_id',
                'key' => 'id',
                'label' => 'name',
                'pivot_table' => 'service_landingables',
                'pivot_name' => 'service_landingable',
                'pivot_type_column' => 'service_landingable_type',
                'pivot_column' => 'service_landingable_id',
                'pivot' => true,
                'taggable' => true,
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'morphed_by_many_service_id_relationship',
            'type' => 'polymorphic',
            'display_name' => 'Рекомендуемые Услуги',
            'required' => 0,
            'browse' => 0,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'model' => 'App\\Models\\Service',
                'table' => 'services',
                'type' => 'morphedByMany',
                'column' => 'service_landing_id',
                'key' => 'id',
                'label' => 'title',
                'pivot_table' => 'service_landingables',
                'pivot_name' => 'service_landingable',
                'pivot_type_column' => 'service_landingable_type',
                'pivot_column' => 'service_landingable_id',
                'pivot' => true,
                'taggable' => true,
            ],
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'image_uri',
            'type' => 'image',
            'display_name' => 'Изображение',
            'required' => 1,
            'browse' => 1,
            'read' => 1,
            'edit' => 1,
            'add' => 1,
            'delete' => 1,
            'details' => [
                'resize' => [
                    'width' => '1200',
                    'height' => 'null',
                ],
                'quality' => '70%',
                'upsize' => true,
                'thumbnails' => [
                    [
                        'name' => 'medium',
                        'scale' => '50%',
                    ],
                    [
                        'name' => 'small',
                        'scale' => '25%',
                    ],
                    [
                        'name' => 'cropped',
                        'crop' => [
                            'width' => '300',
                            'height' => '300',
                        ],
                    ],
                ],
            ],
        ]);


        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'created_at',
            'type' => 'timestamp',
            'display_name' => 'Дата создания',
            'required' => 0,
            'browse' => 1,
            'read' => 1,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'updated_at',
            'type' => 'timestamp',
            'display_name' => 'Дата обновления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        DataRow::create([
            'data_type_id' => $dataType->id,
            'field' => 'deleted_at',
            'type' => 'timestamp',
            'display_name' => 'Дата удаления',
            'required' => 0,
            'browse' => 0,
            'read' => 0,
            'edit' => 0,
            'add' => 0,
            'delete' => 0,
        ]);

        Permission::firstOrCreate(['key' => 'browse_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
        Permission::firstOrCreate(['key' => 'read_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
        Permission::firstOrCreate(['key' => 'edit_'.self::TABLE_NAME, 'table_name' => self::TABLE_NAME]);
    }
}
