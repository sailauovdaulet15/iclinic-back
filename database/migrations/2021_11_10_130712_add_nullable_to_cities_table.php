<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\Type;
use TCG\Voyager\Database\Types\Postgresql\JsonbType;

class AddNullableToCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws \Doctrine\DBAL\Exception
     */
    public function up()
    {
        if (!Type::hasType('jsonb')) {
            Type::addType('jsonb', JsonbType::class);
        }
        Schema::table('cities', function (Blueprint $table) {
            $table->jsonb('phones')->nullable()->comment('Телефоны')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function (Blueprint $table) {
            //
        });
    }
}
