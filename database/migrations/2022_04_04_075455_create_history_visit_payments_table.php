<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryVisitPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_visit_payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('history_visit_id')->nullable()->constrained('history_visits');
            $table->foreignId('service_id')->nullable()->constrained('services');
            $table->integer('amount')->nullable();
            $table->integer('sum')->nullable();
            $table->foreignId('handbook_order_type_id')->nullable()->constrained('handbook_order_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_visit_payments');
    }
}
