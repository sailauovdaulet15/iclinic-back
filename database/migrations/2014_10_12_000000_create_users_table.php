<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('email')->nullable()->unique();
            $table->string('phone')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');

            // profile
            $table->string('first_name')->nullable();
            $table->string('second_name')->nullable();
            $table->string('third_name')->nullable();

            $table->foreignId('nationality_id')->nullable()->constrained();
            $table->tinyText('sex')->nullable();
            $table->tinyText('language')->nullable();
            $table->boolean('is_limited_by_mobility')->default(false);

            $table->string('postcode')->nullable();
            $table->string('phone2')->nullable();
            $table->string('address')->nullable();
            $table->date('date_birth')->nullable();
            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->string('city')->nullable();
            $table->integer('legal_agent_id')->nullable();


            $table->integer('general_discount')->nullable()->comment('Скидка на все услуги');
            $table->integer('bonus_count')->nullable();
            $table->boolean('is_notification')->default(false);
            $table->boolean('is_push_notification')->default(false);

            $table->foreignId('handbook_blood_group_id')->nullable()->constrained();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
