<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kalnoy\Nestedset\NestedSet;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('annotation')->nullable();
            $table->string('description')->nullable();
            $table->jsonb('link')->nullable();
            $table->integer('price_original')->nullable();
            $table->integer('price_percent')->nullable();
            $table->integer('price_bonus')->nullable();
            $table->integer('position')->default(0)->comment('Позиция');
            $table->integer('order')->default(0)->comment('Сортировка');
            $table->boolean('is_visible')->default(false);
            NestedSet::columns($table);

            $table->foreignId('specialisation_id')->constrained()->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
