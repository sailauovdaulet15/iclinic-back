<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->string('image_uri')->nullable();
            $table->string('card_color')->nullable();
            $table->foreignId('first_icon_id')->references('id')->on('icons');
            $table->foreignId('second_icon_id')->references('id')->on('icons');
            $table->timestamp('start_time');
            $table->timestamp('end_time');
            $table->integer('position');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
