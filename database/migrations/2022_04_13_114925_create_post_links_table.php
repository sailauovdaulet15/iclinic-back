<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_links', function (Blueprint $table) {
            $table->id();
            $table->string('amount')->nullable();
            $table->string('currency')->nullable();
            $table->string('reference')->nullable();
            $table->string('approvalСode')->nullable();
            $table->integer('code')->nullable();
            $table->string('secure')->nullable();
            $table->string('accountId')->nullable();
            $table->string('invoiceId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_links');
    }
}
