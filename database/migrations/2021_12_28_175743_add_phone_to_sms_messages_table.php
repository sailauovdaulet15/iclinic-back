<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddPhoneToSmsMessagesTable extends Migration
{
    private const TABLE_NAME = 'sms_messages';
    private const SQLITE_DRIVER_NAME = 'sqlite';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->string('phone');
        });
            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropConstrainedForeignId('user_id');
            if (DB::getDriverName() !== self::SQLITE_DRIVER_NAME) {
                $table->index(['phone', 'created_at']);
            }
            $table->unique('sms_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropColumn('phone');
            if (DB::getDriverName() !== self::SQLITE_DRIVER_NAME) {
                $table->integer('user_id');
            } else {
                $table->integer('user_id')->nullable();
            }
        });
    }
}
