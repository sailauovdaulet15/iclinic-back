<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOnDeleteNullToMainServicePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('main_service_points', function (Blueprint $table) {
            $table->dropForeign('main_service_points_specialisation_id_foreign');
            $table->foreign('specialisation_id')->on('specialisations')->references('id')->nullOnDelete();
            $table->integer('specialisation_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('main_service_points', function (Blueprint $table) {
            //
        });
    }
}
