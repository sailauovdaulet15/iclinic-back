<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBonusesTable extends Migration
{
    private const TABLE_NAME = 'bonuses';

    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->char('description', 200)->comment('Описание');
            $table->string('icon')->comment('Иконка');
            $table->string('numbers')->comment('Цифры');
            $table->string('unit_measurement')->comment('Единица измерения');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
