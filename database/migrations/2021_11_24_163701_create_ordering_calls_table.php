<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderingCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordering_calls', function (Blueprint $table) {
            $table->id();
            $table->string('user_name')->comment('Имя');
            $table->string('user_phone')->comment('Номер телефона');
            $table->foreignId('user_id')->nullable()->constrained()->cascadeOnDelete();
            $table->foreignId('specialisation_id')->nullable()->constrained();
            $table->date('date')->nullable()->comment('Удобная дата приема');
            $table->time('time')->nullable()->comment('Удобное время приема');
            $table->text('comment')->nullable()->comment('Комментарий');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordering_calls');
    }
}
