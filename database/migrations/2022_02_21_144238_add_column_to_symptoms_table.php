<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToSymptomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('symptoms', function (Blueprint $table) {
            $table->integer('specialisation_id')->default(1);
            $table->integer('position')->default(1);
            $table->foreign('specialisation_id')->references('id')->on('specialisations')->cascadeOnDelete();
            $table->index('specialisation_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('symptoms', function (Blueprint $table) {
            $table->dropForeign(['specialisation_id']);
            $table->dropIndex(['specialisation_id']);
            $table->dropColumn('specialisation_id');
            $table->dropColumn('position');
        });
    }
}
