<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCascadeToDoctorSpecialisationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctor_specialisation', function (Blueprint $table) {
            $table->dropForeign('doctor_specialisation_doctor_user_id_foreign');
            $table->dropForeign('doctor_specialisation_specialisation_id_foreign');

            $table->foreign('specialisation_id')->references('id')->on('specialisations')->cascadeOnDelete();
            $table->foreign('doctor_user_id')->references('user_id')->on('doctors')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctor_specialisation', function (Blueprint $table) {
            //
        });
    }
}
