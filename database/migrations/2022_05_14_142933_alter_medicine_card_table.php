<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMedicineCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medicine_cards', function (Blueprint $table) {
            $table->string('uid')->nullable();
            $table->foreignId('appointment_id')->constrained('appointments');
            $table->dropForeign(['history_visit_service_id']);
            $table->dropColumn('history_visit_service_id');
            $table->foreignId('history_visit_id')->nullable()->constrained('history_visits');
            $table->dropColumn('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
