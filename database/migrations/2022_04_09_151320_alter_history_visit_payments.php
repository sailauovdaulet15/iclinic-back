<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterHistoryVisitPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('history_visit_payments');

        Schema::create('history_visit_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->foreignId('history_visit_id')->constrained()->cascadeOnDelete();
            $table->foreignId('handbook_order_type_id')->nullable()->constrained()->cascadeOnDelete();;
            $table->integer('sum');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
