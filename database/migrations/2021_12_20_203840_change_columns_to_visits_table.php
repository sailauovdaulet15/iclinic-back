<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsToVisitsTable extends Migration
{
    private const TABLE_NAME = 'visits';
    private const SQLITE_DRIVER_NAME = 'sqlite';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (DB::getDriverName() !== self::SQLITE_DRIVER_NAME) { // Keep just this
            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
                $table->jsonb('data');
                $table->dropColumn('date');
                $table->dropColumn('start_time');
                $table->dropColumn('end_time');
                $table->dropColumn('time_mode');
                $table->dropColumn('cabinet_num');
            });
        } else {
            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
                $table->jsonb('data');
            });
            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
                $table->dropColumn(['date', 'start_time', 'end_time', 'time_mode', 'cabinet_num']);
            });
        }
    }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
                $table->date('date')->comment('Дата приема');
                $table->time('start_time')->comment('Время начала приема');
                $table->time('end_time')->comment('Время окончания приема');
                $table->boolean('time_mode');
                $table->integer('cabinet_num')->comment('Номер кабинета');
                $table->dropColumn('data');
            });
        }
    }
