<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainServicePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_service_points', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->string('description');

            $table->foreignId('specialisation_id')
                ->references('id')
                ->on('specialisations')
                ->restrictOnDelete();
            $table->foreignId('category_id')
                ->references('id')
                ->on('service_points_categories')
                ->restrictOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_service_points');
    }
}
