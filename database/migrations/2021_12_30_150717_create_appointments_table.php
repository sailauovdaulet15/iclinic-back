<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->integer('cabinet_num');
            $table->date('date')->comment('Дата приема');
            $table->time('start_time')->comment('Время начала приема');
            $table->time('end_time')->comment('Время окончания приема');
            $table->integer('status')->default(0);
            $table->foreignId('patient_id')->references('id')->on('users');
            $table->foreignId('doctor_user_id')->references('user_id')->on('doctors');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
