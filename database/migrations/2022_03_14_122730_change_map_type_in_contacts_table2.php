<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Doctrine\DBAL\Types\Type;
use TCG\Voyager\Database\Types\Postgresql\JsonbType;

class ChangeMapTypeInContactsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Type::hasType('jsonb')) {
            Type::addType('jsonb', JsonbType::class);
        }
        Schema::table('contacts', function (Blueprint $table) {
            $table->dropColumn('map');
        });
        Schema::table('contacts', function (Blueprint $table) {
            $table->jsonb('map')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table) {

        });
    }
}
