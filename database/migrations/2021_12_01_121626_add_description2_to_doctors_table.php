<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddDescription2ToDoctorsTable extends Migration
{
    private const TABLE_NAME = 'doctors';
    private const SQLITE_DRIVER_NAME = 'sqlite';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (DB::getDriverName() !== self::SQLITE_DRIVER_NAME) {
            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
                if (DB::getDriverName() !== self::SQLITE_DRIVER_NAME) {
                    $table->string('about2')->nullable();
                }
                $table->string('about')->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (DB::getDriverName() !== self::SQLITE_DRIVER_NAME) {
            Schema::table(self::TABLE_NAME, function (Blueprint $table) {
                if (DB::getDriverName() !== self::SQLITE_DRIVER_NAME) {
                    $table->dropColumn('about2');
                }
                $table->text('about')->change();
            });
        }
    }
}
