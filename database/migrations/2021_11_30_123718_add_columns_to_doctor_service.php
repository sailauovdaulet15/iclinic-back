<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToDoctorService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctor_service', function (Blueprint $table) {
            $table->dropUnique("doctor_service_doctor_user_id_service_id_unique");
            $table->float("service_percent")->nullable()
                ->comment("% от услуги");
            $table->date("start_date")->nullable()
                ->comment("Дата начала действия (оказания услуги)");
            $table->foreignId("form_of_participation_id")->nullable()
                ->comment("Форма участия (оказывает, направляет)")
                ->constrained();
            $table->integer("status")->nullable()
                ->comment("Статус (действует, прекращено)");
            $table->float("sum_of")->nullable()
                ->comment("Сумма");

            $table->unique(['doctor_user_id', 'service_id', 'form_of_participation_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctor_service', function (Blueprint $table) {
            $table->dropColumn(["service_percent","start_date","form_of_participation_id","status","sum_of"]);
        });
    }
}
