<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFailurePostLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('failure_post_links', function (Blueprint $table) {
            $table->id();
            $table->string('amount')->nullable();
            $table->string('currency')->nullable();
            $table->string('message')->nullable();
            $table->integer('code')->nullable();
            $table->string('accountId')->nullable();
            $table->string('invoiceId')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('failure_post_links');
    }
}
