<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCascadeOnDeleteToAppointmentServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appointment_service', function (Blueprint $table) {
            $table->dropForeign('appointment_service_appointment_id_foreign');
            $table->dropForeign('appointment_service_service_id_foreign');
            $table->foreign('appointment_id')->on('appointments')->references('id')->cascadeOnDelete();
            $table->foreign('service_id')->on('services')->references('id')->cascadeOnDelete();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointment_service', function (Blueprint $table) {
            //
        });
    }
}
