<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_visits', function (Blueprint $table) {
            $table->id();
            $table->string('uid')->index();
            $table->integer('doctor_user_id')->index();
            $table->integer('user_id')->index();
            $table->integer('total_amount');
            $table->integer('sum_bonus')->nullable();
            $table->foreign('doctor_user_id')->references('user_id')->on('doctors')->cascadeOnDelete();
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
            $table->timestamp('date_of_receipt')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_visits');
    }
}
