<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RemoveSpecialisationIdFromDoctorsTable extends Migration
{
    private const TABLE_NAME = 'doctors';
    private const SQLITE_DRIVER_NAME = 'sqlite';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            $table->dropConstrainedForeignId('specialisation_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(self::TABLE_NAME, function (Blueprint $table) {
            if (DB::getDriverName() !== self::SQLITE_DRIVER_NAME) {
                $table->foreignId('specialisation_id')->constrained();
            } else {
                $table->foreignId('specialisation_id')->nullable()->constrained();
            }
        });
    }
}
