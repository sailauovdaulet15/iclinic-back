<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswerOnQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_on_questions', function (Blueprint $table) {
            $table->id();
            $table->integer('history_visit_id')->index();
            $table->integer('handbook_survey_question_id')->index();
            $table->integer('patient_id')->index();
            $table->integer('doctor_user_id')->index();
            $table->integer('answer');
            $table->foreign('history_visit_id')->references('id')->on('history_visits')
                ->cascadeOnDelete();
            $table->foreign('handbook_survey_question_id')->references('id')
                ->on('handbook_survey_questions')
                ->cascadeOnDelete();
            $table->foreign('patient_id')->references('id')->on('users')->cascadeOnDelete();
            $table->foreign('doctor_user_id')->references('user_id')->on('doctors')
                ->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_on_questions');
    }
}
