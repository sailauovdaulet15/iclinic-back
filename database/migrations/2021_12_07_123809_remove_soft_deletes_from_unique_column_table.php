<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveSoftDeletesFromUniqueColumnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });

        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('specialisations', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
        Schema::table('doctors', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
