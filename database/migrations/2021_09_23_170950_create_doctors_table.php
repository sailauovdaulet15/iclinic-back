<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->integer('user_id')->primary();
            $table->string('category')->nullable()->comment('Категория');
            $table->integer('experience')->nullable()->comment('Стаж');
            $table->text('about')->nullable();
            $table->jsonb('link')->nullable();
            $table->jsonb('work_experience')->nullable();
            $table->jsonb('education_experience')->nullable();
            $table->integer('position')->default('1');

            // additional
            $table->string('experience')->comment('Стаж')->nullable()->change();
            $table->boolean('is_visible')->default(1);
            $table->string('active_status')->nullable();
            $table->integer('agent_percent')->nullable();
            $table->integer('duration')->nullable();


            $table->string('image_uri')->nullable();

            $table->foreignId('handbook_heal_category_id')->nullable()->constrained();
            $table->foreignId('specialisation_id')->constrained();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
