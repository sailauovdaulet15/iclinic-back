<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBurgerMenusTable extends Migration
{
    private const TABLE_NAME = 'burger_menus';

    public function up()
    {
        Schema::create(self::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string('title')->comment('Название');
            $table->string('link_external_internal')->comment('Ссылка (внешняя или внутреняя)');
            $table->integer('position')->default('1');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists(self::TABLE_NAME);
    }
}
