<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicineCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicine_cards', function (Blueprint $table) {
            $table->id();
            $table->integer('history_visit_service_id')->index()->comment('История посещение услуг');;
            $table->foreign('history_visit_service_id')->references('id')->on('history_visit_service')->cascadeOnDelete();
            $table->jsonb('files')->nullable()->comment('Файлы');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicine_cards');
    }
}
