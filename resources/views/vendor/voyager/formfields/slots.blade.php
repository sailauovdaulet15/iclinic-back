<br>


@foreach($dataTypeContent->{$row->field} as $field)
    <h3>{{ $field['date'] }}</h3>
    @foreach($field['interval_slots'] as $period)
        @if($period["status"])
            <button type="button" class="btn btn-primary">{{ $period["time"] }}</button>
        @else
            <button type="button" class="btn btn-light">{{ $period["time"] }}</button>
        @endif
    @endforeach
@endforeach
