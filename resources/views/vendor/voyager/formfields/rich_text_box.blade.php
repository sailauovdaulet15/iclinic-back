<textarea class="form-control richTextBox" name="{{ $row->field }}" id="richtext{{ $row->field }}">
    {{ old($row->field, $dataTypeContent->{$row->field} ?? '') }}
</textarea>

@push('javascript')
    <script>
        $(document).ready(function () {
            var additionalConfig = {
                selector: 'textarea.richTextBox[name="{{ $row->field }}"]',
            }

            $.extend(additionalConfig, {!! json_encode($options->tinymceOptions ?? '{}') !!})

            tinymce.init({
                selector: 'textarea.richTextBox[name="{{ $row->field }}"]',
                skin_url: $('meta[name="assets-path"]').attr('content') + '?path=js/skins/voyager',
                language_url: '{{ asset('js/langs/ru.js') }}',
                plugins: 'preview importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists wordcount help charmap emoticons',
                editimage_cors_hosts: ['picsum.photos'],
                menubar: 'file edit view insert format tools table help',
                toolbar: 'undo redo | bold italic underline strikethrough | fontfamily fontsize blocks | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
                toolbar_sticky: true,
                autosave_ask_before_unload: true,
                autosave_interval: '30s',
                autosave_prefix: '{path}{query}-{id}-',
                autosave_restore_when_empty: false,
                autosave_retention: '2m',
                image_advtab: true,
                image_class_list: [
                    {title: 'None', value: ''},
                    {title: 'Some class', value: 'class-name'}
                ],
                importcss_append: true,
                file_browser_callback: function (field_name, url, type, win) {
                    if (type == 'image') {
                        $('#upload_file').trigger('click');
                    }
                },
                template_cdate_format: '[Дата создана (CDATE): %m/%d/%Y : %H:%M:%S]',
                template_mdate_format: '[Дата обновлена (MDATE): %m/%d/%Y : %H:%M:%S]',
                height: 600,
                image_caption: true,
                noneditable_class: 'mceNonEditable',
                toolbar_mode: 'sliding',
                contextmenu: 'link image table',
                content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }',
                init_instance_callback: function (editor) {
                    if (typeof tinymce_init_callback !== "undefined") {
                        tinymce_init_callback(editor);
                    }
                },
                relative_urls : false,
                remove_script_host : false,
                convert_urls : true,
                external_plugins: {
                    colorpicker: '{{ asset('js/tinymce/plugins/colorpicker/plugin.min.js') }}',
                    preview: '{{ asset('js/tinymce/plugins/preview/plugin.min.js') }}',
                    importcss: '{{ asset('js/tinymce/plugins/importcss/plugin.min.js') }}',
                    searchreplace: '{{ asset('js/tinymce/plugins/searchreplace/plugin.min.js') }}',
                    autolink: '{{ asset('js/tinymce/plugins/autolink/plugin.min.js') }}',
                    autosave: '{{ asset('js/tinymce/plugins/autosave/plugin.min.js') }}',
                    save: '{{ asset('js/tinymce/plugins/save/plugin.min.js') }}',
                    directionality: '{{ asset('js/tinymce/plugins/directionality/plugin.min.js') }}',
                    save: '{{ asset('js/tinymce/plugins/save/plugin.min.js') }}',
                    visualblocks: '{{ asset('js/tinymce/plugins/visualblocks/plugin.min.js') }}',
                    visualchars: '{{ asset('js/tinymce/plugins/visualchars/plugin.min.js') }}',
                    fullscreen: '{{ asset('js/tinymce/plugins/fullscreen/plugin.min.js') }}',
                    media: '{{ asset('js/tinymce/plugins/media/plugin.min.js') }}',
                    codesample: '{{ asset('js/tinymce/plugins/codesample/plugin.min.js') }}',
                    charmap: '{{ asset('js/tinymce/plugins/charmap/plugin.min.js') }}',
                    pagebreak: '{{ asset('js/tinymce/plugins/pagebreak/plugin.min.js') }}',
                    nonbreaking: '{{ asset('js/tinymce/plugins/nonbreaking/plugin.min.js') }}',
                    anchor: '{{ asset('js/tinymce/plugins/anchor/plugin.min.js') }}',
                    insertdatetime: '{{ asset('js/tinymce/plugins/insertdatetime/plugin.min.js') }}',
                    advlist: '{{ asset('js/tinymce/plugins/advlist/plugin.min.js') }}',
                    wordcount: '{{ asset('js/tinymce/plugins/wordcount/plugin.min.js') }}',
                    help: '{{ asset('js/tinymce/plugins/help/plugin.min.js') }}',
                    emoticons: '{{ asset('js/tinymce/plugins/emoticons/plugin.min.js') }}',
                    template: '{{ asset('js/tinymce/plugins/template/plugin.min.js') }}',
                }
            });

        });
    </script>
@endpush
